import setuptools

from dhcp import __version__

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setuptools.setup(
    name="dhcp-neonatal-fmri-pipeline",
    version=__version__,
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Developing Human Connectome Project neonatal fMRI preproessing pipeline.",
    url="https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline",
    install_requires=install_requires,
    scripts=['dhcp/dhcp_neofmri.py', 'dhcp/util/dhcp_fetch_resources.py', 'dhcp/util/slicesdir.py'],
    packages=setuptools.find_packages(),
    include_package_data=True,
    python_requires='>=3.6',
)