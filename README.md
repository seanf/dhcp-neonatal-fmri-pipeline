# dHCP rfMRI Pipeline

Preprocessing pipeline for neonatal fMRI data from the [developing Human Connectome Project (dHCP)](http://www.developingconnectome.org).

[[_TOC_]]

## The pipeline

Neonates present significant challenges to data processing due to rapid developmental changes, low and variable contrast, and high levels of head motion. The dHCP neonatal fMRI pre-processing pipeline has been designed specifically to address the challenges of neonatal data. The pipeline includes integrated dynamic distortion and slice-to-volume motion correction, a robust multimodal registration approach including custom neonatal templates, bespoke ICA-based denoising, and an automated QC framework.

## Dependencies

1. FSL 6.0.4 (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/)
1. ANTs (http://stnava.github.io/ANTs/)
1. C3D (http://www.itksnap.org/pmwiki/pmwiki.php?n=Convert3D.Documentation)
1. FSL Fix (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX)
1. Toblerone (https://ibme-gitcvs.eng.ox.ac.uk/TomK/pytoblerone)
1. Connectome worbench (https://www.humanconnectome.org/software/get-connectome-workbench#download)
1. The dHCP structural pipeline (https://github.com/BioMedIA/dhcp-structural-pipeline)

## Installation
```shell
# install development version
pip install git+https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline.git

#OR append with specific version
pip install git+https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline.git@v1.0
```
To run the pipeline you will need to additionally download the dHCP neonatal volumetric atlas, (~7.2 GB), the trained FIX classifier (~176 MB), the dHCP group maps (~13 MB), and the dHCP release02 group QC (~189 MB). *This only needs to be run once.*

```shell
dhcp_fetch_resources.py --path=<path-to-download-to>
```

## Pipeline Schematic

![alt text](images/pipeline_schematic.png "Pipeline Schematic")

> The schematic is segregated into the 4 main conceptual processing stages by coloured background; fieldmap pre-processing (red), susceptibility and motion correction (orange), registration (green), and denoising (purple). Inputs to the pipeline are grouped in the top row, and the main pipeline outputs are grouped in the lower right. Blue filled rectangles with rounded corners indicate processing steps, whilst black rectangles (with no fill) represent data. The critical path is denoted by magenta connector arrows. (dc) = distortion corrected; (mcdc) = motion and distortion corrected.

## Usage

- [CLI Usage](doc/usage.md)
- Example [Python API usage](dhcp/examples/examples.md)

## Support

We are supporting this pipeline via the `developing-hcp` tag on [Neurostars](https://neurostars.org/tags/developing-hcp).

## How to cite

Detailed instructions on how to cite can be found here: http://www.developingconnectome.org/how-to-cite/

**Primary citation:**

Fitzgibbon, SP, Harrison, SJ, Jenkinson, M, Baxter, L, Robinson, EC, Bastiani, M, Bozek, J, Karolis, V, Cordero Grande, L, Price, AN, Hughes, E, Makropoulos, A, Passerat-Palmbach, J, Schuh, A, Gao, J, Farahibozorg, S, O'Muircheartaigh, J, Ciarrusta, J, O'Keeffe, C, Brandon, J, Arichi, T, Rueckert, D, Hajnal, JV, Edwards, AD, Smith, SM, \*Duff, E, \*Andersson, J  **The developing Human Connectome Project automated functional processing framework for neonates.**, *NeuroImage (2020), 223: 117303*, 2020. **doi:** https://doi.org/10.1016/j.neuroimage.2020.117303 _\*Authors contributed equally._

```
@article {Fitzgibbon766030,
	author = {Fitzgibbon, Sean P. and Harrison, Samuel J. and Jenkinson, Mark and Baxter, Luke and Robinson, Emma C. and Bastiani, Matteo and Bozek, Jelena and Karolis, Vyacheslav and Grande, Lucilio Cordero and Price, Anthony N. and Hughes, Emer and Makropoulos, Antonios and Passerat-Palmbach, Jonathan and Schuh, Andreas and Gao, Jianliang and Farahibozorg, Seyedeh-Rezvan and O{\textquoteright}Muircheartaigh, Jonathan and Ciarrusta, Judit and O{\textquoteright}Keeffe, Camilla and Brandon, Jakki and Arichi, Tomoki and Rueckert, Daniel and Hajnal, Joseph V. and Edwards, A. David and Smith, Stephen M. and Duff, Eugene and Andersson, Jesper},
	title = {The developing Human Connectome Project (dHCP) automated resting-state functional processing framework for newborn infants},
	elocation-id = {117303},
	year = {2020},
	doi = {10.1016/j.neuroimage.2020.117303},
	publisher = {Elsevier},
	URL = {https://doi.org/10.1016/j.neuroimage.2020.117303},
	eprint = {https://www.sciencedirect.com/science/article/pii/S1053811920307898/pdfft?md5=18806cf190a26f783de4bef456fe28b6&pid=1-s2.0-S1053811920307898-main.pdf},
	journal = {NeuroImage}
}
```
## Contributing

For information on how to contribute to this project see [here](doc/contributing.md)

## Other dHCP open resources

The dHCP consortium has publically released a number of [resources](doc/dhcp_open_resources.md) including data, pipelines, and atlases.

