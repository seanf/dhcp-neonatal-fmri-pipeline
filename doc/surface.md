# dHCP fMRI Surface Notes
Notes pertaining to sampling the dHCP fMRI to the native CIFTI, and then resampling the fMRI from the native CIFTI to the template CIFTI.

> Note: This work is under active development and validation, and is **NOT** ready for use.  It is likely to change. Use at your own risk.

## Aligning native surfaces to template surface

This is not currently implemented within the dHCP fMRI pipeline.

Scripts to align dHCP native surfaces to the dHCP surface template/atlas:
- https://github.com/ecr05/dHCP_template_alignment

## Sampling fMRI to the native surface

The adult HCP has a well established and validated pipeline for sampling the volumetric fMRI data to the native surface. 

1. Calculate GM ribbon mask
2. Identify local outliers of variation over time, exclude from mask
3. Ribbon-constrained volume to surface mapping
4. Surface-based dilation and masking

However this process it is not suitable for the dHCP data.  The main problem is that the dHCP fMRI voxel resolution (2.15mm isotropic) is much larger than the gray matter (GM) which has a thickness of ~1mm. Therefore, all GM voxels have severe partial voluming.  Our estimates suggest that most GM voxels in the dHCP data have only ~60% GM, and the best GM voxels only have around 80% GM (see figure below).  Compared to the adult HCP where are large proportion of GM voxels are 100% (i.e. *NO* partial voluming).

![alt text](../images/dhcp_gm_pve_example.png "GM Partial Volume Estimate (single subject)")

Therefore, we augment the adult HCP approach by dropping the "local outliers" stage (step 2), and incorporating a volumetric partial volume correction (PVC):

1. Partial volume correction (PVC)
2. Signal loss estimation and masking
3. Ribbon-constrained volume to surface mapping (as per adult HCP)
4. Dilation and masking (as per adult HCP)

The PVC procedure uses [Toblerone](https://github.com/tomfrankkirk/toblerone) to calculate a partial volume estimate, and then implements a local regression-based PVC.

![alt text](../images/PVC.png "With and without PVC")

The `sample_to_native` function in the `dhcp.func.surface` module implements all 4 stages and returns the fMRI in CIFTI format where the surface mesh has `native` surface vertex correspondence and the (subcortical) voxels are in native `func` space. See [dhcp_coordinate_systems.md](dhcp_coordinate_systems.md). 

```python
from dhcp.func import surface
surface.sample_to_native(
    struct_white_surf_right=f'sub-{subid}_ses-{sesid}_right_white.surf.gii',
    struct_white_surf_left=f'sub-{subid}_ses-{sesid}_left_white.surf.gii',
    struct_pial_surf_right=f'sub-{subid}_ses-{sesid}_right_pial.surf.gii',
    struct_pial_surf_left=f'sub-{subid}_ses-{sesid}_left_pial.surf.gii',
    struct_mid_surf_right=f'sub-{subid}_ses-{sesid}_right_midthickness.surf.gii',
    struct_mid_surf_left=f'sub-{subid}_ses-{sesid}_left_midthickness.surf.gii',
    medialwall_shape_left=f'sub-{subid}_ses-{sesid}_left_roi.shape.gii',
    medialwall_shape_right=f'sub-{subid}_ses-{sesid}_right_roi.shape.gii',
    struct_to_func_affine=f'sub-{subid}/ses-{sesid}/reg/func-mcdc2struct_invaffine.mat',
    func=f'sub-{subid}/ses-{sesid}/fix/func_clean.nii.gz',
    func_brainmask=f'sub-{subid}/ses-{sesid}/mcdc/func_mcdc_brainmask.nii.gz',
    struct=f'sub-{subid}/ses-{sesid}/import/T2w.nii.gz',
    workdir=f'sub-{subid}/ses-{sesid}/surface',
    struct_subcortical_mask=f'sub-{subid}/ses-{sesid}/import/T2w_scmask.nii.gz',
    do_sigloss=True
)
```


## Resampling native CIFTI fMRI to the template CIFTI

Resample the native CIFTI (i.e. surface mesh has `native` surface vertex correspondence, subcortical voxels in native `func` space) to the template CIFTI where the surface mesh has `fsLR32k` surface vertex correspondence and the subcortical voxels are in dHCP week-40 volumetric `template` space.  See [dhcp_coordinate_systems.md](dhcp_coordinate_systems.md).

```python
from dhcp.func import surface
surface.resample_to_template(
    func=f'../sub-{subid}/ses-{sesid}/fix/func_clean.nii.gz',
    func2template_warp=f'sub-{subid}/ses-{sesid}/reg/func-mcdc2standard_warp.nii.gz',
    func2template_invwarp=f'sub-{subid}/ses-{sesid}/reg/func-mcdc2standard_invwarp.nii.gz',
    func_gii_left=f'sub-{subid}/ses-{sesid}/surface/func_hemi-left_topo-native.func.gii',
    func_gii_right=f'sub-{subid}/ses-{sesid}/surface/func_hemi-right_topo-native.func.gii',
    sphere_reg_left=f"./surface_transforms/sub-{subid}_ses-{sesid}_left_sphere.reg.surf.gii",
    sphere_reg_right=f"./surface_transforms/sub-{subid}_ses-{sesid}_right_sphere.reg.surf.gii",
    volumetric_template='dhcp_volumetric_atlas/atlas/T2/template-40.nii.gz',
    volumetric_template_dseg='dhcp_volumetric_atlas/atlas/labels/tissues/label-40.nii.gz',
    template_sphere_left=f'templateDHCP/dHCP.week40.L.sphere.surf.gii',
    template_sphere_right=f'templateDHCP/dHCP.week40.R.sphere.surf.gii',
    template_medialwall_left='templateDHCP/dHCP.week40.L.roi.shape.gii',
    template_medialwall_right='templateDHCP/dHCP.week40.R.roi.shape.gii',
    struct_white_surf_right=f'sub-{subid}_ses-{sesid}_right_white.surf.gii',
    struct_white_surf_left=f'sub-{subid}_ses-{sesid}_left_white.surf.gii',
    struct_pial_surf_right=f'sub-{subid}_ses-{sesid}_right_pial.surf.gii',
    struct_pial_surf_left=f'sub-{subid}_ses-{sesid}_left_pial.surf.gii',
    struct_inflated_surf_right=f'sub-{subid}_ses-{sesid}_right_inflated.surf.gii',
    struct_inflated_surf_left=f'sub-{subid}_ses-{sesid}_left_inflated.surf.gii',
    struct_veryinflated_surf_right=f'sub-{subid}_ses-{sesid}_right_very_inflated.surf.gii',
    struct_veryinflated_surf_left=f'sub-{subid}_ses-{sesid}_left_very_inflated.surf.gii',
    struct_midthickness_surf_right=f'sub-{subid}_ses-{sesid}_right_midthickness.surf.gii',
    struct_midthickness_surf_left=f'sub-{subid}_ses-{sesid}_left_midthickness.surf.gii',
    workdir=f'sub-{subid}/ses-{sesid}/surface',
)
```
