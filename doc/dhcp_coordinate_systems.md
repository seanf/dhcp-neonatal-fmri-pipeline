# Coordinate Systems

[[_TOC_]]

## Space labels
For voxel coordinates, and/or surface coordinates

| Label | Description |
| --- | --- |
| `dhcp40wk` | dHCP atlas week-40 volumetric template space |
| `extdhcp40wk` | dHCP **extended** atlas week-40 volumetric template space |
| `serag40wk` | Serag atlas week-40 volumetric template space |
| `T2w` | native structural T2w space |
| `T1w` | native structural T1w space |
| `func` | native functional space (distorted) |
| `funcmcdc` | motion and distortion corrected functional space |
| `sbref` | native single-band EPI reference space |
| `fmap` | derived fieldmap space |

## Mesh labels
For surface vertices:

| Label | Description |
| --- | --- |
| `dhcp32kSym` | vertex correspondence with dHCP 32k Symmetric surface atlas |
| `native` | native vertices (no vertex correspondence) |

## Filenames

The `space` key with a *space-label* value is used to indicate the *space* that volumetric (or voxel) data is aligned to.

The `mesh` key with a *mesh-label* value is used to indicate *mesh* that surface data has vertex correspondence to. 

## Examples

1. gifti surface-geometry `*.surf.gii` requires both mesh-info and space-info for the surface, e.g.
```
{basename}_hemi-{hemi}_mesh-{mesh-label}_space-{space-label}.surf.gii

sub01_hemi-left_mesh-native_space-T2w.surf.gii
sub01_hemi-left_mesh-dhcp32kSym_space-T2w.surf.gii
sub01_hemi-left_mesh-dhcp32kSym_space-dhcp40wk.struct.surf.gii
```

2. gifti continuous, `*.func.gii` or `*.shape.gii`, require only mesh-info, e.g.
```
{basename}_hemi-{hemi}_mesh-{mesh-label}.surf.gii

sub01_hemi-right_mesh-native.func.gii
sub01_hemi-left_mesh-dhcp32kSym.func.gii
```

3. cifti dense time-series, `*.dtseries.nii`, requires a mesh-info for the surface and space-info for the subcortical voxels, e.g.
```
{basename}_mesh-{mesh-label}_space-{space-label}.dtseries.nii

sub01_mesh-native_space-func.dtseries.nii
sub01_mesh-dhcp32kSym_space-func.dtseries.nii
sub01_mesh-dhcp32kSym_space-dhcp40wk.dtseries.nii
```

# Volumetric Transforms

Volumetric transforms contain a `from` key and a `to` key to indicate the direction that the registration was calculated.  
The `from` value is the space-label for the *origin/source/moving* image, and the `to` value is the spcae-label for the *reference/target* image. e.g.


`from-{origin}_to-{ref}`: transform was calculated by registering the image in `origin` space  to the image in `ref` space 

**Affines:**

Transforms with <= 12 DOF have the file suffix `_affine` for the forward transform, or `_invaffine` for the inverse. 
```
{basename}_from-{space-label}_to-{space-label}_affine.mat
{basename}_from-{space-label}_to-{space-label}_invaffine.mat
```

**Warps:**

Nonlinear transforms have the file suffix `warp` for the forward transform, or `invwarp` for the inverse.
```
{basename}_from-{space-label}_to-{space-label}_warp.nii.gz
{basename}_from-{space-label}_to-{space-label}_invwarp.nii.gz
```

**Applying transforms:**
TBD

# Surface Transforms
Surface transforms have the suffix `sphere`.  
```
{basename}_from-{mesh-label}_to-{mesh-label}_sphere.gii

{basename}_from-native_to-dhcp32kSym_sphere.gii
```

**Applying transforms:**
TBD






