# Usage: dHCP neonatal fMRI pipeline

[[_TOC_]]

## CLI Usage

The main CLI script for running the dHCP neonatal fMRI pipeline is `dhcp_neofmri.py`.

Help for `dhcp_neofmri.py` can be obtained with the `-h` flag:

``` 

>> dhcp_neofmri.py -h

usage: dhcp_neofmri.py [-h]
                       {RUNALL,PRE-MCDC,MCDC,POST-MCDC,IMPORT,FIELDMAP,ICA,DENOISE,STANDARD}
                       ... path subid sesid

positional arguments:
  {RUNALL,PRE-MCDC,MCDC,POST-MCDC,IMPORT,FIELDMAP,ICA,DENOISE,STANDARD}
  path                  Base path for pipeline outputs
  subid                 Subject ID
  sesid                 Session ID

optional arguments:
  -h, --help            show this help message and exit

```

`dhcp_neofmri.py` contains a number of sub-commands: 

`{RUNALL,PRE-MCDC,MCDC,POST-MCDC,IMPORT,FIELDMAP,ICA,DENOISE,STANDARD}`

each of which has it's own help:

```  
>> dhcp_neofmri.py IMPORT -h

usage: dhcp_neofmri.py IMPORT [-h] --scan_ga <float> --birth_ga <float> --T2w <path> --T2w_brainmask
                              <path> --T2w_dseg <path>
                              [--T2w_dseg_type {'drawem', 'freesurfer_aseg', 'fsl_fast'}]
                              [--T1w <path>] --func <path> [--func_pedir {'AP', 'PA', 'LR', 'RL'}]
                              [--func_echospacing <float>] [--func_sliceorder <path>] --sbref <path>
                              [--sbref_pedir {'AP', 'PA', 'LR', 'RL'}] [--sbref_echospacing <float>]
                              --spinecho <path> --spinecho_pedir <comma-separated list>
                              --spinecho_echospacing <float> [--spinecho_epifactor <int>]
                              [--spinecho_inplaneaccel <float>]

Import files in the pipeline

optional arguments:
  -h, --help                                           show this help message and exit
  --T1w <path>                                         T1w image filename (aligned with T2w)
  --spinecho_epifactor <int>                           Spinecho epi factor (k-space lines in single
                                                       epi shot) (default=calculated from spinecho)
  --spinecho_inplaneaccel <float>                      Spinecho in-plane acceleration factor
                                                       (default=1)

required named arguments:
  --scan_ga <float>                                    Gestational age at scan (weeks)
  --birth_ga <float>                                   Gestational age at birth (weeks)
  --T2w <path>                                         T2w image filename
  --T2w_brainmask <path>                               T2w binary brain mask filename
  --T2w_dseg <path>                                    T2w discrete segmentation filename
  --T2w_dseg_type {'drawem', 'freesurfer_aseg', 'fsl_fast'}
                                                       T2w discrete segmentation type
                                                       [default=drawem]
  --func <path>                                        Functional 4D image filename
  --func_pedir {'AP', 'PA', 'LR', 'RL'}                Functional phase encoding direction
  --func_echospacing <float>                           Functional effective EPI echo spacing
                                                       (seconds)
  --func_sliceorder <path>                             Name of slice order specification file (or
                                                       "dhcp" for default dhcp slice spec)
  --sbref <path>                                       Name of SBref image
  --sbref_pedir {'AP', 'PA', 'LR', 'RL'}               SBRef phase encoding direction
  --sbref_echospacing <float>                          SBRef effective EPI echo spacing (seconds)
  --spinecho <path>                                    Name of 4D file with spinecho images
  --spinecho_pedir <comma-separated list>              Spinecho phase encoding direction
  --spinecho_echospacing <float>                       Spinecho effective EPI echo spacing (seconds)


```



## CLI example (RUNALL)

In this example, the pipeline is run on one-subject from the dHCP release 02 data. The pipeline is run in two stages:
1. `IMPORTDATA`: Import the necessary data
2. `RUNALL`: Run all the post-import stages

> Note: The pipeline requires environments variables to be set indicating where to find ANTs, FIX, and C3D dependencies.  Also FIX requires several environment variables to be set.

```shell
# Setup env variables for pipeline dependencies
export DHCP_ANTS_PATH="/vol/dhcp-results/bin/ANTs/bin/bin"
export DHCP_C3D_PATH="/vol/dhcp-results/bin/c3d/bin"
export DHCP_FIX_PATH="/vol/dhcp-results/bin/fix"

# Setup env variables for FIX
export FSL_FIX_MATLAB_MODE=1
export MATLAB_BIN="/usr/bin/matlab"

# Location of sourcedata and derivatives data (as per dHCP Release 02)
sourcedata="./sourcedata"
derivatives="./derivatives/dhcp_anat_pipeline"

# subject info
subid="CC00108XX09"
sesid="36800"
scan_age=42.57
birth_age=40.0

# root path for pipeline outputs 
workdir="./pipeline_test"

# import
dhcp_neofmri.py IMPORT \
    --scan_ga=$scan_age \
    --birth_ga=$birth_age \
    --func="${sourcedata}/sub-${subid}/ses-${sesid}/func/sub-${subid}_ses-${sesid}_task-rest_bold.nii.gz" \
    --func_echospacing=0.0007216 \
    --func_pedir=PA \
    --sbref="${sourcedata}/sub-${subid}/ses-${sesid}/func/sub-${subid}_ses-${sesid}_task-rest_sbref.nii.gz" \
    --sbref_echospacing=0.0007216 \
    --sbref_pedir=PA \
    --func_sliceorder="func.slorder" \
    --T2w="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-restore_T2w.nii.gz" \
    --T1w="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-restore_space-T2w_T1w.nii.gz" \
    --T2w_brainmask="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-bet_space-T2w_brainmask.nii.gz" \
    --T2w_dseg="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-drawem9_space-T2w_dseg.nii.gz" \
    --T2w_dseg_type="drawem" \
    --spinecho="${sourcedata}/sub-${subid}/ses-${sesid}/fmap/sub-${subid}_ses-${sesid}_dir-APPA_epi.nii.gz" \
    --spinecho_echospacing=0.0007188 \
    --spinecho_pedir="AP,PA,AP,PA,AP,PA,AP,PA" \
    ${workdir} ${subid} ${sesid}

dhcp_neofmri.py RUNALL ${workdir} ${subid} ${sesid}
```

## CLI example (stages)

The pipeline can also be run in stages, which can be convenient for managing your compute resources, or if certain stages need to be rerun. 

For example, the  `MCDC` stage requires GPU resources which is often a limited resource in many clusters.  Therefore, it can beneficial to run the `MCDC` stage separately in a GPU environment.

In this example, the pipeline is run on one-subject from the dHCP release 02 data. The pipeline is run in four stages:
1. `IMPORTDATA`: Import the necessary data
2. `PRE-MCDC`: Run all the pre-MCDC stages
3. `MCDC`: Run the MCDC stage, which requires access to a GPU
4. `POST-MCDC`: Run all the post-MCDC stages

First we import the data:

```shell
# Location of sourcedata and derivatives data (as per dHCP Release 02)
sourcedata="./sourcedata"
derivatives="./derivatives/dhcp_anat_pipeline"

# subject info
subid="CC00108XX09"
sesid="36800"
scan_age=42.57
birth_age=40.0

# root path for pipeline outputs 
workdir="./pipeline_test"

# import
dhcp_neofmri.py IMPORT \
    --scan_ga=$scan_age \
    --birth_ga=$birth_age \
    --func="${sourcedata}/sub-${subid}/ses-${sesid}/func/sub-${subid}_ses-${sesid}_task-rest_bold.nii.gz" \
    --func_echospacing=0.0007216 \
    --func_pedir=PA \
    --sbref="${sourcedata}/sub-${subid}/ses-${sesid}/func/sub-${subid}_ses-${sesid}_task-rest_sbref.nii.gz" \
    --sbref_echospacing=0.0007216 \
    --sbref_pedir=PA \
    --func_sliceorder="func.slorder" \
    --T2w="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-restore_T2w.nii.gz" \
    --T1w="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-restore_space-T2w_T1w.nii.gz" \
    --T2w_brainmask="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-bet_space-T2w_brainmask.nii.gz" \
    --T2w_dseg="${derivatives}/sub-${subid}/ses-${sesid}/anat/sub-${subid}_ses-${sesid}_desc-drawem9_space-T2w_dseg.nii.gz" \
    --T2w_dseg_type="drawem" \
    --spinecho="${sourcedata}/sub-${subid}/ses-${sesid}/fmap/sub-${subid}_ses-${sesid}_dir-APPA_epi.nii.gz" \
    --spinecho_echospacing=0.0007188 \
    --spinecho_pedir="AP,PA,AP,PA,AP,PA,AP,PA" \
    ${workdir} ${subid} ${sesid}
```

Then we run the pre-MCDC stages:

```shell
# subject info
subid="CC00108XX09"
sesid="36800"

# root path for pipeline outputs 
workdir="./pipeline_test"

# Run pre-MCDC stages
dhcp_neofmri.py PRE-MCDC ${workdir} ${subid} ${sesid}
```

Then we run the MCDC stage (GPU required):

```shell
# subject info
subid="CC00108XX09"
sesid="36800"

# root path for pipeline outputs 
workdir="./pipeline_test"

# Run MCDC stage
dhcp_neofmri.py MCDC ${workdir} ${subid} ${sesid}
```

Finally we run the post-MCDC stages:

> Note: The pipeline requires environments variables to be set indicating where to find ANTs, FIX, and C3D dependencies.  Also FIX requires several environment variables to be set.

```shell
# Setup env variables for pipeline dependencies
export DHCP_ANTS_PATH="/vol/dhcp-results/bin/ANTs/bin/bin"
export DHCP_C3D_PATH="/vol/dhcp-results/bin/c3d/bin"
export DHCP_FIX_PATH="/vol/dhcp-results/bin/fix"

# Setup env variables for FIX
export FSL_FIX_MATLAB_MODE=1
export MATLAB_BIN="/usr/bin/matlab"

# subject info
subid="CC00108XX09"
sesid="36800"

# root path for pipeline outputs 
workdir="./pipeline_test"

# Run post-MCDC stages
dhcp_neofmri.py POST-MCDC ${workdir} ${subid} ${sesid}
```

Alternatively, the `PRE-MCDC` and `POST-MCDC` stages can be further separated into sub-stages:
1. `IMPORTDATA`: Import the necessary data (*not shown*)
2. `FIELDMAP`: Prepare fieldmaps, and register to native structural and functional spaces
3. `MCDC`: Run the MCDC stage, which requires access to a GPU, and register to native structural space
4. `STANDARD`: Register to dHCP volumetric atlas
5. `ICA`: Run single-subject spatial ICA
6. `DENOISE`: Run FIX-based denoising

> Note: the `STANDARD` and `ICA` stages are mutually exclusive and can be run in parallel, however the other stages must be run sequentially.

```shell
# Setup env variables for pipeline dependencies
export DHCP_ANTS_PATH="/vol/dhcp-results/bin/ANTs/bin/bin"
export DHCP_C3D_PATH="/vol/dhcp-results/bin/c3d/bin"
export DHCP_FIX_PATH="/vol/dhcp-results/bin/fix"

# Setup env variables for FIX
export FSL_FIX_MATLAB_MODE=1
export MATLAB_BIN="/usr/bin/matlab"

# subject info
subid="CC00108XX09"
sesid="36800"

# root path for pipeline outputs 
workdir="./pipeline_test"

# Prepare fieldmaps
dhcp_neofmri.py FIELDMAP ${workdir} ${subid} ${sesid}

# MCDC
dhcp_neofmri.py MCDC ${workdir} ${subid} ${sesid}

# Register to dHCP volumetric atlas
dhcp_neofmri.py STANDARD ${workdir} ${subid} ${sesid}

# Run ICA
dhcp_neofmri.py ICA ${workdir} ${subid} ${sesid}

# FIX-based denoising
dhcp_neofmri.py DENOISE ${workdir} ${subid} ${sesid}

```