# Contributing to ``dhcp-neonatal-fmri-pipeline``

> This document is adapted from https://git.fmrib.ox.ac.uk/fsl/fslpy/-/blob/master/doc/contributing.rst \
> Created by [@paulmc](https://git.fmrib.ox.ac.uk/paulmc)

Development model
-----------------


- All development occurs on the master branch.

- All changes to the master branch occur via merge requests. Individual
  developers are free to choose their own development workflow in their own
  repositories.


Version number
--------------


The ``dhcp-neonatal-fmri-pipeline`` version number roughly follows `semantic versioning
<http://semver.org/>` rules, so that dependant projects are able to perform
compatibility testing.  The full version number string consists of three
numbers::

    major.minor.patch

- The ``patch`` number is incremented on bugfixes and minor
  (backwards-compatible) changes.

- The ``minor`` number is incremented on feature additions and/or
  backwards-compatible changes.

- The ``major`` number is incremented on major feature additions, and
  backwards-incompatible changes.


The version number in the ``master`` branch should be of the form
``major.minor.patch.dev``, to indicate that any releases made from this branch
are development releases (although development releases are not part of the
release model).


Releases
--------


A separate branch is created for each **minor** release. The name of the
branch is ``v[major.minor]``, where ``[major.minor]`` is the first two
components of the release version number (see above). For example, the branch
name for minor release ``1.0`` would be ``v1.0``.


Patches and bugfixes may be added to these release branches as ``patch``
releases.  These changes should be made on the master branch like any other
change (i.e. via merge requests), and then cherry-picked onto the relevant
release branch(es).


Every release commit is also tagged with its full version number.  For
example, the first release off the ``v1.0`` branch would be tagged with
``1.0.0``.  Patch releases to the ``v1.0`` branch would be tagged with
``1.0.1``, ``1.0.2``, etc.