#!/usr/bin/env fslpython
from unittest import TestCase

import dhcp.func.registration as reg
from dhcp.util import util
from dhcp.util.io import Path
from dhcp.func import surface
from dhcp.util.enums import PhaseEncodeDirection
from dhcp.util.workdir import WorkDir
import logging
from fsl.utils.filetree import FileTree
import numpy as np
from dhcp.util import fslpy as fsl


TESTDIR = util.get_dhcp_dir().join('tests')
TESTDATA = TESTDIR.join('data')

SUBID = 'CC00108XX09'
SESID = '36800'

SURF_PATH = Path(f'{TESTDATA}/derivatives/dhcp_anat_pipeline/sub-{SUBID}/ses-{SESID}/anat')


def test_template_to_struct(*args, **kwargs):

    scan_ga = 42.57
    age = int(np.round(scan_ga))

    atlas_tree = Path(util.get_setting('dhcp_volumetric_atlas_tree'))
    atlas = FileTree.read(atlas_tree).update(path=atlas_tree.dirname)

    # extract and format PVE (toblerone)

    surface.toblerone_pve(
        white_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_wm.surf.gii'),
        white_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_wm.surf.gii'),
        pial_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_pial.surf.gii'),
        pial_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_pial.surf.gii'),
        ref=f'{TESTDATA}/sub-{SUBID}/ses-{SESID}/import/T2w.nii.gz',
        struct=f'{TESTDATA}/sub-{SUBID}/ses-{SESID}/import/T2w.nii.gz',
        struct_to_ref_xfm='I',
        workdir=f'{TESTDATA}/test_template_to_struct',
    )

    fsl.fslroi(
        input=f'{TESTDATA}/test_template_to_struct/pve.nii.gz',
        output=f'{TESTDATA}/test_template_to_struct/gm_probseg.nii.gz',
        tmin=0,
        tsize=1,
    )

    fsl.fslmaths(f'{TESTDATA}/test_template_to_struct/gm_probseg.nii.gz').mul(100).run()

    # template (scan-age) -> struct

    reg.template_to_struct(
        age=age,
        struct_brainmask=f'{TESTDATA}/sub-{SUBID}/ses-{SESID}/import/T2w_brainmask.nii.gz',
        struct_gmprob=f'{TESTDATA}/test_template_to_struct/gm_probseg.nii.gz',
        struct_T2w=f'{TESTDATA}/sub-{SUBID}/ses-{SESID}/import/T2w.nii.gz',
        atlas=atlas,
        workdir=f'{TESTDATA}/test_template_to_struct'
    )




# reg.fmap_to_struct(
#     fmap=inputs.get('fmap'),
#     fmap_magnitude=inputs.get('fmap_magnitude'),
#     fmap_brainmask=inputs.get('fmap_brainmask'),
#     struct=inputs.get('T2w'),
#     struct_brainmask=inputs.get('T2w_brainmask'),
#     struct_boundarymask=inputs.get('T2w_wmmask'),
#     outputs=defaults.update(src_space='fmap', ref_space='struct'),
#     do_bbr=True
# )
#
# reg.func_to_sbref(
#     func=inputs.get('func0'),
#     func_brainmask=inputs.get('func_brainmask'),
#     sbref=inputs.get('sbref'),
#     sbref_brainmask=inputs.get('sbref_brainmask'),
#     outputs=defaults.update(src_space='func', ref_space='sbref')
# )


# # sbref2struct with BBR and DC
# reg.sbref2struct(
#     sbref=inputs.get('sbref'),
#     sbref_brainmask=inputs.get('sbref_brainmask'),
#     sbref_pedir=PhaseEncodeDirection.PA,
#     sbref_echospacing=0.0007216,
#     struct=inputs.get('T2w'),
#     struct_brainmask=inputs.get('T2w_brainmask'),
#     struct_boundarymask=inputs.get('T2w_wmmask'),
#     fmap=inputs.get('fmap'),
#     fmap_brainmask=inputs.get('fmap_brainmask'),
#     fmap2struct_xfm=defaults.update(src_space='fmap', ref_space='struct').get('affine'),
#     do_bbr=True,
#     do_dc=True,
#     outputs=defaults.update(src_space='sbref', ref_space='struct')
# )

# composite func2struct
# reg.func_to_struct_composite(
#     func=inputs.get('func0'),
#     struct=inputs.get('T2w'),
#     sbref2struct_affine=defaults.update(src_space='sbref', ref_space='struct').get('affine'),
#     sbref2struct_warp=defaults.update(src_space='sbref', ref_space='struct').get('warp'),
#     func2sbref_affine=defaults.update(src_space='func', ref_space='sbref').get('affine'),
#     outputs=defaults.update(src_space='func', ref_space='struct')
# )


# # composite fmap2func
# reg.fmap2func(
#     fmap=inputs.get('fmap'),
#     func=inputs.get('func0'),
#     fmap2struct_affine=defaults.update(src_space='fmap', ref_space='struct').get('affine'),
#     func2struct_invaffine=defaults.update(src_space='func', ref_space='struct').get('inv_affine'),
#     outputs=defaults.update(src_space='fmap', ref_space='func')
# )
