#!/usr/bin/env python
import dhcp.func.importdata as importdata
from dhcp.util import util
from dhcp.util.io import Path
from dhcp.util.enums import PhaseEncodeDirection, PhaseUnits
import tempfile
import shutil

from .metrics import assert_is_equal

TESTDIR = util.get_dhcp_dir().join('tests')
TESTDATA = TESTDIR.join('data')

SUBID = 'CC00108XX09'
SESID = '36800'
SUBDIR = f'sub-{SUBID}/ses-{SESID}'

SURF_PATH = Path(f'{TESTDATA}/derivatives/dhcp_anat_pipeline/sub-{SUBID}/ses-{SESID}/anat')
SOURCEDATA_PATH = Path(f'{TESTDATA}/sourcedata')
DERIVATIVES_PATH = Path(f'{TESTDATA}/derivatives/dhcp_anat_pipeline')


# TESTS

def test_import_data(*args, **kwargs):

    # imaging parameters
    AP = PhaseEncodeDirection.AP
    PA = PhaseEncodeDirection.PA

    spinecho_echospacing = 0.0007188
    spinecho_pedir = [AP, PA, AP, PA, AP, PA, AP, PA]
    spinecho_inplaneaccel = 1

    sbref_echospacing = 0.0007216
    sbref_pedir = PA

    func_echospacing = 0.0007216
    func_pedir = PA

    with tempfile.TemporaryDirectory(dir=TESTDATA) as tmp:

        importdata.import_info(
            subid=SUBID,
            sesid=SESID,
            scan_pma=41.3,
            birth_ga=40.0,
            workdir=tmp,
        )

        importdata.import_fieldmap(
            fieldmap=f'{TESTDATA}/derivatives/dhcp_fmri_pipeline/{SUBDIR}/fmap/sub-{SUBID}_ses-{SESID}_fieldmap.nii.gz',
            fieldmap_magnitude=f'{TESTDATA}/derivatives/dhcp_fmri_pipeline/{SUBDIR}/fmap/sub-{SUBID}_ses-{SESID}_magnitude.nii.gz',
            fieldmap_brainmask=None,
            fieldmap_units=PhaseUnits.rads,
            workdir=tmp,
        )

        importdata.import_func(
            func=f'{TESTDATA}/sourcedata/{SUBDIR}/func/sub-{SUBID}_ses-{SESID}_task-rest_bold.nii.gz',
            func_pedir=func_pedir,
            func_echospacing=func_echospacing,
            sbref=f'{TESTDATA}/sourcedata/{SUBDIR}/func/sub-{SUBID}_ses-{SESID}_task-rest_sbref.nii.gz',
            sbref_pedir=sbref_pedir,
            sbref_echospacing=sbref_echospacing,
            func_slorder=util.get_resource('default_func.slorder'),
            workdir=tmp,
        )

        importdata.import_struct(
            T2w=f'{DERIVATIVES_PATH}/{SUBDIR}/anat/sub-{SUBID}_ses-{SESID}_desc-restore_T2w.nii.gz',
            T1w=f'{DERIVATIVES_PATH}/{SUBDIR}/anat/sub-{SUBID}_ses-{SESID}_desc-restore_space-T2w_T1w.nii.gz',
            brainmask=f'{DERIVATIVES_PATH}/{SUBDIR}/anat/sub-{SUBID}_ses-{SESID}_desc-drawem_space-T2w_brainmask.nii.gz',
            dseg=f'{DERIVATIVES_PATH}/{SUBDIR}/anat/sub-{SUBID}_ses-{SESID}_desc-drawem9_space-T2w_dseg.nii.gz',
            workdir=tmp,
        )

        importdata.import_spinecho(
            spinecho=f'{TESTDATA}/sourcedata/{SUBDIR}/fmap/sub-{SUBID}_ses-{SESID}_dir-APPA_epi.nii.gz',
            spinecho_pedir=spinecho_pedir,
            spinecho_echospacing=spinecho_echospacing,
            spinecho_inplaneaccel=spinecho_inplaneaccel,
            workdir=tmp,
        )

        importdata.import_surfaces(
            white_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_wm.surf.gii'),
            white_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_wm.surf.gii'),
            pial_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_pial.surf.gii'),
            pial_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_pial.surf.gii'),
            midthickness_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_midthickness.surf.gii'),
            midthickness_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_midthickness.surf.gii'),
            inflated_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_inflated.surf.gii'),
            inflated_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_inflated.surf.gii'),
            veryinflated_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_veryinflated.surf.gii'),
            veryinflated_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_veryinflated.surf.gii'),
            sphere_surf_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_space-T2w_sphere.surf.gii'),
            sphere_surf_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_space-T2w_sphere.surf.gii'),
            medialwall_shape_left=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-R_desc-medialwall_mask.shape.gii'),
            medialwall_shape_right=SURF_PATH.join(f'sub-{SUBID}_ses-{SESID}_hemi-L_desc-medialwall_mask.shape.gii'),
            workdir=tmp,
        )

        # shutil.copytree(
        #     tmp,
        #     f'{TESTDATA}/test_importdata_new',
        # )
        #


        # TODO: Add sidecar
        # TODO: check surfaces
        files = [
            'func.nii.gz',
            'func0.nii.gz',
            'func_mean.nii.gz',
            'func_std.nii.gz',
            'func_tsnr.nii.gz',
            'func_brainmask.nii.gz',
            'func_dilated_brainmask.nii.gz',
            'func_regressors.tsv',
            'T2w.nii.gz',
            'T2w_brainmask.nii.gz',
            'T2w_wmmask.nii.gz',
            'T2w_dseg.nii.gz',
            'T1w.nii.gz',
            'spinecho.nii.gz',
            'sbref.nii.gz',
            'sbref_brainmask.nii.gz',
            'fieldmap_magnitude.nii.gz',
            'fieldmap.nii.gz',
            'fieldmap_brainmask.nii.gz'
        ]

        for f in files:
            assert_is_equal(
                f'{tmp}/{f}',
                f'{TESTDATA}/test_importdata/{f}',
            )