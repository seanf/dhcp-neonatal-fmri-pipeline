#!/usr/bin/env python
import dhcp.func.pipeline as pipeline
from dhcp.util import util
import tempfile
from dhcp.util.enums import PhaseEncodeDirection, SegType
import shutil

from .metrics import assert_is_equal

TESTDIR = util.get_dhcp_dir().join('tests')
TESTDATA = TESTDIR.join('data')

subid = 'CC00108XX09'
sesid = '36800'

subdir = f'sub-{subid}/ses-{sesid}'

AP = PhaseEncodeDirection.AP
PA = PhaseEncodeDirection.PA

# TESTS

def test_import(*args, **kwargs):

    with tempfile.TemporaryDirectory(dir=TESTDATA) as tmp:

        p = pipeline.Pipeline(subid, sesid, tmp)

        p.import_data(
            scan_ga=41.3,
            birth_ga=40.0,
            func=f'{TESTDATA}/{subdir}/import/func.nii.gz',
            func_echospacing=0.0007216,
            func_pedir=PA,
            sbref=f'{TESTDATA}/{subdir}/import/sbref.nii.gz',
            sbref_pedir=PA,
            sbref_echospacing=0.0007216,
            func_sliceorder=util.get_resource('default_func.slorder'),
            T2w=f'{TESTDATA}/{subdir}/import/T2w.nii.gz',
            T1w=f'{TESTDATA}/{subdir}/import/T1w.nii.gz',
            T2w_brainmask=f'{TESTDATA}/{subdir}/import/T2w_brainmask.nii.gz',
            T2w_dseg=f'{TESTDATA}/{subdir}/import/T2w_dseg.nii.gz',
            T2w_dseg_type=SegType.drawem,
            spinecho=f'{TESTDATA}/{subdir}/import/spinecho.nii.gz',
            spinecho_echospacing=0.0007188,
            spinecho_pedir=[AP, PA, AP, PA, AP, PA, AP, PA],
        )

        files = [
            'func.nii.gz',
            'func.json',
            'func0.nii.gz',
            'func_mean.nii.gz',
            'func_std.nii.gz',
            'func_tsnr.nii.gz',
            'func_brainmask.nii.gz',
            'func_dilated_brainmask.nii.gz',
            'func_regressors.tsv',
            'T2w.nii.gz',
            'T2w_brainmask.nii.gz',
            'T2w_wmmask.nii.gz',
            'T2w_dseg.nii.gz',
            'T2w_dseg.json',
            'T1w.nii.gz',
            'spinecho.nii.gz',
            'spinecho.json',
            'sbref.nii.gz',
            'sbref.json',
            'sbref_brainmask.nii.gz',
            'func_regressors.tsv'
        ]

        for f in files:
            assert_is_equal(
                f'{tmp}/{subdir}/import/{f}',
                f'{TESTDATA}/{subdir}/import/{f}',
            )