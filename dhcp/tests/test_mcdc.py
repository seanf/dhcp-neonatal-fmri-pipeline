#!/usr/bin/env python
import shutil
import unittest
from typing import Optional, Union
import tempfile
import dhcp.func.mcdc as mcdc
from dhcp.util import enums
from dhcp.util import util
from dhcp.util.io import Path
from dhcp.util import fslpy as fsl

from dhcp.tests.metrics import assert_is_equal
from dhcp.tests.mocks import mock_eddy_mcdc, mock_mcflirt_mc

# import os

TESTDIR = util.get_dhcp_dir().join('tests')
TESTDATA = TESTDIR.join('data')

subid = 'CC00108XX09'
sesid = '36800'


@unittest.mock.patch('dhcp.func.mcdc.mcflirt_mc', side_effect=mock_mcflirt_mc)
def test_mcdc_rigid_nodc_mcflirt(*args, **kwargs):
    # rigid, no DC, mcflirt

    with tempfile.TemporaryDirectory(dir=TESTDIR) as tmp:

        mcdc.mcdc(
            func=f'{TESTDATA}/sub-{subid}/ses-{sesid}/import/func.nii.gz',
            use_mcflirt=True,
            workdir=tmp
        )

        files = [
            'func_mc.nii.gz',
            'func_mc_mean.nii.gz',
            'func_mc_tsnr.nii.gz',
            'func_mc_std.nii.gz',
            'func_mc_brainmask.nii.gz',
            'func_mc_motion.tsv',
            'func_mc_regressors.tsv',
        ]

        for f in files:
            assert_is_equal(
                f'{tmp}/{f}',
                f'{TESTDATA}/test_mcdc_rigid_nodc_mcflirt/{f}',
            )


@unittest.mock.patch('dhcp.func.mcdc.eddy_mcdc', side_effect=mock_eddy_mcdc)
def test_mcdc_s2v_mbs_dc(*args, **kwargs):
    # s2v, mbs, eddy

    with tempfile.TemporaryDirectory(dir=TESTDIR) as tmp:

        mcdc.mcdc(
            func=f'{TESTDATA}/sub-{subid}/ses-{sesid}/import/func.nii.gz',
            func_echospacing=0.0007216,
            func_pedir=enums.PhaseEncodeDirection.PA,
            func_brainmask=f'{TESTDATA}/sub-{subid}/ses-{sesid}/import/func_brainmask.nii.gz',
            fmap=f'{TESTDATA}/sub-{subid}/ses-{sesid}/reg/fmap_to_func/fmap_to_func_img.nii.gz',
            workdir=tmp
        )

        files = [
            'func_mcdc.nii.gz',
            'func_mcdc_mean.nii.gz',
            'func_mcdc_tsnr.nii.gz',
            'func_mcdc_std.nii.gz',
            'func_mcdc_brainmask.nii.gz',
            'func_mcdc_motion.tsv',
            'func_mcdc_regressors.tsv',
            'func_mcdc.eddy_mbs_first_order_fields.nii.gz',
            'func_mcdc.eddy_output_mask.nii.gz',
            'func_mcdc_fovmask.nii.gz',
            'func_mcdc_fovpercent.nii.gz',
        ]

        for f in files:
            assert_is_equal(
                f'{tmp}/{f}',
                f'{TESTDATA}/test_mcdc_s2v_mbs_dc/{f}',
            )


def test_mcflirt_mc(*args, **kwargs):
    # rigid, mcflirt

    with tempfile.TemporaryDirectory(dir=TESTDIR) as tmp:

        fsl.fslroi(
            input=f'{TESTDATA}/sub-{subid}/ses-{sesid}/import/func.nii.gz',
            output=f'{tmp}/func_roi.nii.gz',
            tmin=0,
            tsize=50,
        )


        mcdc.mcflirt_mc(
            func=f'{tmp}/func_roi.nii.gz',
            func_mc=f'{tmp}/func_mc.nii.gz',
            ref=0,
        )


        files = [
            'func_mc.nii.gz',
            'func_mc.par',
            'func_mc_abs.rms',
            'func_mc_rel.rms',
            'func_mc_abs_mean.rms',
            'func_mc_rel_mean.rms',
        ]

        for f in files:
            assert_is_equal(
                f'{tmp}/{f}',
                f'{TESTDATA}/test_mcflirt_mc/{f}',
            )