#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
dHCP neonatal fMRI pipeline - main pipeline
"""
import logging
import os
import os.path as op
from typing import Optional
import pprint
import nibabel as nb

import numpy as np
from fsl.utils.filetree import FileTree

from dhcp.func import importdata, fieldmap as fmap, registration as reg, mcdc, ica, denoise
from dhcp.func.qc import Subject
from dhcp.util import util, mask
from dhcp.util.enums import PhaseEncodeDirection, FieldmapType
from dhcp.util.enums import SegType
from dhcp.util.io import Path
from dhcp.util import fslpy as fsl


class Pipeline:

    def __init__(self, subid, sesid, output_path):
        """
        Constructor for the Pipeline class.

        Args:
            subid:
            sesid:
            output_path:
            **kwargs:
        """

        # setup defaults
        self.defaults = FileTree.read(util.get_resource('dhcp-defaults.tree'), partial_fill=False).update(
            subid=subid,
            sesid=sesid,
            workdir=output_path,
        )

        # if individual subject filetree already exists, then load it.
        # the workdir is updated to the provided output_path in case this pipeline dir was copied from elsewhere
        if self.defaults.on_disk('filetree'):
            self.defaults = FileTree.load_json(self.defaults.get('filetree')).update(workdir=output_path)

        # setup logging
        logging.basicConfig(
            filename=str(self.defaults.get('log', make_dir=True)),
            format='[%(asctime)s - %(name)s.%(funcName)s ] %(levelname)s : %(message)s',
            level=logging.DEBUG
        )

    @property
    def subid(self):
        return self.defaults.variables['subid']

    @property
    def sesid(self):
        return self.defaults.variables['sesid']

    @property
    def output_path(self):
        return self.defaults.variables['workdir']

    def export_filetree(self):
        self.defaults.save_json(self.defaults.get('filetree'))

    def import_data(self,
                    scan_pma: float,
                    func: str,
                    func_echospacing: float,
                    func_pedir: PhaseEncodeDirection,
                    sbref: str,
                    sbref_echospacing: float,
                    sbref_pedir: PhaseEncodeDirection,
                    func_sliceorder: str,
                    T2w: str,
                    T2w_brainmask: str,
                    T2w_dseg: str,
                    T2w_dseg_type: SegType,
                    spinecho: str,
                    spinecho_echospacing: float,
                    spinecho_pedir: PhaseEncodeDirection,
                    spinecho_inplaneaccel: float = 1.0,
                    spinecho_epifactor: Optional[int] = None,
                    birth_ga: Optional[float] = None,
                    T1w: Optional[str] = None,
                    ):
        """
        Import the data for the dHCP neonatal fMRI pipeline.

        Args:
            spinecho_epifactor:
            spinecho_inplaneaccel:
            T2w_dseg_type:
            scan_pma:
            func:
            func_echospacing:
            func_pedir:
            sbref:
            sbref_echospacing:
            sbref_pedir:
            func_sliceorder:
            T2w:
            T2w_brainmask:
            T2w_dseg:
            spinecho:
            spinecho_echospacing:
            spinecho_pedir:
            birth_ga:
            T1w:

        Returns:

        """

        importdata.DEFAULTS = self.defaults

        # subject info
        importdata.import_info(
            subid=self.subid,
            sesid=self.sesid,
            scan_pma=scan_pma,
            birth_ga=birth_ga,
        )

        # import func
        importdata.import_func(
            func=func,
            sbref=sbref,
            func_slorder=func_sliceorder,
            func_echospacing=func_echospacing,
            func_pedir=func_pedir,
            sbref_echospacing=sbref_echospacing,
            sbref_pedir=sbref_pedir,
        )

        # import struct/anat
        importdata.import_struct(
            T2w=T2w,
            T1w=T1w,
            brainmask=T2w_brainmask,
            dseg=T2w_dseg,
            dseg_type=T2w_dseg_type,
        )

        # import spinecho
        importdata.import_spinecho(
            spinecho=spinecho,
            spinecho_echospacing=spinecho_echospacing,
            spinecho_pedir=spinecho_pedir,
            spinecho_epifactor=spinecho_epifactor,
            spinecho_inplaneaccel=spinecho_inplaneaccel,
        )

        self.export_filetree()

    def prepare_fieldmap(self,
                         fieldmap_source=FieldmapType.spin_echo_epi_derived,
                         pedir_n=2,
                         ):
        """
        Prepare fieldmap and register to native structural and native functional spaces.

        Args:
            fieldmap_source:
            pedir_n:

        Returns:

        """

        self.defaults.on_disk(['spinecho'], error=True)

        if fieldmap_source == FieldmapType.spin_echo_epi_derived:
            fmap.DEFAULTS = self.defaults

            spinecho = self.defaults.get('spinecho')
            meta = util.load_sidecar(spinecho)

            fmap.fieldmap(
                spinecho=spinecho,
                echospacing=meta['echo_spacing'],
                pedir=[PhaseEncodeDirection[d] for d in meta['phase_encode_dir']],
                pedir_n=pedir_n,
                inplaneaccel=meta.pop('inplane_accel', 1.0),
                epifactor=meta.pop('epi_factor', None),
            )
        elif fieldmap_source == FieldmapType.dual_echo_time_derived:
            raise RuntimeError('Dual-echo-time-derived fieldmap not currently supported')
        else:
            raise RuntimeError('Invalid fieldmap source')

        # fmap -> struct

        reg.DEFAULTS = self.defaults.update(src_space='fmap', ref_space='struct')

        # TODO: will need to turn BBR off if fmap is dual-echo
        reg.fmap_to_struct(
            fmap=self.defaults.get('fmap'),
            fmap_magnitude=self.defaults.get('fmap_magnitude'),
            fmap_brainmask=self.defaults.get('fmap_brainmask'),
            struct=self.defaults.get('T2w'),
            struct_brainmask=self.defaults.get('T2w_brainmask'),
            struct_boundarymask=self.defaults.get('T2w_wmmask'),
            do_bbr=True
        )

        # func -> sbref (distorted)

        reg.DEFAULTS = self.defaults.update(src_space='func', ref_space='sbref')

        reg.func_to_sbref(
            func=self.defaults.get('func0'),
            func_brainmask=self.defaults.get('func_brainmask'),
            sbref=self.defaults.get('sbref'),
            sbref_brainmask=self.defaults.get('sbref_brainmask')
        )

        # sbref -> struct (with BBR and DC)

        reg.DEFAULTS = self.defaults.update(src_space='sbref', ref_space='struct')

        sbref = self.defaults.get('sbref')
        sbref_meta = util.load_sidecar(sbref)

        reg.sbref_to_struct(
            sbref=sbref,
            sbref_brainmask=self.defaults.get('sbref_brainmask'),
            sbref_pedir=PhaseEncodeDirection[sbref_meta['phase_encode_dir']],
            sbref_echospacing=sbref_meta['echo_spacing'],
            struct=self.defaults.get('T2w'),
            struct_brainmask=self.defaults.get('T2w_brainmask'),
            struct_boundarymask=self.defaults.get('T2w_wmmask'),
            fmap=self.defaults.get('fmap'),
            fmap_brainmask=self.defaults.get('fmap_brainmask'),
            fmap2struct_xfm=self.defaults.update(src_space='fmap', ref_space='struct').get('affine'),
            do_bbr=True,
            do_dc=True,
        )

        # func (distorted) -> sbref -> struct (composite)

        reg.DEFAULTS = self.defaults.update(src_space='func', ref_space='struct')

        reg.func_to_struct_composite(
            func=self.defaults.get('func0'),
            struct=self.defaults.get('T2w'),
            func2sbref_affine=self.defaults.update(src_space='func', ref_space='sbref').get('affine'),
            sbref2struct_affine=self.defaults.update(src_space='sbref', ref_space='struct').get('affine'),
            sbref2struct_warp=self.defaults.update(src_space='sbref', ref_space='struct').get('warp'),
        )

        # fmap -> func (composite)

        reg.DEFAULTS = self.defaults.update(src_space='fmap', ref_space='func')

        reg.fmap_to_func_composite(
            fmap=self.defaults.get('fmap'),
            func=self.defaults.get('func0'),
            fmap2struct_affine=self.defaults.update(src_space='fmap', ref_space='struct').get('affine'),
            func2struct_invaffine=self.defaults.update(src_space='func', ref_space='struct').get('inv_affine'),
        )

        self.export_filetree()

    def mcdc(self):
        """
        Run motion and distortion correction.

        Returns:

        """

        func = self.defaults.get('func')
        meta = util.load_sidecar(func)

        assert 'echo_spacing' in meta, 'func sidecar must contain echo_spacing'
        assert 'phase_encode_dir' in meta, 'func sidecar must contain phase_encode_dir'

        self.defaults = self.defaults.update(dc='dc')

        args = {
            'func': func,
            'func_brainmask': self.defaults.get('func_brainmask'),
            'fmap': self.defaults.update(src_space='fmap', ref_space='func').get('resampled_image'),
            # 'fmap2func_affine': self.defaults.update(src_space='fmap', ref_space='func').get('affine'),
            'func_echospacing': meta['echo_spacing'],
            'func_pedir': PhaseEncodeDirection[meta['phase_encode_dir']],
            'func_slorder': self.defaults.get('func_slorder'),
        }

        mcdc.DEFAULTS = self.defaults
        mcdc.mcdc(**args)

        # func (undistorted) -> sbref (undistorted)

        reg.DEFAULTS = self.defaults.update(src_space='func-mcdc', ref_space='sbref-dc')

        reg.func_to_sbref(
            func=self.defaults.get('func_mcdc_mean'),
            func_brainmask=self.defaults.get('func_mcdc_brainmask'),
            sbref=self.defaults.update(src_space='sbref', ref_space='struct').get('dc_image'),
            sbref_brainmask=self.defaults.update(src_space='sbref', ref_space='struct').get('dc_brainmask')
        )

        # func (undistorted) -> sbref (undistorted) -> struct (composite)

        reg.DEFAULTS = self.defaults.update(src_space='func-mcdc', ref_space='struct')

        reg.func_to_struct_composite(
            func=self.defaults.get('func_mcdc_mean'),
            struct=self.defaults.get('T2w'),
            func2sbref_affine=self.defaults.update(src_space='func-mcdc', ref_space='sbref-dc').get('affine'),
            sbref2struct_affine=self.defaults.update(src_space='sbref', ref_space='struct').get('affine'),
            sbref2struct_warp=self.defaults.update(src_space='sbref', ref_space='struct').get('warp'),
        )

        self.export_filetree()

    def standard(self, standard_age=40, quick=False, atlas_tree=None):
        """
        Register standard atlas space to the native structural.

        Args:
            atlas_tree:
            quick:
            standard_age:

        Returns:

        """

        #  template -> struct

        assert os.getenv('DHCP_ANTS_PATH') is not None, 'DHCP_ANTS_PATH is not set. Are you sure ANTs is installed?'
        assert os.getenv('DHCP_C3D_PATH') is not None, 'DHCP_C3D_PATH is not set'
        assert util.has_setting('dhcp_volumetric_atlas_tree'), 'DHCP atlas is not set'

        if atlas_tree is None:
            atlas_tree = Path(util.get_setting('dhcp_volumetric_atlas_tree'))
            atlas = FileTree.read(atlas_tree).update(path=atlas_tree.dirname)

        scan_pma = util.json2dict(self.defaults.get('subject_info'))['scan_pma']
        age = int(np.round(scan_pma))

        reg.DEFAULTS = self.defaults.update(src_space=f'template-{age}', ref_space='struct')

        reg.template_to_struct(age=age, struct_brainmask=self.defaults.get('T2w_brainmask'), atlas=atlas,
                               struct_T1w=self.defaults.get('T1w') if self.defaults.on_disk('T1w') else None,
                               struct_T2w=self.defaults.get('T2w'), quick=quick)

        # struct -> age-matched template -> standard template (composite)

        reg.DEFAULTS = self.defaults.update(src_space='struct', ref_space='standard')

        reg.struct_to_template_composite(
            struct=self.defaults.get('T2w'),
            struct2template_warp=self.defaults.update(src_space=f'template-{age}', ref_space='struct').get(
                'inv_warp'),
            age=age,
            standard_age=standard_age,
            atlas=atlas,
        )

        # func (undistorted) -> struct -> age-matched template -> standard template (composite)

        reg.DEFAULTS = self.defaults.update(src_space='func-mcdc', ref_space='standard')

        reg.func_to_template_composite(
            func=self.defaults.get('func_mcdc_mean'),
            func2struct_affine=self.defaults.update(src_space='func-mcdc', ref_space='struct').get('affine'),
            struct2template_warp=self.defaults.update(src_space='struct', ref_space='standard').get('warp'),
            standard_age=standard_age,
            atlas=atlas,
        )

        self.export_filetree()

    def ica(self, temporal_fwhm=150.0, icadim=None):
        """
        Run single-subject ICA.

        Args:
            temporal_fwhm:
            icadim:

        Returns:

        """

        ica.DEFAULTS = self.defaults

        ica.ica(
            func=self.defaults.get('func_mcdc'),
            func_brainmask=self.defaults.get('func_mcdc_brainmask'),
            temporal_fwhm=temporal_fwhm,
            icadim=icadim,
        )
        util.update_sidecar(
            self.defaults.get('func_filt'),
            temporal_fwhm=temporal_fwhm,
        )

        self.export_filetree()

    def denoise(self, rdata=None, fix_threshold=None):
        """
        Run FIX-based denoising.

        Args:
            rdata:
            fix_threshold:

        Returns:

        """

        assert os.getenv('FSL_FIX_MATLAB_MODE') is not None, 'FSL_FIX_MATLAB_MODE is not set'
        assert os.getenv('MATLAB_BIN') is not None, 'MATLAB_BIN is not set'
        assert os.getenv('DHCP_FIX_PATH') is not None, 'DHCP_FIX_PATH is not set'

        denoise.DEFAULTS = self.defaults

        struct_dseg = self.defaults.get('T2w_dseg')
        dseg_type = SegType[util.load_sidecar(struct_dseg)['dseg_type']]

        func_filt = self.defaults.get('func_filt')
        temporal_fwhm = util.load_sidecar(func_filt)['temporal_fwhm']

        denoise.fix_extract(
            func_filt=func_filt,
            func_ref=self.defaults.get('func_mcdc_mean'),
            struct=self.defaults.get('T2w'),
            struct_brainmask=self.defaults.get('T2w_brainmask'),
            struct_dseg=struct_dseg,
            dseg_type=dseg_type,
            func2struct_mat=self.defaults.update(src_space='func-mcdc', ref_space='struct').get('affine'),
            mot_param=self.defaults.get('motparams'),
            icadir=self.defaults.get('icadir'),
            temporal_fwhm=temporal_fwhm,
        )

        if rdata is None:
            rdata = util.get_setting('dhcp_trained_fix', None)

        if fix_threshold is None:
            fix_threshold = util.get_setting('dhcp_trained_fix_threshold', None)

        if (rdata is not None) and (fix_threshold is not None):
            denoise.fix_classify(
                rdata=rdata,
                threshold=fix_threshold,
            )

            denoise.fix_apply(
                temporal_fwhm=temporal_fwhm,
            )

        self.export_filetree()

    def report(self, group_qc: Optional[str] = None):

        if group_qc is None:
            group_qc = Path(util.get_setting('dhcp_group_qc'))

        template = 'dhcp_individual_qc_report_template.html'
        qcdir = self.defaults.get('qcdir')

        qc0 = Subject.from_qcdir(qcdir)
        qc0.parse(template, self.defaults.get('qc_report', make_dir=True), group_json=group_qc)

    def qc(self,
           group_map: Optional[str] = None,
           standard_age: Optional[int] = 40,
           standard_res: Optional[float] = 1.5,
           atlas_tree: Optional[FileTree] = None,
           ):

        if group_map is None:
            group_map = Path(util.get_setting('dhcp_group_maps'))

        # setup logging
        logger = logging.getLogger()
        logger.info('start')
        logger.debug(pprint.pformat(locals(), indent=2))

        subject_filetree = self.defaults
        # qc = Subject.from_qcdir(subject_filetree.get('qcdir', make_dir=True))
        qc = Subject(subject_filetree.get('qcdir', make_dir=True))

        ##############
        # add subject info
        ##############

        scan_pma = util.json2dict(subject_filetree.get('subject_info'))['scan_pma']
        birth_ga = util.json2dict(subject_filetree.get('subject_info'))['birth_ga']
        qc.add_subjectinfo(subject_id=self.subid, session_id=self.sesid, scan_age=scan_pma, birth_age=birth_ga)

        ##############
        # add fmap
        ##############

        if subject_filetree.on_disk(['fmap', 'fmap_magnitude'], error=True):

            if subject_filetree.on_disk('spinecho', error=False):
                spinecho = subject_filetree.get('spinecho')
            else:
                spinecho = None

            logger.info('Add fieldmap to QC')
            qc.add_fmap(
                label='fmap',
                fmap=subject_filetree.get('fmap'),
                fmap_mag=subject_filetree.get('fmap_magnitude'),
                fmap_brainmask=subject_filetree.get('fmap_brainmask'),
                spinecho=spinecho
            )
        else:
            logger.warning('Fieldmap not found')

        ##############
        # registration: fmap2struct
        ##############

        struct_brainmask = Path(subject_filetree.get('qcdir')).join(f'struct_brainmask.nii.gz')
        mask.create_mask(
            dseg=subject_filetree.get('T2w_dseg'),
            dseg_type=SegType.drawem,
            labels=['wm', 'gm', 'sc', 'cb', 'bs'],
            outname=struct_brainmask,
        )

        wd0 = subject_filetree.update(src_space='fmap', ref_space='struct')

        if wd0.on_disk(['T2w', 'resampled_image', 'T2w_brainmask', 'T2w_wmmask', 'T2w_dseg'], error=False):
            logger.info('Add fieldmap2struct to QC')
            qc.add_reg(
                label='fmap2struct',
                source=wd0.get('resampled_image'),
                ref=wd0.get('T2w'),
                ref_brainmask=struct_brainmask,
                ref_boundarymask=wd0.get('T2w_wmmask'),
                ref_dseg=wd0.get('T2w_wmmask')
            )
        else:
            logger.warning('fmap2struct not added')

        ##############
        # registration: sbref2struct
        ##############

        wd0 = subject_filetree.update(src_space='sbref', ref_space='struct')

        if wd0.on_disk(['T2w', 'resampled_image', 'T2w_brainmask', 'T2w_wmmask', 'T2w_dseg'], error=False):
            logger.info('Add sbref2struct to QC')
            qc.add_reg(
                label='sbref2struct',
                source=wd0.get('resampled_image'),
                ref=wd0.get('T2w'),
                ref_brainmask=struct_brainmask,
                ref_boundarymask=wd0.get('T2w_wmmask'),
                ref_dseg=wd0.get('T2w_wmmask')
            )
        else:
            logger.warning('sbref2struct not added')

        ##############
        # registration: func2sbref
        ##############

        wd0 = subject_filetree.update(src_space='func', ref_space='sbref')

        if wd0.on_disk(['sbref', 'resampled_image', 'sbref_brainmask'], error=False):
            logger.info('Add func2sbref to QC')
            qc.add_reg(
                label='func2sbref',
                source=wd0.get('resampled_image'),
                ref=wd0.get('sbref'),
                ref_brainmask=wd0.get('sbref_brainmask')
            )
        else:
            logger.warning('func2sbref not added')

        ##############
        # registration: func-mcdc2sbref
        ##############

        wd0 = subject_filetree.update(src_space='func-mcdc', ref_space='sbref-dc')

        if wd0.on_disk(['sbref', 'resampled_image', 'sbref_brainmask'], error=False):
            logger.info('Add func-mcdc2sbref to QC')
            qc.add_reg(
                label='func-mcdc2sbref',
                source=wd0.get('resampled_image'),
                ref=wd0.get('sbref'),
                ref_brainmask=wd0.get('sbref_brainmask')
            )
        else:
            logger.warning('func-mcdc2sbref not added')

        ##############
        # registration: template2struct
        ##############

        scan_pma = int(np.round(scan_pma))
        wd0 = subject_filetree.update(src_space=f'template-{scan_pma}', ref_space='struct')

        if wd0.on_disk(['T2w', 'T2w_brainmask', 'T2w_wmmask', 'T2w_dseg', 'resampled_image'], error=False):
            logger.info('Add template2struct to QC')
            qc.add_reg(
                label='template2struct',
                source=wd0.get('resampled_image'),
                ref=wd0.get('T2w'),
                ref_brainmask=struct_brainmask,
                ref_boundarymask=wd0.get('T2w_wmmask'),
                ref_dseg=wd0.get('T2w_wmmask')
            )
        else:
            logger.warning('template2struct not added')

        ##############
        # Prepare standard space
        ##############

        # resample standard and standard_dseg to specified resolution (typically down sampled)

        # TODO: use atlas *.tree
        # atlas = Path(os.getenv('DHCP_ATLAS', ''))
        if atlas_tree is None:
            atlas_tree = Path(util.get_setting('dhcp_volumetric_atlas_tree'))
            atlas = FileTree.read(atlas_tree).update(path=atlas_tree.dirname)

        standard = atlas.update(age=int(standard_age)).get('template_T2')
        if op.exists(standard):
            fsl.flirt(
                src=standard,
                ref=standard,
                applyisoxfm=standard_res,
                init=Path(os.getenv('FSLDIR', '')).join('etc/flirtsch/ident.mat'),
                out=Path(subject_filetree.get('qcdir')).join(f'standard-{standard_res}mm.nii.gz'),
                interp='spline'
            )
            standard = nb.load(Path(subject_filetree.get('qcdir')).join(f'standard-{standard_res}mm.nii.gz'))
        else:
            standard = None

        standard_dseg = atlas.update(age=int(standard_age)).get('dseg')
        if op.exists(standard_dseg):
            fsl.flirt(
                src=standard_dseg,
                ref=standard_dseg,
                applyisoxfm=standard_res,
                init=Path(os.getenv('FSLDIR', '')).join('etc/flirtsch/ident.mat'),
                out=Path(subject_filetree.get('qcdir')).join(f'standard-dseg-{standard_res}mm.nii.gz'),
                interp='nearestneighbour'
            )
            standard_dseg = nb.load(
                Path(subject_filetree.get('qcdir')).join(f'standard-dseg-{standard_res}mm.nii.gz'))
        else:
            standard_dseg = None

        # load warps

        wd0 = subject_filetree.update(src_space='func-mcdc', ref_space='standard')

        if wd0.on_disk('warp', error=False):
            func2standard = wd0.get('warp')
        else:
            func2standard = None

        if wd0.on_disk('inv_warp', error=False):
            standard2func = wd0.get('inv_warp')
        else:
            standard2func = None

        ##############
        # func: raw
        ##############

        if subject_filetree.on_disk(['func', 'func_brainmask'], error=False):
            logger.info('Add func_raw to QC')
            qc.add_func(
                func=subject_filetree.get('func'),
                label='raw',
                brainmask=subject_filetree.get('func_brainmask'),
                standard=standard,
                func2standard_warp=func2standard,
                template=group_map,
                template2func_warp=standard2func,
                template_dseg=standard_dseg,
                template_dseg_labels=mask.get_dseg_labels(SegType.drawem),
            )
        else:
            logger.warning('func_raw not added')

        ##############
        # func: mcdc
        ##############

        if subject_filetree.on_disk(['func_mcdc', 'func_mcdc_brainmask'], error=False):
            logger.info('Add func_mcdc to QC')
            qc.add_func(
                func=subject_filetree.get('func_mcdc'),
                label='mcdc',
                brainmask=wd0.get('func_mcdc_brainmask'),
                standard=standard,
                func2standard_warp=func2standard,
                template=group_map,
                template2func_warp=standard2func,
                template_dseg=standard_dseg,
                template_dseg_labels=mask.get_dseg_labels(SegType.drawem),
            )
        else:
            logger.warning('func_mcdc not added')

        ##############
        # func: clean
        ##############

        if subject_filetree.on_disk(['func_clean', 'func_mcdc_brainmask'], error=False):
            logger.info('Add func_clean to QC')
            qc.add_func(
                func=subject_filetree.get('func_clean'),
                label='clean',
                brainmask=wd0.get('func_mcdc_brainmask'),
                standard=standard,
                func2standard_warp=func2standard,
                template=group_map,
                template2func_warp=standard2func,
                template_dseg=standard_dseg,
                template_dseg_labels=mask.get_dseg_labels(SegType.drawem),
            )
        else:
            logger.warning('func_clean not added')

        ##############
        # motion parameters
        ##############

        if subject_filetree.on_disk('motparams', error=False):
            logger.info('Add motion parameters to QC')
            qc.add_motparam('motparam', motparams=subject_filetree.get('motparams'))
        else:
            logger.warning('motion parameters not added')

        ##############
        # FIX
        ##############

        if subject_filetree.on_disk(['melodic/melodic_IC', 'melodic/melodic_mix'], error=False):

            if subject_filetree.on_disk('fix_labels', error=False):
                labels = subject_filetree.get('fix_labels')
            else:
                labels = None

            logger.info('Add FIX to QC')

            qc.add_fix(
                label='fix',
                ic=subject_filetree.get('melodic/melodic_IC'),
                mix=subject_filetree.get('melodic/melodic_mix'),
                labels=labels
            )

        else:
            logger.warning('FIX/ICA not found')

    def surface(self):
        assert os.getenv('DHCP_WBCMD') is not None, 'DHCP_WBCMD is not set'

        # TODO: create subcortical mask
        # TODO: run surface

        # subid = sys.argv[1]
        # sesid = sys.argv[2]
        #
        # outpath = Path(f'/vol/dhcp-results/neofmri_2nd_release_rerun2/surface/sub-{subid}/ses-{sesid}')
        #
        # if not op.exists(outpath.join('import')):
        #     os.makedirs(outpath.join('import'))
        #
        # mask.create_mask(
        #     dseg=f'../sub-{subid}/ses-{sesid}/import/T2w_dseg.nii.gz',
        #     dseg_type=enums.SegType.drawem,
        #     labels=['sc', 'bs', 'cb'],
        #     outname=outpath.join(f'import/T2w_scmask.nii.gz')
        # )
        #
        # derived_data = Path('/vol/dhcp-derived-data/derived_02Jun2018/derivatives')
        #
        # if not op.exists(outpath.join('surface')):
        #     os.makedirs(outpath.join('surface'))
        #
        # surfdir = derived_data.join(f'sub-{subid}/ses-{sesid}/anat/Native/')
        #
        # os.environ['DHCP_WBCMD'] = '/vol/dhcp-results/bin/workbench/bin_rh_linux64/wb_command'
        #
        # surface(
        #     white_surf_right=surfdir.join(f'sub-{subid}_ses-{sesid}_right_white.surf.gii'),
        #     white_surf_left=surfdir.join(f'sub-{subid}_ses-{sesid}_left_white.surf.gii'),
        #     pial_surf_right=surfdir.join(f'sub-{subid}_ses-{sesid}_right_pial.surf.gii'),
        #     pial_surf_left=surfdir.join(f'sub-{subid}_ses-{sesid}_left_pial.surf.gii'),
        #     mid_surf_right=surfdir.join(f'sub-{subid}_ses-{sesid}_right_midthickness.surf.gii'),
        #     mid_surf_left=surfdir.join(f'sub-{subid}_ses-{sesid}_left_midthickness.surf.gii'),
        #     medialwall_shape_left=surfdir.join(f'sub-{subid}_ses-{sesid}_left_roi.shape.gii'),
        #     medialwall_shape_right=surfdir.join(f'sub-{subid}_ses-{sesid}_right_roi.shape.gii'),
        #     struct_to_func_xfm=f'../sub-{subid}/ses-{sesid}/reg/func-mcdc2struct_invaffine.mat',
        #     func=f'../sub-{subid}/ses-{sesid}/mcdc/func_mcdc.nii.gz',
        #     func_brainmask=f'../sub-{subid}/ses-{sesid}/mcdc/func_mcdc_brainmask.nii.gz',
        #     struct=f'../sub-{subid}/ses-{sesid}/import/T2w.nii.gz',
        #     workdir=outpath.join('surface'),
        #     subcortical_mask=f'../sub-{subid}/ses-{sesid}/import/T2w_scmask.nii.gz',
        #     do_sigloss=True
        # )

    def pre_mcdc(self,
                 fieldmap_source=FieldmapType.spin_echo_epi_derived,
                 pedir_n=2
                 ):
        """
        Run all pre-motion-and-distortion-correction stages of the dHCP neonatal fMRI pipeline.

        Args:
            fieldmap_source:
            pedir_n:

        Returns:

        """

        self.prepare_fieldmap(
            fieldmap_source=fieldmap_source,
            pedir_n=pedir_n
        )

    def post_mcdc(self,
                  standard_age: int = 40,
                  temporal_fwhm: float = 150,
                  icadim: Optional[int] = None,
                  rdata: Optional[str] = None,
                  fix_threshold: Optional[int] = None,
                  group_map: Optional[str] = None,
                  standard_res: Optional[float] = 1.5,
                  group_qc: Optional[str] = None,
                  ):
        """
        Run all post-motion-and-distortion-correction stages of the dHCP neonatal fMRI pipeline.

        Returns:

        """
        self.standard(standard_age=standard_age)
        self.ica(
            temporal_fwhm=temporal_fwhm,
            icadim=icadim,
        )
        self.denoise(
            rdata=rdata,
            fix_threshold=fix_threshold,
        )
        self.qc(
            standard_res=standard_res,
            standard_age=standard_age,
            group_map=group_map
        )
        self.report(
            group_qc=group_qc,
        )

    def run_all(self,
                fieldmap_source=FieldmapType.spin_echo_epi_derived,
                pedir_n=2,
                standard_age: int = 40,
                temporal_fwhm: float = 150,
                icadim: Optional[int] = None,
                rdata: Optional[str] = None,
                fix_threshold: Optional[int] = None,
                group_map: Optional[str] = None,
                standard_res: Optional[float] = 1.5,
                group_qc: Optional[str] = None,
                ):
        """
        Run all stages of the dHCP neonatal fMRI pipeline.

        Args:
            fieldmap_source:
            pedir_n:
            standard_age:
            temporal_fwhm:
            icadim:
            rdata:
            fix_threshold:

        Returns:

        """
        self.prepare_fieldmap(
            fieldmap_source=fieldmap_source,
            pedir_n=pedir_n
        )
        self.mcdc()
        self.standard(standard_age=standard_age)
        self.ica(
            temporal_fwhm=temporal_fwhm,
            icadim=icadim,
        )
        self.denoise(
            rdata=rdata,
            fix_threshold=fix_threshold,
        )
        self.qc(
            standard_res=standard_res,
            standard_age=standard_age,
            group_map=group_map
        )
        self.report(
            group_qc=group_qc,
        )
