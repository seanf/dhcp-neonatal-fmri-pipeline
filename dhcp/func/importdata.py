#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Import data stage of the dHCP neonatal fMRI pipeline.
"""
import logging
import os.path as op
import pprint
import shutil
from tempfile import TemporaryDirectory
from typing import Optional, List
import nibabel as nib

from fsl.utils.filetree import FileTree

from dhcp.func.mcdc import motion_outlier
from dhcp.util import util, fslpy as fsl, mask
from dhcp.util.enums import SegType, PhaseEncodeDirection, PhaseUnits, FieldmapType
from dhcp.util.io import Path
from dhcp.util.util import validate_path_input

DEFAULTS = FileTree.read(util.get_resource('importdata.tree'), partial_fill=False)


@util.timeit
def import_info(subid: str,
                scan_pma: float,
                sesid: Optional[str] = None,
                birth_ga: Optional[float] = None,
                workdir: Optional[str] = None,
                **kwargs):
    """
    Import subject information.

    Args:
        subid:
        scan_pma:
        sesid:
        birth_ga:
        workdir:
        **kwargs:

    Returns:

    """
    # setup logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    outputs.defines(['subject_info'], error=True)

    info_fname = Path(outputs.get('subject_info', make_dir=True))

    info = util.json2dict(info_fname) if info_fname.exists() else {}
    info = {**info, 'subid': subid, 'sesid': sesid, 'birth_ga': birth_ga, 'scan_pma': scan_pma, **kwargs}
    util.dict2json(info, info_fname)

    logger.info('end')


@util.timeit
def import_func(func: str,
                func_echospacing: float,
                func_pedir: PhaseEncodeDirection,
                func_brainmask: Optional[str] = None,
                func_slorder: Optional[str] = None,
                sbref: Optional[str] = None,
                sbref_brainmask: Optional[str] = None,
                sbref_echospacing: Optional[float] = None,
                sbref_pedir: Optional[PhaseEncodeDirection] = None,
                workdir: Optional[str] = None,
                mask_func=False):
    """
    Import functional EPI into pipeline, along with appropriate (optional) slice order file and sbref.

    Args:
        mask_func:
        func:
        func_brainmask:
        func_slorder:
        sbref:
        sbref_brainmask:
        workdir:

    Returns:

    """

    # setup logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    output_keys = [
        'func_mean', 'func', 'func_brainmask', 'func_slorder', 'sbref',
        'sbref_brainmask', 'func_std', 'func_tsnr'
    ]
    outputs.defines(output_keys, error=True)

    # import functional EPI
    func = validate_path_input(func, ndim=4)
    util.copy_zip_orient(func, outputs.get('func', make_dir=True))
    # nib.as_closest_canonical(nib.load(func)).to_filename(outputs.get('func', make_dir=True))
    util.update_sidecar(
        Path(outputs.get('func', make_dir=True)),
        echo_spacing=func_echospacing,
        phase_encode_dir=func_pedir.name,
    )

    # create the func_mean
    fsl.fslmaths(outputs.get('func')).Tmean().run(outputs.get('func_mean', make_dir=True))
    fsl.fslroi(outputs.get('func'), outputs.get('func0'), tmin=0, tsize=1)

    # import or create the func_brainmask
    if func_brainmask is not None:
        func_brainmask = validate_path_input(func_brainmask, ndim=3)
        util.copy_zip_orient(func_brainmask, outputs.get('func_brainmask', make_dir=True))
        # nib.as_closest_canonical(nib.load(func_brainmask)).to_filename(outputs.get('func_brainmask', make_dir=True))
    else:
        with TemporaryDirectory(dir=Path(outputs.get('func')).dirname) as tmp:
            brain = op.join(tmp, 'brain.nii.gz')
            fsl.bet(outputs.get('func_mean'), brain, fracintensity=0.4, mask=False)
            fsl.fslmaths(brain).bin().run(outputs.get('func_brainmask'))

    # create dilated brainmask
    fsl.fslmaths(outputs.get('func_brainmask')).dilM().dilM().bin().run(outputs.get('func_dil_brainmask'))

    # mask the func (with dilated brainmask) to reduce file size
    if mask_func:
        fsl.fslmaths(outputs.get('func')).mas(outputs.get('func_dil_brainmask')).run()

    # create func_std and func_tsnr
    fsl.fslmaths(outputs.get('func')).Tstd().run(outputs.get('func_std', make_dir=True))
    fsl.fslmaths(outputs.get('func_mean')).div(outputs.get('func_std')).run(outputs.get('func_tsnr', make_dir=True))

    # calculate motion metrics and outliers
    motion_outlier(
        outputs.get('func'),
        metric_name=outputs.get('func_metrics', make_dir=True)
    )

    if func_slorder is not None:
        func_slorder = validate_path_input(func_slorder, test_nifti=False)
        shutil.copy(func_slorder, outputs.get('func_slorder', make_dir=True))

    if sbref is not None:

        if sbref_pedir is None:
            raise RuntimeError('sbref phase encode direction is required')

        if sbref_echospacing is None:
            raise RuntimeError('sbref echo-spacing is required')

        sbref = validate_path_input(sbref, ndim=3)
        util.copy_zip_orient(sbref, outputs.get('sbref', make_dir=True))
        # nib.as_closest_canonical(nib.load(sbref)).to_filename(outputs.get('sbref', make_dir=True))
        util.update_sidecar(
            Path(outputs.get('sbref', make_dir=True)),
            echo_spacing=sbref_echospacing,
            phase_encode_dir=sbref_pedir.name,
        )

        # import or create the sbref_brainmask
        if sbref_brainmask is not None:
            sbref_brainmask = validate_path_input(sbref_brainmask, ndim=3)
            util.copy_zip_orient(sbref_brainmask, outputs.get('sbref_brainmask', make_dir=True))
            # nib.as_closest_canonical(nib.load(sbref_brainmask)).to_filename(outputs.get('sbref_brainmask',
            #                                                                             make_dir=True))
        else:
            with TemporaryDirectory(dir=Path(outputs.get('func')).dirname) as tmp:
                brain = op.join(tmp, 'brain.nii.gz')
                fsl.bet(outputs.get('sbref'), brain, fracintensity=0.4, mask=False)
                fsl.fslmaths(brain).bin().run(outputs.get('sbref_brainmask'))

    logger.info('end')


@util.timeit
def import_struct(T2w: str,
                  brainmask: str,
                  dseg: str,
                  dseg_type: Optional[SegType] = SegType.drawem,
                  probseg: Optional[str] = None,
                  probseg_type: Optional[SegType] = SegType.drawem,
                  wmmask: Optional[str] = None,
                  T1w: Optional[str] = None,
                  workdir: Optional[str] = None):
    """
    Import structural/anatomical image into pipeline

    Args:
        probseg:
        probseg_type:
        T2w:
        brainmask:
        dseg:
        dseg_type:
        wmmask:
        T1w:
        workdir:

    Returns:

    """
    # setup logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    output_keys = [
        'T1w', 'T2w', 'T2w_brainmask', 'T2w_dseg', 'T2w_wmmask'
    ]
    outputs.defines(output_keys, error=True)

    # import structural/anatomical images

    T2w = validate_path_input(T2w, ndim=3)
    util.copy_zip_orient(T2w, outputs.get('T2w', make_dir=True))

    brainmask = validate_path_input(brainmask, ndim=3)
    util.copy_zip_orient(brainmask, outputs.get('T2w_brainmask', make_dir=True))

    dseg = validate_path_input(dseg, ndim=3)
    util.copy_zip_orient(dseg, outputs.get('T2w_dseg', make_dir=True))
    util.update_sidecar(
        Path(outputs.get('T2w_dseg', make_dir=True)),
        seg_type=dseg_type.name
    )

    if probseg is not None:
        probseg = validate_path_input(probseg, ndim=4)
        util.copy_zip_orient(dseg, outputs.get('T2w_probseg', make_dir=True))
        util.update_sidecar(
            Path(outputs.get('T2w_probseg', make_dir=True)),
            seg_type=probseg_type.name
        )

    if wmmask is not None:
        wmmask = validate_path_input(wmmask, ndim=3)
        util.copy_zip_orient(wmmask, outputs.get('T2w_wmmask', make_dir=True))
    else:
        mask.create_mask(
            dseg=outputs.get('T2w_dseg'),
            dseg_type=dseg_type,
            labels=['wm'],
            outname=outputs.get('T2w_wmmask', make_dir=True)
        )

    if T1w is not None:
        T1w = validate_path_input(T1w, ndim=3)
        util.copy_zip_orient(T1w, outputs.get('T1w', make_dir=True))

    logger.info('end')


@util.timeit
def import_fieldmap(fieldmap: str,
                    fieldmap_magnitude: str,
                    fieldmap_units: PhaseUnits,
                    fieldmap_type: FieldmapType,
                    fieldmap_brainmask: Optional[str] = None,
                    workdir: Optional[str] = None):
    """
    Import fieldmap and magnitude image.

    Args:
        fieldmap_brainmask:
        fieldmap:
        fieldmap_magnitude:
        fieldmap_units:
        workdir:

    Returns:

    """
    # setup logging
    logger = logging.getLogger(__name__)
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # import phase
    fieldmap = validate_path_input(fieldmap, ndim=3)
    util.copy_zip_orient(fieldmap, outputs.get('fmap', make_dir=True))

    if fieldmap_units == PhaseUnits.hz:
        fsl.fslmaths(outputs.get('fmap')).mul(6.2832).run()
        fieldmap_units = PhaseUnits.rads

    util.update_sidecar(
        Path(outputs.get('fmap', make_dir=True)),
        units=fieldmap_units.name,
        type=fieldmap_type.name,
    )

    # import magnitude
    fieldmap_magnitude = validate_path_input(fieldmap_magnitude, ndim=3)
    util.copy_zip_orient(fieldmap_magnitude, outputs.get('fmap_magnitude', make_dir=True))

    # import or create the fieldmap_brainmask
    if fieldmap_brainmask is not None:
        fieldmap_brainmask = validate_path_input(fieldmap_brainmask, ndim=3)
        util.copy_zip_orient(fieldmap_brainmask, outputs.get('fmap_brainmask', make_dir=True))
    else:
        with TemporaryDirectory(dir=Path(outputs.get('fmap')).dirname) as tmp:
            brain = op.join(tmp, 'brain.nii.gz')
            fsl.bet(outputs.get('fmap_magnitude'), brain, fracintensity=0.4, mask=False)
            fsl.fslmaths(brain).bin().run(outputs.get('fmap_brainmask'))



@util.timeit
def import_spinecho(spinecho: str,
                    spinecho_echospacing: float,
                    spinecho_pedir: List[PhaseEncodeDirection],
                    spinecho_inplaneaccel: Optional[float] = 1.0,
                    spinecho_epifactor: Optional[int] = None,
                    workdir: Optional[str] = None):
    """
    Import spinecho blip-up blip-down (reversed phase encode direction) image to pipeline.

    Args:
        spinecho:
        workdir:

    Returns:

    """

    # setup logging
    logger = logging.getLogger(__name__)
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    outputs.defines(['spinecho'], error=True)

    # Import spinecho blip-up blip-down image into pipeline
    logger.debug("test inputs")
    spinecho = validate_path_input(spinecho, ndim=4)

    logger.debug("copy zip reorient")
    util.copy_zip_orient(spinecho, outputs.get('spinecho', make_dir=True))
    # nib.as_closest_canonical(nib.load(spinecho)).to_filename(outputs.get('spinecho', make_dir=True))

    util.update_sidecar(
        outputs.get('spinecho', make_dir=True),
        echo_spacing=spinecho_echospacing,
        phase_encode_dir=[d.name for d in spinecho_pedir],
        epi_factor=spinecho_epifactor,
        inplane_accel=spinecho_inplaneaccel,
    )

    logger.info('end')

@util.timeit
def import_surfaces(white_surf_right: str,
                    white_surf_left: str,
                    pial_surf_right: str,
                    pial_surf_left: str,
                    midthickness_surf_right: str,
                    midthickness_surf_left: str,
                    inflated_surf_right: str,
                    inflated_surf_left: str,
                    veryinflated_surf_right: str,
                    veryinflated_surf_left: str,
                    sphere_surf_right: str,
                    sphere_surf_left: str,
                    medialwall_shape_left: str,
                    medialwall_shape_right: str,
                    workdir: Optional[str] = None,
                    mesh='native',
                    space='T2w'
                    ):

    # setup logging
    logger = logging.getLogger(__name__)
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    for anat in ['white', 'pial', 'midthickness', 'inflated', 'veryinflated', 'sphere']:
        for hemi in ['left', 'right']:
            shutil.copy(
                locals()[f'{anat}_surf_{hemi}'],
                outputs.update(mesh=mesh, space=space, hemi=hemi).get(f'surf_{anat}', make_dir=True)
            )

    shutil.copy(
        medialwall_shape_left,
        outputs.update(mesh=mesh, space=space, hemi='left').get('shape_medialwall', make_dir=True)
    )

    shutil.copy(
        medialwall_shape_right,
        outputs.update(mesh=mesh, space=space, hemi='right').get('shape_medialwall', make_dir=True)
    )


