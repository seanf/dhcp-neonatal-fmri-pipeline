#!/usr/bin/env python
"""
    DHCP pipeline - Tools for FIX denoising.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os
import os.path as op
import shutil
from typing import Optional

import numpy as np
import pandas as pd
from fsl.utils.filetree import FileTree
from fsl.data import fixlabels

import dhcp.util.fslpy as fsl
import dhcp.util.image as img
import dhcp.util.shellops as shops
import dhcp.util.util as util
from dhcp.func.ica import ica
from dhcp.util import mask
from dhcp.util.enums import SegType
from dhcp.util.util import timeit
from dhcp.util.io import Path

DEFAULTS = FileTree.read(util.get_resource('denoise.tree'), partial_fill=False)


@timeit
def fix_extract(func_filt: str,
                func_ref: str,
                struct: str,
                struct_brainmask: str,
                struct_dseg: str,
                dseg_type: SegType,
                func2struct_mat: str,
                mot_param: str,
                icadir: Optional[str] = None,
                workdir: Optional[str] = None,
                func_tr: Optional[float] = None,
                temporal_fwhm: Optional[float] = 150,
                ):
    """
    FIX: extract features
    :param func_filt:
    :param func_ref:
    :param struct:
    :param struct_brainmask:
    :param struct_dseg:
    :param dseg_type:
    :param func2struct_mat:
    :param mot_param:
    :param icadir:
    :param workdir:
    :param func_tr:
    :param temporal_fwhm:
    :return:
    """

    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    workdir = outputs.get('fixdir', make_dir=True)
    if not op.exists(workdir):
        os.makedirs(workdir)

    # TODO: add input validation
    func_filt = util.validate_path_input(func_filt, ndim=4)
    func_ref = util.validate_path_input(func_ref)
    struct = util.validate_path_input(struct)
    struct_brainmask = util.validate_path_input(struct_brainmask)
    struct_dseg = util.validate_path_input(struct_dseg)

    # setup fake fix dir

    util.rel_sym_link(func_filt, op.join(workdir, "filtered_func_data.nii.gz"))

    if icadir is None:
        icadir = op.join(workdir, "filtered_func_data.ica")
        ica(func=func_filt,
            func_tr=func_tr,
            temporal_fwhm=temporal_fwhm,
            workdir=icadir)
    else:
        util.rel_sym_link(icadir, op.join(workdir, "filtered_func_data.ica"))

    mcdir = op.join(workdir, 'mc')
    if not op.exists(mcdir):
        os.makedirs(mcdir)

    mp = pd.read_csv(mot_param, delimiter='\t', index_col=None)
    np.savetxt(
        op.join(mcdir, 'prefiltered_func_data_mcf.par'),
        mp[['RotX', 'RotY', 'RotZ', 'X', 'Y', 'Z']].values,
        fmt='%0.6f',
    )

    mean_func = op.join(workdir, 'mean_func.nii.gz')
    fsl.fslmaths(input=func_filt).Tmean().run(output=mean_func)

    fixregdir = op.join(workdir, 'reg')
    if not op.exists(fixregdir):
        os.makedirs(fixregdir)
    util.rel_sym_link(func_ref, op.join(fixregdir, 'example_func.nii.gz'))

    fsl.fslmaths(struct).mul(struct_brainmask).run(
        op.join(fixregdir, 'highres.nii.gz'))

    fsl.invxfm(
        inmat=func2struct_mat,
        omat=op.join(fixregdir, 'highres2example_func.mat'))

    funcmask = op.join(workdir, 'mask.nii.gz')
    fsl.applyxfm(
        src=struct_brainmask,
        ref=op.join(fixregdir, 'example_func.nii.gz'),
        mat=op.join(fixregdir, 'highres2example_func.mat'),
        out=funcmask)
    fsl.fslmaths(input=funcmask).thr(0.5).bin().run()

    mask.convert_to_fsl_fast(
        dseg=struct_dseg,
        type=dseg_type,
        outname=op.join(fixregdir, "highres_pveseg.nii.gz")
    )

    # extract FIX

    fixsrc = op.join(os.getenv('DHCP_FIX_PATH', ''), 'fix')

    cmd = "{0} -f {1}".format(fixsrc, workdir)
    shops.run(cmd)


def _classify(fixdir, rdata, threshold, fix_src):
    cmd = f'{fix_src} -c {fixdir} {rdata} {threshold}'
    try:
        shops.run(cmd)
    except:
        with open(fixdir+'/.fix_2b_predict.log', 'r') as f:
            log = f.read()
        raise RuntimeError(log)

@timeit
def fix_classify(rdata: str,
                 threshold: int,
                 workdir: Optional[str] = None):
    """
    FIX: classify features

    Args:
        workdir:
        rdata:
        threshold:

    Returns:

    """

    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    fixdir = outputs.get('fixdir', make_dir=True)
    if not op.exists(fixdir):
        os.makedirs(fixdir)

    fix_src = op.join(os.getenv('DHCP_FIX_PATH', ''), 'fix')

    # check inputs
    rdata = util.validate_path_input(rdata, test_nifti=False)

    # FIX classify
    _classify(fixdir, rdata, threshold, fix_src)

    # rename FIX labels
    labels = f'fix4melview_{img.basename(rdata)}_thr{threshold}.txt'
    shutil.copy(op.join(fixdir, labels), outputs.get('fix_labels'))

    # create fix_regressors.tsv
    noise_idx = fixlabels.loadLabelFile(outputs.get('fix_labels'), returnIndices=True)[2]
    noise_idx = np.array(noise_idx)-1
    mix = np.loadtxt(op.join(fixdir, 'filtered_func_data.ica/melodic_mix'))[:, noise_idx]
    df = pd.DataFrame(mix, columns=[f'noise_{i}' for i in noise_idx])
    df.to_csv(outputs.get('fix_regressors'), sep='\t', index=None)

@timeit
def fix_apply(workdir: Optional[str] = None,
              temporal_fwhm: Optional[float] = 150.0):
    """
    FIX: regress noise ICs from data

    Args:
        workdir:
        temporal_fwhm:

    Returns:

    """

    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    fixdir = outputs.get('fixdir', make_dir=True)
    if not op.exists(fixdir):
        os.makedirs(fixdir)

    fix_src = op.join(os.getenv('DHCP_FIX_PATH', ''), 'fix')

    labels = outputs.get('fix_labels')

    # fix apply
    cmd = f'{fix_src} -a {labels} -m -h {temporal_fwhm}'
    shops.run(cmd)

    #
    func_clean = outputs.get('func_clean', make_dir=True)
    func_mean = outputs.get('func_clean_mean', make_dir=True)
    func_stdev = outputs.get('func_clean_std', make_dir=True)
    func_tsnr = outputs.get('func_clean_tsnr', make_dir=True)

    util.rel_sym_link(
        target=op.join(fixdir, 'filtered_func_data_clean.nii.gz'),
        linkname=func_clean,
    )

    fsl.fslmaths(func_clean).Tmean().run(func_mean)
    fsl.fslmaths(func_clean).Tstd().run(func_stdev)
    fsl.fslmaths(func_mean).div(func_stdev).run(func_tsnr)
