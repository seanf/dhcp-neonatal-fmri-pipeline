#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Single-subject ICA for the dHCP neonatal fMRI pipeline
"""
import logging
import os.path as op
import pprint
from tempfile import TemporaryDirectory
from typing import Optional

import numpy as np
from fsl.utils.filetree import FileTree

import dhcp.util.assertions as assrt
import dhcp.util.fslpy as fsl
import dhcp.util.shellops as shops
import dhcp.util.util as util
from dhcp.util.util import timeit

DEFAULTS = FileTree.read(util.get_resource('ica.tree'), partial_fill=False)


@timeit
def ica(func: str,
        func_brainmask: str,
        func_tr: Optional[float] = None,
        temporal_fwhm: Optional[float] = 150,
        icadim: Optional[int] = None,
        workdir: Optional[str] = None,
        ):
    """
    Run ICA

    Args:
        func:
        func_brainmask:
        func_tr:
        temporal_fwhm:
        icadim:
        workdir:

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['func_filt']
    outputs.defines(out_names, error=True)

    assrt.assert_file_exists(func, func_brainmask)
    assrt.assert_is_nifti(func, func_brainmask)

    if func_tr is None:
        func_tr = float(shops.run("fslval {0} pixdim4".format(func)))
    else:
        func_tr = float(func_tr)

    if temporal_fwhm is None:
        func_filt = func
    else:
        temporal_fwhm = float(temporal_fwhm)

        sigma = np.round(temporal_fwhm / (2 * func_tr))  # 150 second filter

        func_filt = outputs.get('func_filt', make_dir=True)

        with TemporaryDirectory(dir=outputs.get('icadir', make_dir=True)) as tmp:

            func_mean = op.join(tmp, 'func_mean.nii.gz')

            fsl.fslmaths(input=func).Tmean().run(output=func_mean)
            fsl.fslmaths(input=func).bptf(sigma, -1).add(func_mean).run(output=func_filt)

    fsl.melodic(
        input=func_filt,
        mask=func_brainmask,
        outdir=outputs.get('icadir'),
        dim=icadim,
        tr=func_tr)
