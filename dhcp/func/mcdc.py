#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Motion and distortion correction stage of the dHCP neonatal fMRI pipeline.
"""
import logging
import os
import os.path as op
import pprint
from tempfile import TemporaryDirectory
from typing import Optional, Union

import nibabel as nb
import numpy as np
import pandas as pd
from fsl.utils.filetree import FileTree

from dhcp.util import assertions as asrt
from dhcp.util import enums
from dhcp.util import fslpy as fsl
from dhcp.util import shellops as shops
from dhcp.util import util
from dhcp.util.acqparam import main as acqparam
from dhcp.util.enums import MotionMetric
from dhcp.util.io import Path
from dhcp.util.util import validate_path_input

DEFAULTS = FileTree.read(util.get_resource('mcdc.tree'), partial_fill=False)


def mcdc(func: str,
         func_pedir: Optional[enums.PhaseEncodeDirection] = None,
         func_echospacing: Optional[float] = None,
         func_brainmask: Optional[str] = None,
         func_slorder: Optional[str] = None,
         fmap: Optional[str] = None,
         fmap2func_affine: Optional[str] = None,
         do_dc: Optional[bool] = None,
         do_s2v: Optional[bool] = None,
         do_mbs: Optional[bool] = None,
         use_mcflirt: bool = None,
         workdir: Optional[str] = None,
         ref: Union[str, int] = 0,
         ):
    """
    Run the motion and distortion correction stage of the dHCP neonatal fMRI pipeline.

    Args:
        func:
        func_pedir:
        func_echospacing:
        func_brainmask:
        func_slorder:
        fmap:
        fmap2func_affine:
        do_dc:
        do_s2v:
        do_mbs:
        use_mcflirt:
        workdir:
        ref:

    Returns:

    """

    # required for mcflirt-based MC: func
    # required for eddy-based MC: func, func_brainmask
    # required for eddy-based MCDC: func, func_brainmask, func_echospacing, func_pedir, fmap, fmap2func_affine
    # required for eddy-based S2V: func_slorder

    input_args = locals()
    input_args['func_pedir'] = func_pedir if func_pedir is None else func_pedir.name

    # start logging
    logger = logging.getLogger(__name__)
    logger.info('start')
    logger.debug(pprint.pformat(input_args, indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = [
        'func_dvars', 'func_outlier', 'func_mcdc', 'func_mcdc_dvars', 'func_mcdc_outlier',
        'motparams', 'mcdc_log', 'func_mcdc_mean', 'func_mcdc_std', 'func_mcdc_brainmask',
        'func_mcdc_fovmask', 'func_mcdc_fovpercent'
    ]
    outputs.defines(out_names, error=True)

    logger.info("BEGIN: motion correction and susceptibility distortion correction")

    # TODO: change to logging
    # print(f'DO MCDC on {func}')

    _has_fmap = fmap is not None
    _has_slorder = func_slorder is not None
    _has_acqp = (func_pedir is not None) & (func_echospacing is not None)

    # set control booleans (do_dc, do_s2v, do_mbs) based on required args if they are not explicitly set (i.e. None)
    if do_dc is None:
        do_dc = (_has_fmap & _has_acqp)
    if do_mbs is None:
        do_mbs = (_has_fmap & _has_acqp & do_dc)
    if do_s2v is None:
        do_s2v = _has_slorder
    if use_mcflirt is None:
        use_mcflirt = (not do_dc) & (not do_s2v) & (not do_mbs)

    # update outputs for DC (if req'd)
    # if do_dc:
    #     outputs = outputs.update(dc='dc')

    # save mcdc parameters to json
    util.dict2json(input_args, outputs.get('mcdc_params', make_dir=True))
    input_args.update(do_dc=do_dc, do_mbs=do_mbs, do_s2v=do_s2v, use_mcflirt=use_mcflirt)

    # TODO: change to logging
    # print(f'use_mcflirt: {use_mcflirt}')
    # print(f'do_dc: {do_dc}')
    # print(f'do_mbs: {do_mbs}')
    # print(f'do_s2v: {do_s2v}')

    # check argument combinations

    if (do_dc & use_mcflirt) | (do_s2v & use_mcflirt) | (do_mbs & use_mcflirt):
        raise RuntimeError('Cannot use MCFLIRT with do_dc, do_s2v and/or do_mbs.')

    if do_dc and not (_has_fmap & _has_acqp):
        raise RuntimeError('fmap, fmap2func_affine, func_pedir, and func_echospacing are required for DC.')

    if do_mbs and not (_has_fmap & _has_acqp):
        raise RuntimeError('fmap, fmap2func_affine, func_pedir, and func_echospacing are required for mbs.')

    if do_s2v and not _has_slorder:
        raise RuntimeError('func_slorder is required argument for slice-to-volume motion correction.')

    if do_mbs and not do_dc:
        raise RuntimeError('Cannot do_mbs without do_dc as they are both distortion corrections.')

    if (not use_mcflirt) & (func_brainmask is None):
        raise RuntimeError('func_brainmask is required to use EDDY')

    # print(outputs.variables)
    # print(outputs.get('func_mcdc'))

    # Do MCDC

    if use_mcflirt:

        mcflirt_mc(
            func=func,
            func_mc=outputs.get('func_mcdc', make_dir=True),
            ref=ref,
        )

        mcf = np.loadtxt(Path(outputs.get('func_mcdc')).splitext()[0] + '.par')
        mcf = pd.DataFrame(mcf, columns=['RotX', 'RotY', 'RotZ', 'X', 'Y', 'Z'])
        mcf.to_csv(outputs.get('motparams'), sep='\t', index=None)

    else:

        # eddy_args = {
        #     'func': func,
        #     'func_brainmask': func_brainmask,
        #     'func_mcdc': outputs.get('func_mcdc'),
        # }
        #
        # if do_dc:
        #     eddy_args = {
        #         **eddy_args,
        #         'fmap': fmap,
        #         'fmap2func_affine': fmap2func_affine,
        #         'func_pedir': func_pedir,
        #     }

        eddy_mcdc(
            func=func,
            func_brainmask=func_brainmask,
            func_mcdc=outputs.get('func_mcdc'),
            func_pedir=func_pedir,
            func_echospacing=func_echospacing,
            func_sliceorder=func_slorder,
            fmap=fmap,
            fmap2func_tx=fmap2func_affine,
            mbs=do_mbs)

        mcf = np.loadtxt(Path(outputs.get('func_mcdc')).splitext()[0] + '.eddy_parameters')
        mcf = pd.DataFrame(mcf, columns=['X', 'Y', 'Z', 'RotX', 'RotY', 'RotZ'])
        mcf.to_csv(outputs.get('motparams'), sep='\t', index=None)

    # calculate post-mc motion outliers

    motion_outlier(
        outputs.get('func_mcdc'),
        metric_name=outputs.get('func_mcdc_metrics', make_dir=True),
    )

    # brain extract mcdc images

    mcdc = outputs.get('func_mcdc')
    mcdc_mean = outputs.get('func_mcdc_mean', make_dir=True)
    mcdc_std = outputs.get('func_mcdc_std', make_dir=True)
    mcdc_tsnr = outputs.get('func_mcdc_tsnr', make_dir=True)
    mcdc_brainmask = outputs.get('func_mcdc_brainmask', make_dir=True)

    fsl.fslmaths(mcdc).Tmean().run(mcdc_mean)
    fsl.fslmaths(mcdc).Tstd().run(mcdc_std)
    fsl.fslmaths(mcdc_mean).div(mcdc_std).run(mcdc_tsnr)

    with TemporaryDirectory(dir=Path(mcdc).dirname) as tmp:
        brain = op.join(tmp, 'brain.nii.gz')
        fsl.bet(
            input=mcdc_mean,
            output=brain,
            mask=False,
            fracintensity=0.4,
            robust=True
        )
        fsl.fslmaths(input=brain).bin().run(output=mcdc_brainmask)

    # create out-of-fov masks (for EDDY)
    eddy_output_mask = outputs.get('eddy_mcdc/output_mask')
    if op.exists(eddy_output_mask):
        fov_mask = outputs.get('func_mcdc_fovmask', make_dir=True)
        fsl.fslmaths(mcdc_brainmask).sub(eddy_output_mask).bin().run(fov_mask)
        fov_percent = outputs.get('func_mcdc_fovpercent', make_dir=True)
        fsl.fslmaths(fov_mask).Tmean().mul(100).run(fov_percent)


@util.timeit
def mcflirt_mc(func: str,
               func_mc: str,
               ref: Optional[Union[str, int]] = None,
               dc_warp: Optional[str] = None):
    """
    Run MCFLIRT-based motion and distortion correction.

    Args:
        func:
        func_mc:
        ref:
        dc_warp:

    Returns:

    """

    # check inputs
    func = validate_path_input(func, ndim=4)
    func_mc = validate_path_input(func_mc, test_exist=False)
    if isinstance(ref, str):
        ref = validate_path_input(ref)
    if dc_warp is not None:
        dc_warp = validate_path_input(dc_warp, ndim=4)

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    ref_vol = ref if isinstance(ref, int) else None
    ref_file = ref if isinstance(ref, str) else None

    # func_mc.makedirs()

    fsl.mcflirt(
        infile=func,
        outfile=func_mc,
        reffile=ref_file,
        refvol=ref_vol,
    )

    if dc_warp is not None:
        with TemporaryDirectory(dir=func_mc.dirname) as tmp:
            fsl.fslroi(func_mc, op.join(tmp, 'func0_ref.nii.gz'), tmin=0, tsize=1)

            fsl.applywarp(
                src=func_mc,
                ref=op.join(tmp, 'func0_ref.nii.gz'),
                out=func_mc,
                warp=dc_warp,
                interp='spline',
            )


@util.timeit
def eddy_mcdc(func: str,
              func_brainmask: str,
              func_mcdc: str,
              func_pedir: Optional[enums.PhaseEncodeDirection] = None,
              func_echospacing: Optional[float] = None,
              func_sliceorder: Optional[str] = None,
              fmap: Optional[str] = None,
              fmap2func_tx: Optional[str] = None,
              motparams: Optional[str] = None,
              mbs: bool = False):
    """
    Run EDDY-based motion and distortion correction.

    Args:
        func:
        func_brainmask:
        func_mcdc:
        func_pedir:
        func_echospacing:
        func_sliceorder:
        fmap:
        fmap2func_tx:
        motparams:
        mbs:

    Returns:

    """

    # check inputs
    func = validate_path_input(func, ndim=4)
    func_brainmask = validate_path_input(func_brainmask)
    func_mcdc = validate_path_input(func_mcdc, test_exist=False)

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # check that func is oriented to standard (required for EDDY)
    if not util.is_std_orient(func):
        raise RuntimeError("Input functional image must be oriented to standard (see fslreorient2std)")

    eddy_basename = func_mcdc.splitext()[0]

    # logic tests
    _has_acqp = (func_pedir is not None) & (func_echospacing is not None)
    _has_fmap = fmap is not None
    _has_slorder = func_sliceorder is not None

    if _has_fmap and not _has_acqp:
        raise RuntimeError('Cannot do DC without func_pedir and func_echospacing.')

    if mbs and not _has_fmap:
        raise RuntimeError('Cannot do MBS without fmap.')

    if _has_slorder:
        logger.info(f'Performing slice-to-volume (S2V) motion correction')
    else:
        logger.info(f'Performing rigid-body motion correction')

    if _has_acqp and _has_fmap:
        if mbs:
            logger.info(f'Performing motion-by-susceptibility distortion correction')
        else:
            logger.info(f'Performing static distortion correction')
    else:
        logger.info(f'NO distortion correction')

    # setup eddy files
    Nv = nb.load(func).shape[3]

    if not eddy_basename.dirname.exists():
        eddy_basename.makedirs()

    tmp = eddy_basename.dirname

    index = op.join(tmp, "index.txt")
    np.savetxt(index, np.ones((1, Nv)), fmt="%i")

    bvals = op.join(tmp, "bvals")
    np.savetxt(bvals, np.zeros((1, Nv)), fmt="%i")

    bvecs = op.join(tmp, "bvecs")
    np.savetxt(bvecs, np.ones((3, Nv)) * np.array([[1], [0], [0]]), fmt="%i")

    func_acqp = op.join(tmp, 'eddy.acqp')
    if _has_acqp:
        acqparam(func, func_echospacing, func_pedir.name, outname=func_acqp)
    else:
        with open(func_acqp, 'w') as f:
            f.write("1 0 0 0.1")

    # set some default parameters
    # TODO: make these parameters be arguments
    niter = 10
    fwhm = "10,10,5,5,0,0,0,0,0,0"
    nvoxhp = 1000  # not used for anything by eddy
    cnr_maps = False  # turned off because not relevant for B0's
    residuals = False  # turned off because not relevant for B0's

    if _has_slorder:
        func_sliceorder = validate_path_input(func_sliceorder, test_nifti=False)
        s2v_niter = 10  # 5
        s2v_fwhm = 0
        # set to 16 or smallest (advised by Jesper)
        mporder = np.loadtxt(func_sliceorder).shape[0] - 1
        if mporder > 16:
            mporder = 16
        s2v_interp = "trilinear"
        s2v_lambda = 1
    else:
        s2v_niter = None
        s2v_fwhm = None
        mporder = 0
        s2v_interp = None
        s2v_lambda = None

    if mbs:
        mbs_niter = 20
        mbs_lambda = 5
        mbs_ksp = 5
    else:
        mbs_niter = None
        mbs_lambda = None
        mbs_ksp = None

    # prepare fieldmap transform
    if fmap2func_tx is None:
        fsldir = os.getenv('FSLDIR', '')
        fmap2func_tx = op.join(fsldir, 'etc', 'flirtsch', 'ident.mat')

    # prepare fieldmap
    field_hz = None
    if fmap is not None:
        fmap = validate_path_input(fmap)
        field_hz = op.join(tmp, fmap.basename + "_Hz.nii.gz")
        fsl.fslmaths(input=fmap).div(6.2832).run(output=field_hz)

    # run EDDY
    fsl.eddy(
        imain=func,
        mask=func_brainmask,
        acqp=func_acqp,
        bvecs=bvecs,
        bvals=bvals,
        index=index,
        out=eddy_basename,
        very_verbose=True,
        niter=niter,
        fwhm=fwhm,
        s2v_niter=s2v_niter,
        mporder=mporder,
        nvoxhp=nvoxhp,
        slspec=func_sliceorder,
        b0_only=True,
        field=field_hz,
        field_mat=fmap2func_tx,
        s2v_lambda=s2v_lambda,
        s2v_fwhm=s2v_fwhm,
        dont_mask_output=True,
        s2v_interp=s2v_interp,
        data_is_shelled=True,
        estimate_move_by_susceptibility=mbs,
        mbs_niter=mbs_niter,
        mbs_lambda=mbs_lambda,
        mbs_ksp=mbs_ksp,
        cnr_maps=cnr_maps,
        residuals=residuals)

    if motparams is not None:
        util.rel_sym_link(op.join(eddy_basename + '.eddy_parameters'), motparams)

    # fix TR that was mangled by EDDY.
    # TODO: remove once EDDY is fixed
    # tr = shops.run("fslval {fname} pixdim4".format(fname=func))
    # cmd = "fslmodhd {fname} pixdim4 {TR}"
    # shops.run(cmd.format(fname=func_mcdc, TR=tr))
    func = nb.load(func)
    nb.Nifti1Image(
        nb.load(func_mcdc).get_fdata(),
        header=func.header,
        affine=func.affine
    ).to_filename(func_mcdc)


def __dvars(image):
    """
    Calculate DVARS.

    Args:
        image:

    Returns:

    """
    dvars = np.square(np.diff(image, axis=3))
    dvars = np.nanmean(dvars, axis=(0, 1, 2))
    dvars = np.sqrt(dvars)
    dvars = 1000 * (dvars / np.nanmedian(image))
    dvars = np.concatenate(([0], dvars))

    return dvars


def __refrms(image, ref=None):
    """
    Calculate REFRMS.

    Args:
        image:
        ref:

    Returns:

    """
    if ref is None:
        ref = image[:, :, :, int(np.round(image.shape[3] / 2))]

    rms = image
    for i in range(0, rms.shape[3]):
        rms[:, :, :, i] = (rms[:, :, :, i] - ref)
    rms = rms / np.nanmedian(image)
    rms = np.square(rms)
    rms = np.nanmean(rms, axis=(0, 1, 2))
    rms = np.sqrt(rms)
    rms = np.abs(np.diff(rms))
    rms = np.concatenate(([0], rms))

    return rms


def motion_outlier(func,
                   metric_name=None,
                   plot_name=None,
                   metric=MotionMetric.dvars,
                   ref=None,
                   thr=None):
    """
    Estimate motion in 4D volume.

    Args:
        func:
        metric_name:
        plot_name:
        metric:
        ref:
        thr:

    Returns:

    """
    asrt.assert_is_nifti(func)
    asrt.assert_file_exists(func)

    img0 = nb.load(func)
    image_data = img0.get_data().astype(float)

    # threshold image
    thr2 = np.nanpercentile(image_data, 2)
    thr98 = np.nanpercentile(image_data, 98)
    robust_thr = thr2 + 0.1 * (thr98 - thr2)
    image_data[image_data < robust_thr] = np.nan

    # calculate motion metric
    dvars = __dvars(image_data)
    refrms = __refrms(image_data, ref)

    if metric is None:
        metric = MotionMetric.dvars

    if metric is MotionMetric.dvars:
        metric_data = dvars
    elif metric is MotionMetric.refrms:
        metric_data = refrms
    else:
        raise TypeError('Invalid type for metric')

    # calculate outlier
    if not thr:
        q75, q25 = np.percentile(metric_data, [75, 25])
        iqr = q75 - q25
        thr = q75 + (1.5 * iqr)

    outlier = metric_data > thr

    # save metric values to file
    pd.DataFrame(
        np.stack((dvars, refrms, outlier), axis=1),
        columns=['DVARS', 'RefRMS', 'Outlier' + metric.name.upper()],
    ).to_csv(metric_name, sep='\t', index=None)

    # plot metric
    if plot_name is not None:
        import matplotlib
        matplotlib.use('Agg')
        import matplotlib.pyplot as plt

        hfig = plt.figure()
        ax_hdl = hfig.add_subplot(1, 1, 1)

        outlier[0] = False
        h = ax_hdl.fill_between(
            range(outlier.shape[0]),
            outlier * np.amax(metric_data),
            0,
            color='red',
            label="outliers",
            interpolate=True)
        plt.setp(h, alpha=0.25)

        ax_hdl.plot(metric_data, label=metric.name)
        ax_hdl.set_xlim(1, len(metric_data))
        ax_hdl.set_ylim(0, np.nanmean(metric_data) + (4 * np.nanstd(metric_data)))
        ax_hdl.set_ylabel(metric.name)
        ax_hdl.set_title(Path(func).filename)
        ax_hdl.set_xlabel("time (volumes)")
        plt.legend()
        plt.savefig(plot_name)

    return outlier, metric_data, thr
