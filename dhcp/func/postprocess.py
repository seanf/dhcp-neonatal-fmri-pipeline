#!/usr/bin/env python
"""
    DHCP_FUNC - Run post process spatial smooting and intensity norm.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import sys
import os.path as op
import numpy as np
import nibabel as nb
import dhcp.util.util as util
import dhcp.util.fslpy as fsl
import dhcp.util.assertions as assrt
import dhcp.util.shellops as shops
from dhcp.util.io import Path
import logging
from fsl.utils.filetree import FileTree
# from dhcp.func.workdir import WorkDir

@util.timeit
def postprocess(func: Path,
                func_mean: Path,
                func_brainmask: Path,
                spatial_fwhm=5,
                do_intnorm=False,
                basename=None,
                **kwargs):
    """Run post process spatial smoothing and intensity norm."""

    func = Path(op.expanduser(func))
    func_mean = Path(op.expanduser(func_mean))
    func_brainmask = Path(op.expanduser(func_brainmask))

    # setup logging
    logger = logging.getLogger(__name__)
    logger.info('start')
    logger.debug(locals())

    logger.info(f"Postprocessing: spatial smoothing and/or intensity normalisation")

    assrt.assert_file_exists(func, func_mean, func_brainmask)
    assrt.assert_is_nifti_3D(func_mean, func_brainmask)
    assrt.assert_is_nifti_4D(func)

    # update output vars
    templates = dict(
        func_smooth='{basename}_smooth.nii.gz',
        func_intnorm='{basename}_intnorm.nii.gz'
    )

    # update templates from kwargs
    for k, v in kwargs.items():
        if k not in templates:
            raise ValueError(f'unknown output template {k}')
        templates[k] = v

    # setup basename
    if basename is None:
        basename = func.splitext()[0]
    else:
        basename = Path(op.expanduser(basename))

    variables = dict(
        basename=basename,
    )

    # create workdir filetree
    workdir = FileTree(templates=templates, variables=variables)

    # FEAT-style spatial smoothing

    spatial_fwhm = float(spatial_fwhm)

    func_smooth = workdir.get('func_smooth', make_dir=True)
    fsl.fslmaths(func).mas(func_brainmask).run(func_smooth)

    d = nb.load(func_smooth).get_data().astype(float)
    func_q2, func_q98 = np.percentile(d, [2, 98])

    d = nb.load(func_smooth).get_data().astype(float)
    m = nb.load(func_brainmask).get_data().astype(float)
    median_intensity = np.percentile(d[m >= 1], 50)

    if spatial_fwhm > 0.01:
        #  spatial smoothing using a Gaussian kernel of FWHM filter_width_mm
        sigma = spatial_fwhm / 2.355
        susan_thr = (median_intensity - func_q2) * 0.75

        # fsl.fslmaths(input=func_smooth).Tmean().run(output=func_mean)

        susancmd = 'susan {0} {1} {2} 3 1 1 {3} {1} {4}'
        susancmd = susancmd.format(func_smooth, susan_thr, sigma, func_mean,
                                   func_smooth)
        shops.run(susancmd)
        fsl.fslmaths(func_smooth).mas(func_brainmask).run()
        func = func_smooth

    # FEAT-style intensity normalisation
    func_intnorm = workdir.get('func_intnorm', make_dir=True)
    normmean = 10000
    if do_intnorm:
        # multiplicative mean intensity normalization of the volume at each
        # timepoint
        fsl.fslmaths(func).inm(normmean).run(func_intnorm)
    else:
        # grand-mean intensity normalisation of the entire 4D dataset by a
        # single multiplicative factor
        scaling = normmean / median_intensity
        fsl.fslmaths(func).mul(scaling).run(func_intnorm)
