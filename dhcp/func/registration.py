#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Image registration for the dHCP neonatal fMRI pipeline
"""
import logging
import os
import os.path as op
import pprint
from tempfile import TemporaryDirectory
from typing import Optional

from fsl.utils.filetree import FileTree

from dhcp.util import assertions as assrt, util, fslpy as fsl, image as img, logutil, shellops
from dhcp.util.acqparam import _get_axis as get_axis, phase_encode_dict
from dhcp.util.enums import PhaseEncodeDirection, BBRType
from dhcp.util.io import Path
from dhcp.util.util import validate_path_input

DEFAULTS = FileTree.read(util.get_resource('registration.tree'), partial_fill=False)


@util.timeit
def fmap_to_struct(fmap: str,
                   fmap_magnitude: str,
                   fmap_brainmask: str,
                   struct: str,
                   struct_brainmask: str,
                   struct_boundarymask: Optional[str] = None,
                   do_bbr: bool = True,
                   bbrslope: float = 0.5,
                   bbrtype: BBRType = BBRType.signed,
                   workdir: Optional[str] = None,
                   ):
    """
    Register (rigid) fieldmap to structural image, with optional BBR.

    Args:
        fmap:
        fmap_magnitude:
        fmap_brainmask:
        struct:
        struct_brainmask:
        struct_boundarymask:
        do_bbr:
        bbrslope:
        bbrtype:
        workdir:

    Returns:

    """
    # check inputs (3D nifti, exists, return as Path)
    fmap = validate_path_input(fmap)
    fmap_magnitude = validate_path_input(fmap_magnitude)
    fmap_brainmask = validate_path_input(fmap_brainmask)
    struct = validate_path_input(struct)
    struct_brainmask = validate_path_input(struct_brainmask)

    if do_bbr:
        if struct_boundarymask is None:
            raise RuntimeError('struct_boundarymask is required for BBR')
        struct_boundarymask = validate_path_input(struct_boundarymask)
        assrt.assert_isinstance(BBRType, bbrtype)
        assrt.assert_isfloat(bbrslope)

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=fmap.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=struct.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'inv_affine']
    if do_bbr:
        out_names += ['init_affine', 'resampled_image_init']
    outputs.defines(out_names, error=True)

    tmpdir = Path(outputs.get('affine', make_dir=True)).dirname
    with TemporaryDirectory(dir=tmpdir) as tmp:

        # extract brains
        struct_brain = Path(op.join(tmp, 'struct_brain.nii.gz'))
        fsl.fslmaths(struct).mas(struct_brainmask).run(struct_brain)

        fmap_brain = Path(op.join(tmp, 'fmap_brain.nii.gz'))
        fsl.fslmaths(fmap_magnitude).mas(fmap_brainmask).run(fmap_brain)

        # run registration

        args = dict(
            src=fmap_brain,
            ref=struct_brain,
            affine=outputs.get('affine', make_dir=True),
            inv_affine=outputs.get('inv_affine', make_dir=True),
            src2ref=outputs.get('resampled_image', make_dir=True),
        )

        if do_bbr:
            args = dict(
                **args,
                ref_boundarymask=struct_boundarymask,
                bbrslope=bbrslope,
                bbrtype=bbrtype,
                init_affine=outputs.get('init_affine', make_dir=True),
                src2ref_init=outputs.get('resampled_image_init', make_dir=True)
            )

        epireg(**args)


@util.timeit
def func_to_sbref(func: str,
                  func_brainmask: str,
                  sbref: str,
                  sbref_brainmask: str,
                  workdir: Optional[str] = None,
                  ):
    """
    Register (rigid) functional EPI to SBREF.

    Args:
        func:
        func_brainmask:
        sbref:
        sbref_brainmask:
        workdir:

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # check inputs
    func = validate_path_input(func, ndim=3, test_nifti=True)
    func_brainmask = validate_path_input(func_brainmask, ndim=3, test_nifti=True)
    sbref = validate_path_input(sbref, ndim=3, test_nifti=True)
    sbref_brainmask = validate_path_input(sbref_brainmask, ndim=3, test_nifti=True)

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=func.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=sbref.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'inv_affine']
    outputs.defines(out_names, error=True)

    tmpdir = Path(outputs.get('affine', make_dir=True)).dirname
    with TemporaryDirectory(dir=tmpdir) as tmp:

        func_brain = op.join(tmp, 'func_brain.nii.gz')
        fsl.fslmaths(func).mas(func_brainmask).run(func_brain)

        sbref_brain = op.join(tmp, 'sbref_brain.nii.gz')
        fsl.fslmaths(sbref).mas(sbref_brainmask).run(sbref_brain)

        # run registration (1. brain-extracted, then 2. not brain extracted)

        init_affine = op.join(tmp, 'init.mat')

        epireg(  # brain-extracted
            src=func_brain,
            ref=sbref_brain,
            affine=init_affine,
            inv_affine=Path(op.join(tmp, 'init_inv.mat')),
            src2ref=Path(op.join(tmp, 'init.nii.gz')),
        )

        epireg(  # not brain-extracted
            src=func,
            ref=sbref,
            init_xfm=init_affine,
            affine=Path(outputs.get('affine', make_dir=True)),
            inv_affine=Path(outputs.get('inv_affine', make_dir=True)),
            src2ref=Path(outputs.get('resampled_image', make_dir=True)),
        )


@util.timeit
def sbref_to_struct(sbref: str,
                    sbref_brainmask: str,
                    struct: str,
                    struct_brainmask: str,
                    do_dc: bool = True,
                    fmap: Optional[str] = None,
                    fmap_brainmask: Optional[str] = None,
                    fmap2struct_xfm: Optional[str] = None,
                    sbref_pedir: Optional[PhaseEncodeDirection] = None,
                    sbref_echospacing: Optional[float] = None,
                    struct_boundarymask: Optional[str] = None,
                    do_bbr: bool = True,
                    bbrslope: float = 0.5,
                    bbrtype: BBRType = BBRType.signed,
                    workdir: Optional[str] = None,
                    ):
    """
    Register SBREF to structural image, with optional BBR and distortion correction.

    Args:
        sbref:
        sbref_brainmask:
        struct:
        struct_brainmask:
        do_dc:
        fmap:
        fmap_brainmask:
        fmap2struct_xfm:
        sbref_pedir:
        sbref_echospacing:
        struct_boundarymask:
        do_bbr:
        bbrslope:
        bbrtype:
        workdir:

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # check inputs
    sbref = validate_path_input(sbref)
    sbref_brainmask = validate_path_input(sbref_brainmask)
    struct = validate_path_input(struct)
    struct_brainmask = validate_path_input(struct_brainmask)

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=sbref.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=struct.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # logic tests

    _has_fmap = fmap is not None and fmap_brainmask is not None
    _has_boundary = struct_boundarymask is not None
    _has_sbref_props = sbref_pedir is not None and sbref_echospacing is not None

    if do_bbr:
        if not _has_boundary:
            raise RuntimeError('struct_boundarymask is required for BBR')
        struct_boundarymask = validate_path_input(struct_boundarymask)
        assrt.assert_isfloat(bbrslope)
        assrt.assert_isinstance(BBRType, bbrtype)

    if do_dc:
        if not _has_fmap:
            raise RuntimeError('fmap and fmap_brainmask are required for distortion correction')

        if not _has_sbref_props:
            raise RuntimeError('sbref_pedir and sbref_echospacing are required for distortion correction')

        fmap = validate_path_input(fmap)
        fmap_brainmask = validate_path_input(fmap_brainmask)
        assrt.assert_isfloat(sbref_echospacing)
        assrt.assert_isinstance(PhaseEncodeDirection, sbref_pedir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'inv_affine']
    if do_bbr:
        out_names += ['init_affine', 'resampled_image_init']
    if do_dc:
        out_names += ['warp', 'dc_warp', 'dc_image']
    outputs.defines(out_names, error=True)

    tmpdir = Path(outputs.get('affine', make_dir=True)).dirname
    with TemporaryDirectory(dir=tmpdir) as tmp:
        sbref_brain = op.join(tmp, 'sbref_brain.nii.gz')
        fsl.fslmaths(sbref).mas(sbref_brainmask).run(sbref_brain)

        struct_brain = op.join(tmp, 'struct_brain.nii.gz')
        fsl.fslmaths(struct).mas(struct_brainmask).run(struct_brain)

        # run registration

        args = dict(
            src=sbref_brain,
            ref=struct_brain,
            affine=outputs.get('affine', make_dir=True),
            inv_affine=outputs.get('inv_affine', make_dir=True),
            src2ref=outputs.get('resampled_image', make_dir=True),
        )

        if do_bbr:
            args = dict(
                **args,
                ref_boundarymask=struct_boundarymask,
                bbrslope=bbrslope,
                bbrtype=bbrtype,
                init_affine=outputs.get('init_affine', make_dir=True),
                src2ref_init=outputs.get('resampled_image_init', make_dir=True)
            )

        if do_dc:

            if fmap2struct_xfm is None:
                fmap2struct_xfm = Path(os.getenv('FSLDIR')).join('etc/flirtsch/ident.mat')
            assrt.assert_file_exists(fmap2struct_xfm)

            args = dict(
                **args,
                fmap=fmap,
                fmap_brainmask=fmap_brainmask,
                fmap2ref_xfm=fmap2struct_xfm,
                src_pedir=sbref_pedir.name,
                src_echospacing=sbref_echospacing,
                warp=outputs.get('warp', make_dir=True),
            )

        epireg(**args)

        if do_dc:
            # create warp to distortion correct the func and/or sbref/sbref_brainmask
            fsl.convertwarp(
                warp1=outputs.get('warp'),
                postmat=outputs.get('inv_affine'),
                ref=sbref,
                out=outputs.get('dc_warp', make_dir=True)
            )

            fsl.applywarp(
                src=sbref,
                ref=sbref,
                warp=outputs.get('dc_warp'),
                out=outputs.get('dc_image'),
                interp='spline'
            )

            fsl.applywarp(
                src=sbref_brainmask,
                ref=sbref_brainmask,
                warp=outputs.get('dc_warp'),
                out=outputs.get('dc_brainmask'),
                interp='trilinear'
            )
            fsl.fslmaths(outputs.get('dc_brainmask')).thr(0.5).bin().run()


@util.timeit
def func_to_struct_composite(func: str,
                             struct: str,
                             func2sbref_affine: str,
                             sbref2struct_affine: str,
                             sbref2struct_warp: Optional[str] = None,
                             workdir: Optional[str] = None,
                             ):
    """
    Create composite transform func -> sbref -> struct.
    Create distortion correction warp for func and sbref.

    Args:
        func:
        struct:
        func2sbref_affine:
        sbref2struct_affine:
        sbref2struct_warp:
        workdir:

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # check inputs
    func = validate_path_input(func, ndim=3, test_nifti=True)
    struct = validate_path_input(struct, ndim=3, test_nifti=True)
    func2sbref_affine = validate_path_input(func2sbref_affine, test_nifti=False)
    sbref2struct_affine = validate_path_input(sbref2struct_affine, test_nifti=False)

    _has_warp = sbref2struct_warp is not None

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=func.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=struct.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'inv_affine']
    if _has_warp:
        out_names += ['warp', 'inv_warp', 'dc_warp', 'dc_image']
    outputs.defines(out_names, error=True)

    # func (undistorted) -> struct :: affine
    fsl.concatxfm(
        inmat1=func2sbref_affine,
        inmat2=sbref2struct_affine,
        outmat=outputs.get('affine', make_dir=True)
    )

    # struct -> func (undistorted) :: affine
    fsl.invxfm(
        inmat=outputs.get('affine'),
        omat=outputs.get('inv_affine', make_dir=True)
    )

    if _has_warp:
        sbref2struct_warp = validate_path_input(sbref2struct_warp, ndim=4, test_nifti=True)

        # func (distorted) -> struct :: warp
        fsl.convertwarp(
            premat=func2sbref_affine,
            warp1=sbref2struct_warp,
            out=outputs.get('warp', make_dir=True),
            ref=struct
        )

        # struct -> func (distorted) :: warp
        fsl.invwarp(
            inwarp=outputs.get('warp'),
            ref=func,
            outwarp=outputs.get('inv_warp', make_dir=True)
        )

        # create warp to distortion correct the func
        fsl.convertwarp(
            warp1=outputs.get('warp'),
            postmat=outputs.get('inv_affine'),
            ref=func,
            out=outputs.get('dc_warp', make_dir=True)
        )

        fsl.applywarp(
            src=func,
            ref=func,
            warp=outputs.get('dc_warp'),
            out=outputs.get('dc_image')
        )

    # resample func to struct space
    if _has_warp:
        fsl.applywarp(
            src=func,
            ref=struct,
            warp=outputs.get('warp'),
            out=outputs.get('resampled_image', make_dir=True),
            interp='spline'
        )
    else:
        fsl.applywarp(
            src=func,
            ref=struct,
            premat=outputs.get('affine'),
            out=outputs.get('resampled_image', make_dir=True),
            interp='spline'
        )


@util.timeit
def fmap_to_func_composite(fmap: str,
                           func: str,
                           fmap2struct_affine: str,
                           func2struct_invaffine: str,
                           workdir: Optional[str] = None,
                           ):
    """
    Create composite transform fieldmap -> struct -> sbref -> func.

    Args:
        fmap:
        func:
        fmap2struct_affine:
        func2struct_invaffine:
        workdir:

    Returns:

    """
    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # check inputs
    fmap = validate_path_input(fmap, ndim=3, test_nifti=True)
    func = validate_path_input(func, ndim=3, test_nifti=True)
    fmap2struct_affine = validate_path_input(fmap2struct_affine, test_nifti=False)
    func2struct_invaffine = validate_path_input(func2struct_invaffine, test_nifti=False)

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=fmap.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=func.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine']
    outputs.defines(out_names, error=True)

    # fmap -> func
    fsl.concatxfm(
        inmat1=fmap2struct_affine,
        inmat2=func2struct_invaffine,
        outmat=outputs.get('affine', make_dir=True)
    )

    fsl.applyxfm(
        src=fmap,
        ref=func,
        mat=outputs.get('affine'),
        interp='spline',
        out=outputs.get('resampled_image', make_dir=True)
    )


@util.timeit
def template_to_struct(age: int,
                       struct_brainmask: str,
                       atlas: FileTree,
                       struct_T1w: Optional[str] = None,
                       struct_T2w: Optional[str] = None,
                       struct_gmprob: Optional[str] = None,
                       struct_wmprob: Optional[str] = None,
                       workdir: Optional[str] = None,
                       quick: bool = False,
                       ):
    """
    Register age-matched-template -> struct

    Args:
        struct_wmprob:
        struct_gmprob:
        atlas:
        age (int):  age-at-scan
        struct_brainmask:
        struct_T1w:
        struct_T2w:
        workdir:
        quick:

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # cap/trim age if necessary
    age = int(age)
    age = 28 if age < 28 else age
    age = 44 if age > 44 else age

    # check inputs
    struct_brainmask = validate_path_input(struct_brainmask)

    if (struct_T1w is None) and (struct_T2w is None):
        raise RuntimeError('T2w and/or T1w is required')

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=f'template-{age}')
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space='struct')
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'warp', 'inv_warp']
    outputs.defines(out_names, error=True)

    # build struct and template args
    struct = []
    template = []

    regdir = Path(outputs.get('warp', make_dir=True)).dirname

    if struct_T2w is not None:
        atlas.update(age=age).on_disk('template_T2', error=True)
        struct_T2w = validate_path_input(struct_T2w)
        struct += [struct_T2w]
        template += [atlas.update(age=age).get('template_T2')]

    if struct_T1w is not None:
        atlas.update(age=age).on_disk('template_T1', error=True)
        struct_T1w = validate_path_input(struct_T1w)
        struct += [struct_T1w]
        template += [atlas.update(age=age).get('template_T1')]

    if struct_gmprob is not None:
        atlas.update(age=age).on_disk('prob', error=True)
        struct_gmprob = validate_path_input(struct_gmprob)
        struct += [struct_gmprob]

        gmpb = op.join(regdir, 'template_gmprob.nii.gz')
        fsl.fslroi(
            input=atlas.update(age=age).get('prob'),
            output=gmpb,
            tmin=1,
            tsize=1,
        )
        fsl.fslmaths(gmpb).div(100).run()
        template += [gmpb]

    if struct_wmprob is not None:
        atlas.update(age=age).on_disk('prob', error=True)
        struct_wmprob = validate_path_input(struct_wmprob)
        struct += [struct_wmprob]

        wmpb = op.join(regdir, 'template_wmprob.nii.gz')
        fsl.fslroi(
            input=atlas.update(age=age).get('prob'),
            output=wmpb,
            tmin=2,
            tsize=1,
        )
        fsl.fslmaths(wmpb).div(100).run()
        template += [wmpb]

    nonlinear_reg(
        src=template,
        ref=struct,
        quick=quick,
        ref_brainmask=struct_brainmask,
        antsout=Path(outputs.get('warp')).splitext()[0] + '.ants/output_',
        affine=outputs.get('affine', make_dir=True),
        warp=outputs.get('warp', make_dir=True),
        inv_warp=outputs.get('inv_warp', make_dir=True),
        src2ref=outputs.get('resampled_image', make_dir=True)
    )

    fsl.invwarp(
        inwarp=outputs.get('warp'),
        ref=template[0],
        outwarp=outputs.get('inv_warp', make_dir=True)
    )


@util.timeit
def struct_to_template_composite(struct: str,
                                 struct2template_warp: str,
                                 age: int,
                                 atlas: FileTree,
                                 standard_age: int = 40,
                                 workdir: Optional[str] = None,
                                 ):
    """
    Create composite transform struct -> age-matched template -> standard template

    Args:
        atlas_tree:
        struct:
        struct2template_warp (str): struct to (age-matched) template warp filename
        age:
        standard_age:
        workdir:

    Returns:

    """
    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # cap/trim age if necessary
    age = int(age)
    age = 28 if age < 28 else age
    age = 44 if age > 44 else age

    standard_age = int(standard_age)
    standard_age = 36 if standard_age < 36 else standard_age
    standard_age = 44 if standard_age > 44 else standard_age

    # get standard from atlas
    standard = atlas.update(age=int(standard_age)).get('template_T2')
    standard = validate_path_input(standard)

    # check inputs
    struct = validate_path_input(struct)
    struct2template_warp = validate_path_input(struct2template_warp, ndim=4, test_nifti=True)

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=struct.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=Path(standard).basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'affine', 'warp', 'inv_warp']
    outputs.defines(out_names, error=True)

    # create struct2template warp
    logger.info('create struct2standard warp')

    if age != standard_age:

        # template2standard_warp = atlas.join(
        #     'allwarps', f'template-{age}_to_template-{standard_age}_warp.nii.gz'
        # )
        template2standard_warp = atlas.update(src_age=age, target_age=standard_age).get('xfm')

        fsl.convertwarp(
            out=outputs.get('warp', make_dir=True),
            ref=standard,
            warp1=struct2template_warp,
            warp2=template2standard_warp

        )

    else:

        util.rel_sym_link(
            struct2template_warp,
            outputs.get('warp', make_dir=True),
        )

    # invert warp

    fsl.invwarp(
        inwarp=outputs.get('warp'),
        ref=struct,
        outwarp=outputs.get('inv_warp', make_dir=True)
    )

    # resample struct to template space

    fsl.applywarp(
        src=struct,
        ref=standard,
        warp=outputs.get('warp'),
        out=outputs.get('resampled_image', make_dir=True),
        interp='spline'
    )


@util.timeit
def func_to_template_composite(func: str,
                               func2struct_affine: str,
                               struct2template_warp: str,
                               atlas: FileTree,
                               standard_age: int = 40,
                               workdir: Optional[str] = None,
                               ):
    """
    Create composite transform func -> struct ->  standard template

    Args:
        atlas:
        struct:
        func (str):
        func2struct_affine (str):
        struct2template_warp (str): struct to standard template warp filename
        standard_age (int):
        workdir (str):

    Returns:

    """

    # start logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # cap/trim standard_age if necessary

    standard_age = int(standard_age)
    standard_age = 36 if standard_age < 36 else standard_age
    standard_age = 44 if standard_age > 44 else standard_age

    # get standard from atlas
    standard = atlas.update(age=int(standard_age)).get('template_T2')
    standard = validate_path_input(standard)

    # check inputs
    func = validate_path_input(func, ndim=3, test_nifti=True)
    func2struct_affine = validate_path_input(func2struct_affine, test_nifti=False)
    struct2template_warp = validate_path_input(struct2template_warp, ndim=4, test_nifti=True)

    # setup outputs
    outputs = DEFAULTS

    if 'src_space' not in outputs.variables:
        outputs = outputs.update(src_space=func.basename)
    if 'ref_space' not in outputs.variables:
        outputs = outputs.update(ref_space=standard.basename)
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = ['resampled_image', 'warp', 'inv_warp']
    outputs.defines(out_names, error=True)

    fsl.convertwarp(
        out=outputs.get('warp', make_dir=True),
        ref=standard,
        premat=func2struct_affine,
        warp1=struct2template_warp
    )

    fsl.invwarp(
        inwarp=outputs.get('warp'),
        ref=func,
        outwarp=outputs.get('inv_warp', make_dir=True)
    )

    # resample func to template space

    fsl.applywarp(
        src=func,
        ref=standard,
        warp=outputs.get('warp'),
        out=outputs.get('resampled_image', make_dir=True),
        interp='spline'
    )


@util.timeit
def epireg(src: str,
           ref: str,
           src_brainmask: Optional[str] = None,
           ref_brainmask: Optional[str] = None,
           src_pedir: Optional[str] = None,
           src_echospacing: Optional[float] = None,
           ref_boundarymask: Optional[str] = None,
           fmap: Optional[str] = None,
           fmap_brainmask: Optional[str] = None,
           fmap2ref_xfm: Optional[str] = None,
           init_xfm: Optional[str] = None,
           bbrslope: float = -0.5,
           bbrtype: BBRType = BBRType.signed,
           basename: Optional[str] = None,
           **kwargs
           ):
    """
    Linear (FLIRT-based) registration of EPI to ref, with optional BBR and distortion correction.

    Args:
        src:
        ref:
        src_brainmask:
        ref_brainmask:
        src_pedir:
        src_echospacing:
        ref_boundarymask:
        fmap:
        fmap_brainmask:
        fmap2ref_xfm:
        init_xfm:
        bbrslope:
        bbrtype:
        basename:
        **kwargs:

    Returns:

    """

    # setup logging
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(locals())

    logger.info(f"Register {src} to {ref}")

    src = validate_path_input(src)
    ref = validate_path_input(ref)

    # update output vars
    templates = dict(
        affine='{basename}_affine.mat',
        init_affine='{basename}_init_affine.mat',
        src2ref_init='{basename}_init_img.nii.gz',
        warp='{basename}_warp.nii.gz',
        inv_affine='{basename}_invaffine.mat',
        inv_warp='{basename}_invwarp.nii.gz',
        src2ref='{basename}_img.nii.gz',
        dc_warp=f'{src.splitext()[0]}_dc_warp.nii.gz'
    )

    # update templates from kwargs
    for k, v in kwargs.items():
        if k not in templates:
            raise ValueError(f'unknown output template {k}')
        templates[k] = v

    # setup basename
    if basename is None:
        basename = src.dirname.join(f'{src.basename}_to_{ref.basename}')

    variables = dict(
        basename=basename,
    )

    # create workdir filetree
    workdir = FileTree(templates=templates, variables=variables)

    # do some logic tests

    _has_fmap = fmap is not None and fmap_brainmask is not None and fmap2ref_xfm is not None
    _do_bbr = ref_boundarymask is not None
    _do_dc = _has_fmap and src_pedir is not None and src_echospacing is not None

    logger.debug(f'has_fmap: {_has_fmap}')
    logger.debug(f'do_bbr: {_do_bbr}')
    logger.debug(f'do_dc: {_do_dc}')

    # check fieldmap inputs
    if _has_fmap:
        assrt.assert_file_exists(fmap, fmap_brainmask)
        assrt.assert_is_nifti_3D(fmap, fmap_brainmask)

    # phase encode direction

    if src_pedir is not None and src_pedir in ['PA', 'AP', 'IS', 'SI', 'RL', 'LR']:
        src_pedir = get_axis(src, src_pedir)[0]

    pedict = phase_encode_dict()
    if src_pedir is not None:
        src_pedir = pedict[src_pedir]
    else:
        src_pedir = (None, None)

    # sigloss
    # TODO: re-incorporate sigloss
    sigloss2ref = None

    with TemporaryDirectory(dir=basename.dirname) as tmp:

        if src_brainmask is not None:
            src_brain = Path(tmp).join('src_brain.nii.gz')
            fsl.fslmaths(src).mas(src_brainmask).run(src_brain)

        if ref_brainmask is not None:
            ref_brain = Path(tmp).join('ref_brain.nii.gz')
            fsl.fslmaths(ref).mas(ref_brainmask).run(ref_brain)

        # initial registration: src to ref (for BBR)

        init_affine = workdir.get('init_affine', make_dir=True)
        init_img = workdir.get('src2ref_init', make_dir=True)

        if _do_bbr:
            logutil.warn_file_exists(init_affine)
            assrt.assert_isinstance(BBRType, bbrtype)

            fsl.flirt(
                src=src if src_brainmask is None else src_brain,
                ref=ref if ref_brainmask is None else ref_brain,
                omat=init_affine,
                out=init_img,
                dof=6,
                interp='spline',
                usesqform=True,
                nosearch=True,
            )

        # main registration: src to ref

        affine = workdir.get('affine', make_dir=True)

        flirt_args = {
            'src': src if src_brainmask is None else src_brain,
            'ref': ref if ref_brainmask is None else ref_brain,
            'dof': 6,
            'cost': 'corratio',
            'omat': affine,
            'refweight': sigloss2ref,
            'usesqform': True,
            'nosearch': True,
            'interp': 'spline',
        }

        if init_xfm is not None:
            assrt.assert_file_exists(init_xfm)
            flirt_args['init'] = init_xfm

        if _do_bbr:
            schedule = op.join(os.environ['FSLDIR'], 'etc', 'flirtsch', 'bbr.sch')

            flirt_args = {
                **flirt_args,
                'cost': 'bbr',
                'wmseg': ref_boundarymask,
                'bbrslope': bbrslope,
                'bbrtype': bbrtype.name,
                'schedule': schedule,
                'init': init_affine
            }

        if _do_dc:
            # resample fmap and fmap_brainmask to ref space

            fmap_to_ref = op.join(tmp, 'fmap_to_ref.nii.gz')
            fmap_brainmask_to_ref = op.join(tmp, 'brainmask_to_ref.nii.gz')

            fsl.applyxfm(
                src=fmap,
                ref=ref,
                mat=fmap2ref_xfm,
                out=fmap_to_ref,
                interp='spline'
            )

            fsl.applyxfm(
                src=fmap_brainmask,
                ref=ref,
                mat=fmap2ref_xfm,
                out=fmap_brainmask_to_ref,
                interp='nearestneighbour'
            )

            flirt_args = {
                **flirt_args,
                'echospacing': src_echospacing,
                'pedir': src_pedir[0],
                'fieldmap': fmap_to_ref,
                'fieldmapmask': fmap_brainmask_to_ref,
            }

        # RUN FLIRT

        fsl.flirt(**flirt_args)

        # save flirt parameters to json

        util.dict2json(flirt_args, img.swapext(affine, '.json'))

        # TODO: compare init to main registration to capture gross BBR registration errors
        # compare init Tx to main Tx
        # init = np.loadtxt(src2ref_init_mat)
        # bbr = np.loadtxt(src2ref_mat)
        #
        # init_d = tx.decompose(init)
        # init_bbr = tx.decompose(bbr)
        #
        # print('Translations:    # print(init_d[1]-init_bbr[1])
        #
        # print('Rotations:')
        # print((np.array(init_d[2])-np.array(init_bbr[2])) * (180/np.pi))

        # invert mat

        inv_affine = workdir.get('inv_affine', make_dir=True)

        logutil.warn_file_exists(inv_affine)
        fsl.invxfm(affine, inv_affine)

        #  if not doing distortion correction, return
        if not _do_dc:
            src2ref = workdir.get('src2ref', make_dir=True)

            fsl.applyxfm(
                src=src,
                ref=ref,
                mat=affine,
                out=src2ref,
                interp='spline'
            )

            logger.info('end')
            return affine, inv_affine, src2ref

        # DO distortion correction

        warp = workdir.get('warp', make_dir=True)

        # transform fieldmap_brainmask from ref to src space

        fmap2src_mask = op.join(tmp, "fmap_to_src_brainmask.nii.gz")
        fsl.applyxfm(
            src=fmap_brainmask_to_ref,
            ref=src,
            mat=inv_affine,
            out=fmap2src_mask,
            interp='nearestneighbour')

        # transform fieldmap from ref to src space

        fmap2src = op.join(tmp, "fmap_to_src_phase.nii.gz")
        fsl.applyxfm(
            src=fmap_to_ref,
            ref=src,
            mat=inv_affine,
            out=fmap2src,
            interp='spline')
        fsl.fslmaths(input=fmap2src).mul(fmap2src_mask).run()  # remask

        # calculate shiftmap and combine with transform (src2ref)

        fmap2src_shift = op.join(tmp, "fmap_to_src_shift.nii.gz")

        fsl.fugue(
            loadfmap=fmap2src,
            mask=fmap2src_mask,
            saveshift=fmap2src_shift,
            unmaskshift=True,
            dwell=src_echospacing,
            unwarpdir=src_pedir[1])

        fsl.convertwarp(
            out=warp,
            ref=ref,
            postmat=affine,
            shiftmap=fmap2src_shift,
            shiftdir=src_pedir[1],
            abs=True,
            absout=True)

        # inverse warp

        inv_warp = workdir.get('inv_warp', make_dir=True)
        fsl.invwarp(
            inwarp=warp,
            ref=src,
            outwarp=inv_warp
        )

    fsl.applywarp(
        src=src,
        ref=ref,
        warp=warp,
        out=workdir.get('src2ref', make_dir=True),
        interp='spline'
    )

    logger.info('end')
    return affine, inv_affine, warp, inv_warp


@util.timeit
def nonlinear_reg(src,
                  ref,
                  dimension=3,
                  quick=False,
                  ref_brainmask=None,
                  basename: Optional[str] = None,
                  **kwargs):
    """
    ANTs deformable registration.

    Args:
        src:
        ref:
        dimension:
        quick:
        ref_brainmask:
        basename:
        **kwargs:

    Returns:

    """

    if isinstance(src, str):
        src = [src]

    if isinstance(ref, str):
        ref = [ref]

    # update output args
    basename = Path(basename)
    templates = dict(
        antsout='{basename}.ants/output_',
        affine='{basename}_affine.mat',
        warp='{basename}_warp.nii.gz',
        inv_warp='{basename}_invwarp.nii.gz',
        src2ref='{basename}_img.nii.gz',
    )

    # update templates from kwargs
    for k, v in kwargs.items():
        if k not in templates:
            raise ValueError(f'unknown output template {k}')
        templates[k] = v

    # setup basename
    if basename is None:
        basename = Path(src[0]).basename + '_to_' + Path(ref[0]).basename
        basename = Path(os.getcwd()).join(basename)

    variables = dict(
        basename=basename,
    )

    # create workdir filetree
    ft = FileTree(templates=templates, variables=variables)

    if Path(basename).dirname == '':
        basename = Path('.').join(basename)

    if not Path(basename).dirname.exists():
        os.makedirs(basename.dirname)

    # run ants reg

    antsRegistrationSyN(
        fixed=ref,
        moving=src,
        dimension=dimension,
        output_prefix=Path(ft.get('antsout', make_dir=True)),
        quick=quick,
        fixed_mask=ref_brainmask)

    # convert ants transform matrix to FSL

    cmd = f'c3d_affine_tool -ref {ref[0]} -src {src[0]} -itk {ft.get("antsout")}0GenericAffine.mat -ras2fsl -o {ft.get("affine", make_dir=True)}'
    shellops.run(os.getenv('DHCP_C3D_PATH', '') + '/' + cmd)

    # convert ants warp to FSL

    with TemporaryDirectory(dir=Path(ft.get("warp", make_dir=True)).dirname) as tmp:

        cmd = f'c3d -mcs {ft.get("antsout")}1Warp.nii.gz -oo {tmp}/wx.nii.gz {tmp}/wy.nii.gz {tmp}/wz.nii.gz'
        shellops.run(os.getenv('DHCP_C3D_PATH', '') + '/' + cmd)

        cmd = f'fslmaths {tmp}/wy -mul -1 {tmp}/i_wy'
        shellops.run(os.getenv('FSLDIR', '') + '/bin/' + cmd)

        cmd = f'fslmerge -t {ft.get("warp", make_dir=True)} {tmp}/wx {tmp}/i_wy {tmp}/wz'
        shellops.run(os.getenv('FSLDIR', '') + '/bin/' + cmd)

    cmd = f'convertwarp --ref={ref[0]} --premat={ft.get("affine")} --warp1={ft.get("warp")} --out={ft.get("warp")} '
    shellops.run(os.getenv('FSLDIR', '') + '/bin/' + cmd)

    cmd = f'invwarp -w {ft.get("warp")}  -o {ft.get("inv_warp", make_dir=True)}  -r {src[0]}'
    shellops.run(os.getenv('FSLDIR', '') + '/bin/' + cmd)

    cmd = f'applywarp -i {src[0]} -r {ref[0]} -w {ft.get("warp")} -o {ft.get("src2ref", make_dir=True)} --interp=spline'
    shellops.run(os.getenv('FSLDIR', '') + '/bin/' + cmd)


def antsRegistrationSyN(fixed,
                        moving,
                        dimension,
                        output_prefix,
                        fixed_mask=None,
                        quick=False):
    """
    Python wrapper for ANTs antsRegistrationSyN.

    Args:
        fixed:
        moving:
        dimension:
        output_prefix:
        fixed_mask:
        quick:

    Returns:

    """

    if isinstance(fixed, str):
        fixed = [fixed]

    if isinstance(moving, str):
        moving = [moving]

    assert len(fixed) == len(
        moving), 'The same number of fixed and moving images must be provided'

    # os.putenv('ANTSPATH', os.getenv('DHCP_ANTS_PATH', ''))
    os.environ['ANTSPATH'] = os.getenv('DHCP_ANTS_PATH', '')

    if quick:
        cmd = "antsRegistrationSyNQuick.sh"
    else:
        cmd = "antsRegistrationSyN.sh"

    cmd = op.join(os.getenv('DHCP_ANTS_PATH', ''), cmd)
    cmd += ' -d {0}'.format(dimension)

    for i in range(len(fixed)):
        cmd += ' -f {0} -m {1}'.format(fixed[i], moving[i])

    cmd += ' -o {0}'.format(output_prefix)
    cmd += ' -t s -j 1'

    if fixed_mask is not None:
        cmd += ' -x {0}'.format(fixed_mask)

    print(shellops.run(cmd))

    # print(cmd)

# TODO: replace antsRegistrationSyN with antsRegistration

# /opt/ants-2.2.0/antsRegistration
# --verbose 1
# --dimensionality 3
# --float 0
# --output [/opt/data/dhcp/test/standard2struct_,/opt/data/dhcp/test/standard2struct_Warped.nii.gz,/opt/data/dhcp/test/standard2struct_InverseWarped.nii.gz]
# --interpolation Linear
# --use-histogram-matching 1
# --winsorize-image-intensities [0.005,0.995]
# --initial-moving-transform [/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T2w.nii.gz,/opt/data/atlas/atlas-schuh/T2/template-40.nii.gz,1]

# --transform Rigid[0.1]
# --metric MI[/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T2w.nii.gz,/opt/data/atlas/atlas-schuh/T2/template-40.nii.gz,1,32,Regular,0.25]
# --convergence [1000x500x250x0,1e-6,10]
# --shrink-factors 12x8x4x2
# --smoothing-sigmas 4x3x2x1vox
# -x [NULL,NULL]

# --transform Affine[0.1]
# --metric MI[/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T2w.nii.gz,/opt/data/atlas/atlas-schuh/T2/template-40.nii.gz,1,32,Regular,0.25]
# --convergence [1000x500x250x0,1e-6,10]
# --shrink-factors 12x8x4x2
# --smoothing-sigmas 4x3x2x1vox
# -x [NULL,NULL]

# --transform SyN[0.1,3,0]
# --metric MI[/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T2w.nii.gz,/opt/data/atlas/atlas-schuh/T2/template-40.nii.gz,1,32]
# --metric MI[/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T1w_space-T2w.nii.gz,/opt/data/atlas/atlas-schuh/T1/template-40.nii.gz,1,32]
# --convergence [100x100x70x50x0,1e-6,10]
# --shrink-factors 10x6x4x2x1
# --smoothing-sigmas 5x3x2x1x0vox
# -x [/opt/data/dhcp/test/sub-CC00108XX09/ses-xxxxx/struct/T2w_brainmask.nii.gz, NULL]
