#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Surface projection stage of the dHCP neonatal fMRI pipeline.
"""
import os
from tempfile import TemporaryDirectory

import nibabel as nb
import numba
import numpy as np
import toblerone
from fsl.data.image import Image
from fsl.transform.nonlinear import applyDeformation, DeformationField
from fsl.utils.filetree import FileTree
from nibabel import cifti2
from nilearn import image
from sklearn import mixture

import dhcp.util.surf2vol_coord as svc
from dhcp.util import assertions as assrt
from dhcp.util import fslpy as fsl
from dhcp.util import mask
from dhcp.util import shellops
from dhcp.util import util
from dhcp.util.enums import SegType
from dhcp.util.io import Path

DEFAULTS = FileTree(
    templates=dict(
        pve='{workdir}/pve.nii.gz',
        gm_mask='{workdir}/gm_mask.nii.gz',
        pvc_gm='{workdir}/pvc_gm.nii.gz',
        pvc_wm='{workdir}/pvc_wm.nii.gz',
        pvc_csf='{workdir}/pvc_csf.nii.gz',
        pvc_residual='{workdir}/pvc_residual.nii.gz',
        func_gii='{workdir}/func_hemi-{hemi}_mesh-{topo}.func.gii',
        signal_mask='{workdir}/signal_mask.nii.gz',
        good_voxels='{workdir}/good_voxels.nii.gz',
        sigloss_mask='{workdir}/sigloss_mask.nii.gz',
        func_cifti='{workdir}/func_mesh-{topo}_space-{space}.dtseries.nii',
        white_surf='{workdir}/white_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        pial_surf='{workdir}/pial_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        mid_surf='{workdir}/midthickness_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        inflated_surf='{workdir}/inflated_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        veryinflated_surf='{workdir}/veryinflated_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        midthickness_surf='{workdir}/midthickness_hemi-{hemi}_mesh-{topo}_space-{space}.surf.gii',
        standard='{workdir}/standard_{res}mm.nii.gz',
        standard_scmask='{workdir}/standard_scmask_{res}mm.nii.gz'
    ),
    variables={'workdir': './'},
)


def _blkidx(n_s=3):
    """Calculate indices for a 3x3x3 grid around an index of 0"""
    return (np.array(np.where(np.zeros((n_s, n_s, n_s)) == 0)).T - (
            (n_s - 1) / 2)).astype(int)


@util.timeit
def pvc(func: str,
        pve: str,
        pvc_gm: str,
        pvc_wm: str,
        pvc_csf: str,
        pvc_residual: str,
        kernel=1):
    func = nb.load(func)
    pve = nb.load(pve)

    # find coords of voxels where GM PVE > 0
    # we are not interested in voxels without any GM
    mask_coord = np.array(np.where(pve.get_fdata()[:, :, :, 0] > 0)).T

    # generate neighbourhood voxel coords [m x n]
    #  [
    #     [ vox_crd_1, neighbour_crd_1_1, ..., neighbour_crd_1_n],
    #     [ vox_crd_2, neighbour_crd_2_1, ..., neighbour_crd_2_n],
    #     ...
    #     [ vox_crd_m, neighbour_crd_m_1, ..., neighbour_crd_m_n],
    #  ]

    n_s = 1 + (kernel * 2)

    blkcrd = _blkidx(n_s=n_s)
    x_crd = mask_coord[:, 0][:, np.newaxis] + blkcrd[:, 0][np.newaxis, :]
    y_crd = mask_coord[:, 1][:, np.newaxis] + blkcrd[:, 1][np.newaxis, :]
    z_crd = mask_coord[:, 2][:, np.newaxis] + blkcrd[:, 2][np.newaxis, :]

    # restrict neighbourhood voxel coords to be within the FOV
    x_crd = np.clip(x_crd, 0, pve.shape[0] - 1)
    y_crd = np.clip(y_crd, 0, pve.shape[1] - 1)
    z_crd = np.clip(z_crd, 0, pve.shape[2] - 1)

    # split PVE
    gm_pve = pve.get_fdata()[:, :, :, 0]
    wm_pve = pve.get_fdata()[:, :, :, 1]
    csf_pve = pve.get_fdata()[:, :, :, 2]

    # RUN PVC
    func_data = func.get_fdata()
    if len(func_data.shape) == 3:
        func_data = func_data[:, :, :, np.newaxis]

    gm_b, wm_b, csf_b = _pvc_main_loop(
        func_data,
        pve.get_fdata(),
        mask_coord,
        x_crd,
        y_crd,
        z_crd,
    )

    # refold the PVC

    def refold(b0):
        b1 = np.zeros(func_data.shape)
        b1[mask_coord[:, 0], mask_coord[:, 1], mask_coord[:, 2]] = b0
        return b1

    gm_b = refold(gm_b)
    wm_b = refold(wm_b)
    csf_b = refold(csf_b)

    # save PVC outputs

    def write(b0, pve0, outname):
        pvc0 = b0 * (pve0[:, :, :, np.newaxis] > 0).astype(int)
        nb.Nifti1Image(pvc0.astype(float), affine=func.affine, header=func.header).to_filename(outname)

    write(gm_b, gm_pve, pvc_gm)
    write(wm_b, wm_pve, pvc_wm)
    write(csf_b, csf_pve, pvc_csf)

    # calculate residual

    residual = func_data - (gm_b * gm_pve[:, :, :, np.newaxis]) - (wm_b * wm_pve[:, :, :, np.newaxis]) - (
            csf_b * csf_pve[:, :, :, np.newaxis])

    nb.Nifti1Image(residual, affine=func.affine, header=func.header).to_filename(
        pvc_residual)


@numba.jit(nopython=True)
def _pvc_main_loop(func, pve, mask_coord, x_crd, y_crd, z_crd):
    n_blk = x_crd.shape[1]       # number of voxels in neighbourhood block
    n_vox = mask_coord.shape[0]  # number of voxels to correct
    n_vol = 1 if len(func.shape) == 3 else func.shape[3]  # number of volumes/time-points

    # initialise beta storage (per-tissue)
    gm_b = np.zeros((n_vox, n_vol))
    wm_b = np.zeros((n_vox, n_vol))
    csf_b = np.zeros((n_vox, n_vol))

    # loop over each volume/time-point and correct independently
    for vol_idx in range(n_vol):

        # grab single func volume
        func0 = func[:, :, :, vol_idx]

        # initialise per-volume storage
        b = np.zeros((n_vox, 3))
        X = np.zeros((n_blk, 3))
        y = np.zeros(n_blk)

        # loop through voxels (per vol) and perform PVC on each neighbourhood block (centered on voxel)
        for vox_idx in range(n_vox):

            # loop over neighbourhood coords and collect pve (X) and func (y) values
            for blk_idx, (x0, y0, z0) in enumerate(zip(x_crd[vox_idx, :], y_crd[vox_idx, :], z_crd[vox_idx, :])):
                X[blk_idx, :] = pve[x0, y0, z0, :]
                y[blk_idx] = func0[x0, y0, z0]

            # solve for beta (pure tissue signal)
            b[vox_idx, :] = np.dot(np.linalg.pinv(X), y)

        gm_b[:, vol_idx] = b[:, 0]
        wm_b[:, vol_idx] = b[:, 1]
        csf_b[:, vol_idx] = b[:, 2]

    return gm_b, wm_b, csf_b


def asl_pvc(func, func_brainmask, pve, pvc_gm, pvc_wm, kernel=1):
    with TemporaryDirectory(dir=pvc_gm.dirname) as tmp:
        func = nb.load(func)
        brainmask = nb.load(func_brainmask).get_fdata() > 0

        pve = nb.load(pve)

        pve_gm = image.index_img(pve, 0)
        pve_gm.to_filename(Path(tmp).join('pve_gm.nii.gz'))

        pve_wm = image.index_img(pve, 1)
        pve_wm.to_filename(Path(tmp).join('pve_wm.nii.gz'))

        pvc_gm_data = []
        pvc_wm_data = []

        for idx, func0 in enumerate(image.iter_img(func)):
            print(idx, end=' ')

            func0.to_filename(Path(tmp).join(f'img0.nii.gz'))

            out = shellops.run([
                'asl_file',
                f'--data={func0.get_filename()}',
                '--ntis=1',
                '--iaf=diff',
                f'--mask={func_brainmask}',
                f'--pvgm={tmp}/pve_gm.nii.gz',
                f'--pvwm={tmp}/pve_wm.nii.gz',
                f'--out={tmp}/pvc',
                f'--kernel={kernel}'
            ])

            pvc_gm0 = nb.load(f'{tmp}/pvc_gm.nii.gz').get_fdata() * (pve_gm.get_fdata() > 0)
            pvc_wm0 = nb.load(f'{tmp}/pvc_wm.nii.gz').get_fdata() * (pve_wm.get_fdata() > 0)

            pvc_gm_data.append(pvc_gm0)
            pvc_wm_data.append(pvc_wm0)

    print('Done')
    nb.Nifti1Image(np.stack(pvc_gm_data, axis=3), func.affine, header=func.header).to_filename(pvc_gm)
    nb.Nifti1Image(np.stack(pvc_wm_data, axis=3), func.affine, header=func.header).to_filename(pvc_wm)

    return out


def _applyxfm(mat, src, ref, coord):
    coord = svc.vox2fslmm(coord, src)
    coord = svc.xfm(mat, coord)
    coord = svc.fslmm2vox(coord, ref)
    return coord


def _writemask(name, crd, ref, shape=None):
    if not shape:
        shape = ref.shape
    msk = _makemask(crd, shape)
    newimg = nb.Nifti1Image(msk, ref.affine, ref.header)
    nb.save(newimg, name)


def _makemask(crd, shape):
    shape = np.array(shape)
    idx = np.logical_not(np.any(crd > shape - 1, axis=1))
    crd = crd[idx, :]
    msk = np.zeros(shape)
    np.add.at(msk, tuple(crd.T), 1)
    return msk


def sample(func,
           struct,
           mid_surf,
           struct_to_func_xfm,
           func_gii,
           structure,
           surf_mask=None,
           vox_mask=None,
           debug=False):
    func = nb.load(func)
    func_d = func.get_fdata()

    struct = nb.load(struct)
    struct_to_func_xfm = np.loadtxt(struct_to_func_xfm)

    midsrf = nb.load(mid_surf)
    mid_crd_niimm = midsrf.get_arrays_from_intent('NIFTI_INTENT_POINTSET').pop().data
    mid_crd = svc.niimm2vox(mid_crd_niimm, struct)

    mid_crd_func = _applyxfm(struct_to_func_xfm, struct, func, mid_crd)
    mid_crd_func = svc.int_coord(mid_crd_func)

    mid_crd_func[:, 0] = np.clip(mid_crd_func[:, 0], 0, func_d.shape[0] - 1)
    mid_crd_func[:, 1] = np.clip(mid_crd_func[:, 1], 0, func_d.shape[1] - 1)
    mid_crd_func[:, 2] = np.clip(mid_crd_func[:, 2], 0, func_d.shape[2] - 1)

    if debug:
        _writemask("mid_struct_mask.nii.gz", svc.int_coord(mid_crd), struct)
        _writemask("mid_func_mask.nii.gz", svc.int_coord(mid_crd_func), func, shape=func.shape[0:3])

    if vox_mask is not None:
        vox_mask = nb.load(vox_mask).get_fdata()
        if np.ndim(vox_mask) == 3:
            vox_mask = vox_mask[:, :, :, np.newaxis]
            func_d = func_d * (vox_mask > 0)

    mid_func = func_d[mid_crd_func[:, 0], mid_crd_func[:, 1], mid_crd_func[:, 2], :]

    if surf_mask is not None:
        surf_mask = nb.load(surf_mask)
        surf_mask = surf_mask.get_arrays_from_intent('NIFTI_INTENT_NORMAL').pop().data
        mid_func = mid_func * (surf_mask[:, np.newaxis] > 0)

    metadict = {'AnatomicalStructurePrimary': structure.name}
    meta = nb.gifti.GiftiMetaData().from_dict(metadict)
    gii = nb.gifti.gifti.GiftiImage(meta=meta)

    for i in range(mid_func.shape[1]):
        darray = nb.gifti.gifti.GiftiDataArray(
            data=mid_func[:, i].astype(np.float32),
            intent='NIFTI_INTENT_TIME_SERIES'
        )
        gii.add_gifti_data_array(darray)

    gii.to_filename(func_gii)


def gmm(func, func_brainmask, maskname):
    clf = None
    brainmask = []

    func = nb.load(func)
    func_brainmask = nb.load(func_brainmask)

    for func0 in image.iter_img(func):
        # brain_mask, CLF = _gmm(func0, nb.load(func_brainmask), clf=CLF)
        # mask.append(brain_mask.get_fdata())

        d_0 = func0.get_fdata() * (func_brainmask.get_fdata() > 0)

        shape = d_0.shape
        d_0 = np.reshape(d_0, (-1, 1))

        keep_idx = np.where(d_0 > 0)

        d_1 = d_0[keep_idx]
        d_1 = d_1[:, np.newaxis]

        if clf is None:
            init = [[0], [np.median(d_1)]]
            clf = mixture.GaussianMixture(
                n_components=len(init),
                covariance_type='full',
                means_init=init,
                warm_start=True)

        clf.fit(d_1)

        d_1 = clf.predict(d_1)

        d_0 = np.zeros(d_0.shape)
        d_0[keep_idx] = d_1

        d_0 = np.reshape(d_0, shape)

        brainmask.append(d_0 == 1)

    return nb.Nifti1Image(np.stack(brainmask, axis=3), func.affine, header=func.header).to_filename(maskname)


@util.timeit
def toblerone_pve(white_surf_right,
                  white_surf_left,
                  pial_surf_right,
                  pial_surf_left,
                  ref,
                  struct,
                  struct_to_ref_xfm,
                  pve_name):

    pve0 = toblerone.pvestimation.cortex(
        LWS=white_surf_left,
        LPS=pial_surf_left,
        RWS=white_surf_right,
        RPS=pial_surf_right,
        ref=ref,
        struct2ref=str(struct_to_ref_xfm),
        struct=struct,
        flirt=True
    )

    spc = toblerone.classes.ImageSpace(ref)
    spc.save_image(pve0, pve_name)


# TODO: Bias correction
# TODO: bridge voxels
# TODO: ? use good voxels mask in PVC
# TODO: local coefficient of variation exclusion (as per HCP)
# TODO: ? use PVC for signal loss


@util.timeit
def sample_to_native(func: str,
                     func_brainmask: str,
                     struct_white_surf_right: str,
                     struct_white_surf_left: str,
                     struct_pial_surf_right: str,
                     struct_pial_surf_left: str,
                     struct_mid_surf_right: str,
                     struct_mid_surf_left: str,
                     medialwall_shape_left: str,
                     medialwall_shape_right: str,
                     struct: str,
                     struct_to_func_affine: str,
                     struct_subcortical_mask: str,
                     pvc_kernel=1,
                     pve_threshold=0,
                     workdir=None,
                     do_sigloss=False,
                     pve: str = None,
                     ):
    # check inputs
    assrt.assert_file_exists(func, func_brainmask, struct_white_surf_left, struct_white_surf_right,
                             struct_pial_surf_left, struct_pial_surf_right,
                             struct_mid_surf_left, struct_mid_surf_right, medialwall_shape_left,
                             medialwall_shape_right, struct,
                             struct_to_func_affine, struct_subcortical_mask)
    assrt.assert_is_nifti_4D(func)
    assrt.assert_is_nifti_3D(func_brainmask, struct, struct_subcortical_mask)
    assrt.assert_is_surf_gifti(struct_white_surf_right, struct_white_surf_left, struct_pial_surf_right,
                               struct_pial_surf_left, struct_mid_surf_right,
                               struct_mid_surf_left)
    assrt.assert_is_shape_gifti(medialwall_shape_left, medialwall_shape_right)

    if workdir is None:
        workdir = Path(func).splitext()[0] + '.surface'

    defaults = DEFAULTS.update(workdir=workdir, space='func', topo='native')

    ############
    # Partial Volume Estimation (in native func space)
    ############

    if pve is not None:
        defaults.templates['pve'] = pve
    else:
        print('Running partial volume estimation')
        toblerone_pve(
            white_surf_right=struct_white_surf_right,
            white_surf_left=struct_white_surf_left,
            pial_surf_right=struct_pial_surf_right,
            pial_surf_left=struct_pial_surf_left,
            ref=func,
            struct=struct,
            struct_to_ref_xfm=struct_to_func_affine,
            pve_name=defaults.get('pve', make_dir=True),
        )

    ############
    # Partial Volume Correction [per volume] (in native func space)
    ############

    print('Running partial correction')

    pvc(
        func=func,
        func_brainmask=func_brainmask,
        pve=defaults.get('pve'),
        pvc_gm=defaults.get('pvc_gm', make_dir=True),
        pvc_wm=defaults.get('pvc_wm', make_dir=True),
        pvc_csf=defaults.get('pvc_csf', make_dir=True),
        pvc_residual=defaults.get('pvc_residual', make_dir=True),
        kernel=pvc_kernel,
        pve_threshold=pve_threshold,
    )

    ############
    # Signal-loss estimation
    ############

    print('Running signal-loss estimation')

    good_voxels = defaults.get('good_voxels', make_dir=True)
    signal_mask = defaults.get('signal_mask', make_dir=True)
    sigloss_mask = defaults.get('sigloss_mask', make_dir=True)

    with TemporaryDirectory(dir=workdir) as tmp:

        gm_thres = Path(tmp).join('gm_thres.nii.gz')

        pve = nb.load(defaults.get('pve'))
        gm_pve = (image.index_img(pve, 0).get_fdata()) > pve_threshold

        if do_sigloss:
            nb.Nifti1Image(
                gm_pve,
                header=pve.header,
                affine=pve.affine
            ).to_filename(gm_thres)

            gmm(func, func_brainmask, signal_mask)

            fsl.fslmaths(gm_thres).mas(signal_mask).bin().run(good_voxels)
            fsl.fslmaths(func_brainmask).sub(signal_mask).bin().run(sigloss_mask)
        else:
            nb.Nifti1Image(
                gm_pve,
                header=pve.header,
                affine=pve.affine
            ).to_filename(good_voxels)

    ############
    # Sample to the surface
    ############

    print('Sample to surface')

    # transform white/pial/midthickness surfaces from native struct to func space (rigid)
    for surf in ['white', 'pial', 'mid']:
        for hemi in ['left', 'right']:
            _wb_surface_apply_affine(
                locals()[f'struct_{surf}_surf_{hemi}'],
                struct_to_func_affine,
                struct,
                defaults.update(hemi=hemi).get(f'{surf}_surf', make_dir=True),
                func
            )

    for hemi in ['left', 'right']:
        defaults_hemi = defaults.update(hemi=hemi)

        _wb_sample(
            func=defaults.get('pvc_gm'),
            func_gii=defaults_hemi.get('func_gii', make_dir=True),
            mid_surf=defaults_hemi.get('mid_surf', make_dir=True),
            white_surf=defaults_hemi.get('white_surf', make_dir=True),
            pial_surf=defaults_hemi.get('pial_surf', make_dir=True),
            volume_roi=defaults.get('good_voxels'),
        )

        # TODO: do _metric_dilate_and_mask in func space, currently struct for some reason
        #       this should not matter but will be consistent with HCP
        _metric_dilate_and_mask(
            func_gii=defaults_hemi.get('func_gii'),
            surf_gii=locals()[f'struct_mid_surf_{hemi}'],
            surf_mask=locals()[f'medialwall_shape_{hemi}'],
        )

    with TemporaryDirectory(dir=workdir) as tmp:
        tmp = Path(tmp)

        # resample subcortical mask to func space

        fsl.fslroi(
            input=func,
            output=tmp.join('func0.nii.gz'),
            tmin=0,
            tsize=1
        )

        fsl.applyxfm(
            src=struct_subcortical_mask,
            ref=tmp.join('func0.nii.gz'),
            mat=struct_to_func_affine,
            out=tmp.join('scmask.nii.gz'),
            interp='trilinear'
        )

        # create cifti

        func = nb.load(func)
        subcortical_mask = nb.load(tmp.join('scmask.nii.gz'))

        _to_cifti(
            func_gii_left=defaults.update(hemi='left', topo='native').get('func_gii'),
            func_gii_right=defaults.update(hemi='right', topo='native').get('func_gii'),
            medialwall_shape_left=medialwall_shape_left,
            medialwall_shape_right=medialwall_shape_right,
            subcort_mask=tmp.join('scmask.nii.gz'),
            subcort_ts=func.get_fdata()[subcortical_mask.get_fdata().astype(bool), :],
            func_tr=func.header.get_zooms()[-1],
            cifti_name=defaults.update(topo='native', space='func').get('func_cifti', make_dir=True)
        )


def _readgii(funcname):
    srf = nb.load(funcname)
    d = np.stack([d.data for d in srf.darrays], axis=1)
    return d, srf.meta


def _wb_surface_apply_affine(struct_surf,
                             struct_to_func_affine,
                             struct,
                             func_surf,
                             func):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')

    cmd = [
        wb_cmd,
        '-surface-apply-affine',
        struct_surf,
        struct_to_func_affine,
        func_surf,
        '-flirt',
        struct,
        func,
    ]

    return shellops.run(cmd)


@util.timeit
def _wb_sample(func,
               func_gii,
               mid_surf,
               white_surf,
               pial_surf,
               volume_roi,
               subdiv=10):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')

    cmd = [
        wb_cmd,
        '-volume-to-surface-mapping',
        func,
        mid_surf,
        func_gii,
        '-ribbon-constrained',
        white_surf,
        pial_surf,
        '-voxel-subdiv',
        str(subdiv),
        '-volume-roi',
        volume_roi
    ]

    return shellops.run(cmd)


def _metric_dilate_and_mask(func_gii, surf_gii, surf_mask, distance=10):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')

    # metric dilate
    shellops.run([
        wb_cmd,
        '-metric-dilate',
        func_gii,
        surf_gii,
        str(distance),
        func_gii,
        '-nearest'
    ])

    # metric mask
    shellops.run([
        wb_cmd,
        '-metric-mask',
        func_gii,
        surf_mask,
        func_gii,
    ])


def resample_to_template(func: str,
                         func_gii_left: str,
                         func_gii_right: str,
                         sphere_reg_left: str,
                         sphere_reg_right: str,
                         struct_white_surf_right: str,
                         struct_white_surf_left: str,
                         struct_pial_surf_right: str,
                         struct_pial_surf_left: str,
                         struct_inflated_surf_right: str,
                         struct_inflated_surf_left: str,
                         struct_veryinflated_surf_right: str,
                         struct_veryinflated_surf_left: str,
                         struct_midthickness_surf_right: str,
                         struct_midthickness_surf_left: str,
                         func2template_warp: str,
                         struct2template_warp: str,
                         struct2template_invwarp: str,
                         volumetric_template: str,
                         volumetric_template_dseg: str,
                         template_sphere_left: str,
                         template_sphere_right: str,
                         template_medialwall_left: str,
                         template_medialwall_right: str,
                         workdir: str = None,
                         vox_isores=1.5,
                         volumetric_template_dseg_type: SegType = SegType.drawem,
                         template_space_label='extdhcp40wk',
                         template_mesh_label='fsLR32k',
                         ):
    # downsample volumetric template/atlas to vox_isores (default = 1.5mm iso)

    defaults = DEFAULTS.update(workdir=workdir, res=vox_isores)

    fsl.apply_isoxfm(
        src=volumetric_template,
        res=vox_isores,
        out=defaults.get('standard'),
        interp='spline'
    )

    if not volumetric_template_dseg_type == SegType.drawem:
        raise RuntimeError(f'unsupported dseg type ({volumetric_template_dseg_type})')

    # create volumetric template/atlas subcortial mask and downsample to vox_isores (default = 1.5mm iso)

    mask.create_mask(
        dseg=volumetric_template_dseg,
        dseg_type=volumetric_template_dseg_type,
        labels=['sc', 'bs', 'cb'],
        outname=defaults.get('standard_scmask'),
    )

    fsl.apply_isoxfm(
        src=defaults.get('standard_scmask'),
        res=vox_isores,
        out=defaults.get('standard_scmask'),
        interp='trilinear'
    )
    fsl.fslmaths(defaults.get('standard_scmask')).thr(0.5).bin().run()

    # resample native func metric to have vtx correspondence with 32k template surface

    for hemi in ['left', 'right']:
        _metric_resample(
            metric_in=locals()[f'func_gii_{hemi}'],
            current_sphere=locals()[f'sphere_reg_{hemi}'],
            new_sphere=locals()[f'template_sphere_{hemi}'],
            metric_out=defaults.update(hemi=hemi, topo=template_mesh_label).get('func_gii'),
        )

    # resample native functional subcortical voxels to volumetric template space

    sc_ts = _subcort_resample(
        func=func,
        template=defaults.get('standard'),
        func2template_warp=func2template_warp,
        mask=defaults.get('standard_scmask')
    )

    # make cifti image

    _to_cifti(
        func_gii_left=defaults.update(hemi='left', topo=template_mesh_label).get('func_gii'),
        func_gii_right=defaults.update(hemi='right', topo=template_mesh_label).get('func_gii'),
        medialwall_shape_left=template_medialwall_left,
        medialwall_shape_right=template_medialwall_right,
        subcort_mask=defaults.get('standard_scmask'),
        subcort_ts=sc_ts,
        func_tr=nb.load(func).header.get_zooms()[-1],
        cifti_name=defaults.update(topo=template_mesh_label, space=template_space_label).get('func_cifti',
                                                                                             make_dir=True)
    )

    # resample surface geometry from native structural to dHCP template volumetric space

    for hemi in ['left', 'right']:
        for surf in ['inflated', 'veryinflated', 'pial', 'white', 'midthickness']:
            # resample
            _surface_resample(
                surf_in=locals()[f'struct_{surf}_surf_{hemi}'],
                current_sphere=locals()[f'sphere_reg_{hemi}'],
                new_sphere=locals()[f'template_sphere_{hemi}'],
                surf_out=defaults.update(topo=template_mesh_label, space='struct', hemi=hemi).get(f'{surf}_surf')
            )

            # warp
            _surface_apply_warpfield(
                surf_in=defaults.update(topo=template_mesh_label, space='struct', hemi=hemi).get(f'{surf}_surf'),
                warp=struct2template_warp,
                warp_inv=struct2template_invwarp,
                surf_out=defaults.update(topo=template_mesh_label, space=template_space_label, hemi=hemi).get(
                    f'{surf}_surf'),
            )


def _to_cifti(func_gii_left,  # in desired surface vertex topology
              func_gii_right,
              medialwall_shape_left,  # in desired surface vertex topology
              medialwall_shape_right,
              subcort_mask,  # in desired subcortical voxel space
              subcort_ts,  # in desired subcortical voxel space
              func_tr,
              cifti_name,
              ):
    # create cortical surface model

    ctx_roi_l = _readgii(medialwall_shape_left)[0].astype(bool)
    ctx_roi_r = _readgii(medialwall_shape_right)[0].astype(bool)

    bm_cortex_l = cifti2.BrainModelAxis.from_mask(ctx_roi_l.ravel(), name='cortex_left')
    bm_cortex_r = cifti2.BrainModelAxis.from_mask(ctx_roi_r.ravel(), name='cortex_right')

    # create subcortical voxel model

    sc_mask = nb.load(subcort_mask)
    bm_subcort = cifti2.BrainModelAxis.from_mask(sc_mask.get_fdata(), affine=sc_mask.affine, name='other')

    # create full model

    bm_full = bm_cortex_l + bm_cortex_r + bm_subcort

    # add functional data

    # fd = nb.load(func) if isinstance(func, str) else func

    ctx_l = _readgii(func_gii_left)[0]
    ctx_r = _readgii(func_gii_right)[0]

    series = cifti2.SeriesAxis(start=0, step=func_tr, size=subcort_ts.shape[-1])
    header = cifti2.Cifti2Header.from_axes((series, bm_full))

    dd = np.concatenate(
        (
            ctx_l[ctx_roi_l.ravel(), :],
            ctx_r[ctx_roi_r.ravel(), :],
            subcort_ts
        ),
        axis=0)

    # write CIFTI
    nb.Cifti2Image(dd.T, header=header).to_filename(cifti_name)


def _metric_resample(metric_in, current_sphere, new_sphere, metric_out):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')
    metric_out = shellops.run([
        wb_cmd,
        '-metric-resample',
        metric_in,
        current_sphere,
        new_sphere,
        'BARYCENTRIC',
        metric_out]
    )
    return metric_out


def _surface_resample(surf_in, current_sphere, new_sphere, surf_out):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')
    cmd = [
        wb_cmd,
        '-surface-resample',
        surf_in,
        current_sphere,
        new_sphere,
        'BARYCENTRIC',
        surf_out,
    ]
    return shellops.run(cmd)


def _surface_apply_warpfield(surf_in, warp, warp_inv, surf_out):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')
    cmd = [
        wb_cmd,
        '-surface-apply-warpfield',
        surf_in,
        warp_inv,
        surf_out,
        '-fnirt',
        warp,
    ]
    return shellops.run(cmd)


def _subcort_resample(func, template, func2template_warp, mask):
    func = nb.load(func)
    Nt = func.shape[-1]

    template = nb.load(template)

    mask = nb.load(mask)
    mask_d = mask.get_fdata().ravel().astype(bool)

    d = []

    for idx in range(Nt):
        func0 = nb.Nifti1Image(func.get_fdata()[:, :, :, idx], header=func.header, affine=func.affine)
        d0 = applyDeformation(Image(func0), DeformationField(func2template_warp, Image(func0)), ref=Image(template),
                              mode='constant', cval=0)
        d0 = d0.ravel()[mask_d]
        d += [d0]

    return np.stack(d, axis=1)


def _cifti_smoothing(cifti_in, surf_sigma, vol_sigma, surf_left, surf_right, cifti_out):
    wb_cmd = os.getenv('DHCP_WBCMD', 'wb_command')
    cmd = [
        wb_cmd,
        '-cifti-smoothing',
        cifti_in,
        f'{surf_sigma}',
        f'{vol_sigma}',
        'COLUMN',
        cifti_out,
        '-left-surface',
        surf_left,
        '-right-surface',
        surf_right,
        '-fix-zeros-volume',
        '-fix-zeros-surface',
    ]

    print(' '.join(cmd))
    return shellops.run(cmd)
