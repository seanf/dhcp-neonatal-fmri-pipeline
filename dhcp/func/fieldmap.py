#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Fieldmap preprocssing for the dHCP neonatal fMRI pipeline.
"""
import json
import logging
import os
import os.path as op
import pprint
import tempfile
import warnings
from typing import Optional, List

import nibabel as nb
import numpy as np
from fsl.utils.filetree import FileTree

from dhcp.util import util, fslpy as fsl
from dhcp.util.acqparam import main as acqparam, _get_axis as get_axis
from dhcp.util.enums import PhaseEncodeDirection
from dhcp.util.io import Path
from dhcp.util.util import validate_path_input

DEFAULTS = FileTree.read(util.get_resource('fieldmap.tree'), partial_fill=False)


@util.timeit
def fieldmap(spinecho: str,
             echospacing: float,
             pedir: List[PhaseEncodeDirection],
             epifactor: Optional[int] = None,
             inplaneaccel: Optional[float] = 1.0,
             pedir_n: Optional[int] = 1,
             config: Optional[str] = None,
             workdir: Optional[str] = None,
             brain_thr: Optional[float] = 20.0):
    """
    Prepare fieldmaps.

    Args:
        spinecho:           Name of 4D file with spinecho images (reversed phase encode direction)
        echospacing:        Spinecho effective EPI echo spacing (seconds)
        pedir:              Spinecho phase encoding direction
        epifactor:          Spinecho epi factor (k-space lines in single epi shot)(default=calculated from spinecho)
        inplaneaccel:       Spinecho in-plane acceleration factor (default=1)
        pedir_n:
        config:
        workdir:            Base path for pipeline outputs
        brain_thr:

    Returns:

    """

    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    # setup outputs
    outputs = DEFAULTS
    if workdir is not None:
        outputs = outputs.update(workdir=workdir)

    # check defaults is complete
    out_names = [
        'spinecho_acqp', 'spinecho_best', 'topup_fout', 'topup_iout', 'topup_log',
        'fmap_magnitude', 'fmap', 'fmap_brainmask', 'topup/basename', 'spinecho_best_acqp',
        'spinecho_best_json'
    ]
    outputs.defines(out_names, error=True)

    spinecho = validate_path_input(spinecho, ndim=4)

    acqparam(
        spinecho,
        echospacing=echospacing,
        pedir=','.join([p.name for p in pedir]),
        inplaneaccel=inplaneaccel,
        epifactor=epifactor,
        outname=outputs.get('spinecho_acqp', make_dir=True)
    )

    if not config:
        config = op.join(os.environ['FSLDIR'], 'etc', 'flirtsch', 'b02b0.cnf')
    logger.debug(F'config: {config}')

    # pick best B0

    zsmth = spinecho_zsmoothness(spinecho, brain_thr=brain_thr)
    idx = pick_best_b0(zsmth, outputs.get('spinecho_acqp'), n=pedir_n)

    with open(outputs.get('spinecho_best_json', make_dir=True), 'w') as f:
        json.dump(dict(idx=idx, zsmoothness=zsmth), f, indent=4)

    se = nb.load(spinecho)
    nb.Nifti1Image(se.get_data()[:, :, :, idx], se.affine, se.header).to_filename(outputs.get('spinecho_best'))
    np.savetxt(outputs.get('spinecho_best_acqp'), np.loadtxt(outputs.get('spinecho_acqp'))[idx, :], fmt='%i %i %i %f')

    # run topup
    logger.info('running TOPUP')

    fsl.topup(
        imain=Path(outputs.get('spinecho_best')),
        datain=Path(outputs.get('spinecho_best_acqp')),
        config=config,
        fout=outputs.get('topup_fout', make_dir=True),
        iout=outputs.get('topup_iout', make_dir=True),
        out=outputs.get('topup/basename', make_dir=True),
        verbose=True,
        subsamp="1,1,1,1,1,1,1,1,1",
        logout=outputs.get('topup_log', make_dir=True))

    fsl.fslmaths(input=outputs.get('topup_fout')).mul(6.2832).run(output=outputs.get('fmap'))
    fsl.fslmaths(input=outputs.get('topup_iout')).Tmean().run(output=outputs.get('fmap_magnitude'))

    with tempfile.TemporaryDirectory(dir=Path(outputs.get('topup/basename')).dirname) as tmp:
        brain = Path(tmp).join('brain.nii.gz')
        fsl.bet(input=outputs.get('fmap_magnitude'), output=brain, mask=False, robust=True)
        fsl.fslmaths(brain).bin().run(outputs.get('fmap_brainmask'))

    logger.info('end')


def spinecho_mse(spinecho: Path, acqp: Path) -> np.array:
    """
    Calculate mean squared error (sum of squared differences) of the spin-echo.

    Args:
        spinecho:
        acqp:

    Returns:

    """
    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    config = util.get_resource('best_b0.cnf')
    logger.debug(F'config: {config}')

    with tempfile.TemporaryDirectory(dir=op.dirname(spinecho)) as tmp:
        # run fast topup on all spinecho volumes (reduced iterations)
        logger.info('run fast TOPUP on all spinecho volumes (reduced iterations)')

        iout = op.join(tmp, 'iout.nii.gz')
        fsl.topup(
            imain=spinecho,
            datain=acqp,
            config=config,
            iout=iout,
            out=op.join(tmp, 'topup_output'),
            logout=op.join(tmp, 'topup.log')
        )
        iout = nb.load(iout).get_data()

        # calculate mean squared error (sum of squared differences)
        logger.info('calculate mean squared error (sum of squared differences)')
        mse = np.mean(np.square(iout - np.mean(iout, axis=3, keepdims=True)), axis=(0, 1, 2)).tolist()

        logger.debug(F'mse: {mse}')

    return mse


def spinecho_zsmoothness(img, brain_thr=20):
    """
    Estimate smoothness in IS direction.

    Args:
        img:
        brain_thr:

    Returns:

    """
    logger = logging.getLogger(__name__)
    logger.info('start')

    ax = get_axis(img, 'IS')[0]

    assert ax is 'z', ("z_smoothness currently requires IS to be in the z dimension")

    # load img
    d = nb.load(img)
    d0 = np.copy(d.get_data()).astype('float')

    # threshold at brain_thr to get rid of non-brain
    # TODO: replace with EM gaussian mixture model
    d0[d0 < brain_thr] = np.nan

    # calculate mean intensity xy plane for each volume
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        smth = np.nanmean(np.nanmean(d0, axis=0), axis=0)

    # estimate smoothness across z dimension
    smth = np.nanstd(np.diff(smth, axis=0), axis=0).tolist()
    logger.debug(F'zsmth: {smth}')

    return smth


def pick_best_b0(metric, acqp, n):
    """
    Sort and pick the best B0s.

    Args:
        metric:
        acqp:
        n:

    Returns:

    """

    logger = logging.getLogger()
    logger.info('start')
    logger.debug(pprint.pformat(locals(), indent=2))

    acqp0 = np.loadtxt(acqp).tolist()
    grps = np.unique(acqp0, axis=0)

    metric = np.array(metric)

    idx = []
    for a in grps.tolist():

        idx0 = np.where([a == b for b in acqp0])[0]

        logger.debug(F'idx0: {idx0}')
        logger.debug(F'metric: {metric[idx0]}')

        srt_idx = np.argsort(metric[idx0])

        logger.debug(F'srt_idx: {idx0[srt_idx]}')
        logger.debug(F'metric_sort: {metric[idx0[srt_idx]]}')

        idx0 = idx0[srt_idx]
        idx0 = idx0[0:n]

        for i in idx0:
            idx.append(i)
        logger.debug(F'idx: {idx}')

    logger.info('end')

    return np.array(idx).tolist()
