#!/usr/bin/env python
import logging
import os
from fsl.utils.filetree import FileTree

from dhcp.func import mcdc, ica, importdata, fieldmap as fmap, registration as reg, denoise as fix, surface, qc
from dhcp.util import util, fslpy as fsl
from dhcp.util.enums import PhaseEncodeDirection, SegType
from dhcp.util.io import Path
from tempfile import TemporaryDirectory

import numpy as np

############
# PARAMETERS
############

# Set the parameters for the operation of the pipeline.
# These parameters will need to ba adjusted according to your specific requirements, especially the paths.

INPUT_PATH = './'
LOCAL_BIN = './bin'

# environment variables

os.environ['FSL_FIX_MATLAB_MODE'] = '1'
os.environ['MATLAB_BIN'] = '/usr/bin/matlab'
os.environ['DHCP_FIX_PATH'] = f'{LOCAL_BIN}/fix'

os.environ['DHCP_ANTS_PATH'] = f'{LOCAL_BIN}/ANTs/bin/bin'
os.environ['DHCP_C3D_PATH'] = f'{LOCAL_BIN}/c3d/bin'

os.environ['DHCP_EDDY_PATH'] = f'{LOCAL_BIN}/bin'

# subject info
subid = 'CC00108XX09'
sesid = '36800'
scan_ga = 42.57
birth_ga = 40.0

# input data directories
sourcedata = f'{INPUT_PATH}/sourcedata'
derivatives = f'{INPUT_PATH}/derivatives'

# output workdir
workdir = Path(f'{INPUT_PATH}/derivatives/dhcp-fmri-pipeline')

# imaging parameters
AP = PhaseEncodeDirection.AP
PA = PhaseEncodeDirection.PA

spinecho_echospacing = 0.0007188
spinecho_pedir = [AP, PA, AP, PA, AP, PA, AP, PA]
spinecho_inplaneaccel = 1

sbref_echospacing = 0.0007216
sbref_pedir = PA

func_echospacing = 0.0007216
func_pedir = PA

# pipeline parameters

do_bbr = True       # use BBR for fmap and sbref registration to structural
do_dc = True        # do distortion correction of func and sbref
do_s2v = True       # do slice-to-volume motion correction
do_mbs = True       # do motion-by-susceptibility distortion correction

temporal_fwhm = 150.0   # high-pass filter cut-off (secs)
ica_dim = 150           # cap on single-subject ICA dimensionality

############
# LOGGING & DEFAULT OUTPUT FILENAMES
############

# setup logging
logging.basicConfig(
    format='[%(asctime)s - %(name)s.%(funcName)s ] %(levelname)s : %(message)s',
    level=logging.INFO
)

# setup defaults
# read the dHCP defaults filetree.  The filetree defines the default output filenames.
defaults = FileTree.read(util.get_resource('dhcp-defaults.tree'), partial_fill=False).update(
    subid=subid,
    sesid=sesid,
    workdir=workdir,
)

############
# IMPORT
############

# Import files in the pipeline directory structure

subdir = f'sub-{subid}/ses-{sesid}'

importdata.DEFAULTS = defaults

importdata.import_info(
    subid=subid,
    sesid=sesid,
    scan_pma=scan_ga,
    birth_ga=birth_ga,
)

importdata.import_func(
    func=f'{sourcedata}/{subdir}/func/sub-{subid}_ses-{sesid}_task-rest_bold.nii.gz',
    func_slorder=util.get_resource('default_func.slorder'),
    func_pedir=func_pedir,
    func_echospacing=func_echospacing,
    sbref=f'{sourcedata}/{subdir}/func/sub-{subid}_ses-{sesid}_task-rest_sbref.nii.gz',
    sbref_pedir=sbref_pedir,
    sbref_echospacing=sbref_echospacing,
)

importdata.import_struct(
    T2w=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-restore_T2w.nii.gz',
    T1w=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-restore_space-T2w_T1w.nii.gz',
    brainmask=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-bet_space-T2w_brainmask.nii.gz',
    dseg=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-drawem9_space-T2w_dseg.nii.gz',
)

importdata.import_spinecho(
    spinecho=f'{sourcedata}/{subdir}/fmap/sub-{subid}_ses-{sesid}_dir-APPA_epi.nii.gz',
    spinecho_pedir=spinecho_pedir,
    spinecho_echospacing=spinecho_echospacing,
    spinecho_inplaneaccel=spinecho_inplaneaccel,
)

surf_path = Path(f'{derivatives}/dhcp_anat_pipeline/sub-{subid}/ses-{sesid}/anat')

importdata.import_surfaces(
    white_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_wm.surf.gii'),
    white_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_wm.surf.gii'),
    pial_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_pial.surf.gii'),
    pial_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_pial.surf.gii'),
    midthickness_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_midthickness.surf.gii'),
    midthickness_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_midthickness.surf.gii'),
    inflated_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_inflated.surf.gii'),
    inflated_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_inflated.surf.gii'),
    veryinflated_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_veryinflated.surf.gii'),
    veryinflated_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_veryinflated.surf.gii'),
    sphere_surf_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_space-T2w_sphere.surf.gii'),
    sphere_surf_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_space-T2w_sphere.surf.gii'),
    medialwall_shape_left=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-R_desc-medialwall_mask.shape.gii'),
    medialwall_shape_right=surf_path.join(f'sub-{subid}_ses-{sesid}_hemi-L_desc-medialwall_mask.shape.gii'),
)

############
# FIELDMAP
############

# Generate fieldmap from spinecho

fmap.DEFAULTS = defaults

fmap.fieldmap(
    spinecho=defaults.get('spinecho'),
    echospacing=spinecho_echospacing,
    pedir=spinecho_pedir,
)


############
# REG A
############

# fmap -> struct

reg.DEFAULTS = defaults.update(src_space='fmap', ref_space='struct')

reg.fmap_to_struct(
    fmap=defaults.get('fmap'),
    fmap_magnitude=defaults.get('fmap_magnitude'),
    fmap_brainmask=defaults.get('fmap_brainmask'),
    struct=defaults.get('T2w'),
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_boundarymask=defaults.get('T2w_wmmask'),
    do_bbr=do_bbr
)

# func -> sbref (distorted)

reg.DEFAULTS = defaults.update(src_space='func', ref_space='sbref')

reg.func_to_sbref(
    func=defaults.get('func0'),
    func_brainmask=defaults.get('func_brainmask'),
    sbref=defaults.get('sbref'),
    sbref_brainmask=defaults.get('sbref_brainmask')
)

# sbref -> struct (with BBR and DC)

reg.DEFAULTS = defaults.update(src_space='sbref', ref_space='struct')

reg.sbref_to_struct(
    sbref=defaults.get('sbref'),
    sbref_brainmask=defaults.get('sbref_brainmask'),
    sbref_pedir=sbref_pedir,
    sbref_echospacing=sbref_echospacing,
    struct=defaults.get('T2w'),
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_boundarymask=defaults.get('T2w_wmmask'),
    fmap=defaults.get('fmap'),
    fmap_brainmask=defaults.get('fmap_brainmask'),
    fmap2struct_xfm=defaults.update(src_space='fmap', ref_space='struct').get('affine'),
    do_bbr=do_bbr,
    do_dc=do_dc,
)

# func (distorted) -> sbref -> struct (composite)

reg.DEFAULTS = defaults.update(src_space='func', ref_space='struct')

reg.func_to_struct_composite(
    func=defaults.get('func0'),
    struct=defaults.get('T2w'),
    func2sbref_affine=defaults.update(src_space='func', ref_space='sbref').get('affine'),
    sbref2struct_affine=defaults.update(src_space='sbref', ref_space='struct').get('affine'),
    sbref2struct_warp=defaults.update(src_space='sbref', ref_space='struct').get('warp'),
)

# fmap -> func (composite)

reg.DEFAULTS = defaults.update(src_space='fmap', ref_space='func')

reg.fmap_to_func_composite(
    fmap=defaults.get('fmap'),
    func=defaults.get('func0'),
    fmap2struct_affine=defaults.update(src_space='fmap', ref_space='struct').get('affine'),
    func2struct_invaffine=defaults.update(src_space='func', ref_space='struct').get('inv_affine'),
)

############
# motion and distortion correction
############


defaults = defaults.update(dc='dc')

mcdc.DEFAULTS = defaults

mcdc.mcdc(
    func=defaults.get('func'),
    func_brainmask=defaults.get('func_brainmask'),
    fmap=defaults.update(src_space='fmap', ref_space='func').get('resampled_image'),
    func_echospacing=func_echospacing,
    func_pedir=func_pedir,
    func_slorder=util.get_resource('default_func.slorder'),
    do_dc=do_dc,
    do_s2v=do_s2v,
    do_mbs=do_mbs,
)


############
# REG B
############

# func -> sbref (undistorted)

reg.DEFAULTS = defaults.update(src_space='func-mcdc', ref_space='sbref-dc')

reg.func_to_sbref(
    func=defaults.get('func_mcdc_mean'),
    func_brainmask=defaults.get('func_mcdc_brainmask'),
    sbref=defaults.update(src_space='sbref', ref_space='struct').get('dc_image'),
    sbref_brainmask=defaults.update(src_space='sbref', ref_space='struct').get('dc_brainmask')
)

# func (undistorted) -> sbref -> struct (composite)

reg.DEFAULTS = defaults.update(src_space='func-mcdc', ref_space='struct')

reg.func_to_struct_composite(
    func=defaults.get('func_mcdc_mean'),
    struct=defaults.get('T2w'),
    func2sbref_affine=defaults.update(src_space='func-mcdc', ref_space='sbref-dc').get('affine'),
    sbref2struct_affine=defaults.update(src_space='sbref', ref_space='struct').get('affine'),
    sbref2struct_warp=defaults.update(src_space='sbref', ref_space='struct').get('warp'),
)


############
# ICA
############

ica.DEFAULTS = defaults

ica.ica(
    func=defaults.get('func_mcdc'),
    func_brainmask=defaults.get('func_mcdc_brainmask'),
    temporal_fwhm=temporal_fwhm,
    icadim=ica_dim,
)

util.update_sidecar(
    defaults.get('func_filt'),
    temporal_fwhm=temporal_fwhm,
)

############
# FIX
############

fix.DEFAULTS = defaults

fix.fix_extract(
    func_filt=defaults.get('func_filt'),
    func_ref=defaults.get('func_mcdc_mean'),
    struct=defaults.get('T2w'),
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_dseg=defaults.get('T2w_dseg'),
    dseg_type=SegType.drawem,
    func2struct_mat=defaults.update(src_space='func-mcdc', ref_space='struct').get('affine'),
    mot_param=defaults.get('motparams'),
    icadir=defaults.get('icadir'),
    temporal_fwhm=temporal_fwhm
)

fix.fix_classify(
    rdata=util.get_setting('dhcp_trained_fix', None),
    threshold=util.get_setting('dhcp_trained_fix_threshold', None),
)

fix.fix_apply(
    temporal_fwhm=temporal_fwhm,
)


############
# STANDARD
############

scan_ga = util.json2dict(defaults.get('subject_info'))['scan_ga']
age = int(np.round(scan_ga))

atlas_tree = Path(util.get_setting('dhcp_volumetric_atlas_tree'))
atlas = FileTree.read(atlas_tree).update(path=atlas_tree.dirname)

# extract and format PVE (toblerone)

with TemporaryDirectory(dir=defaults.get('inputdir')) as tmp:

    srf_defaults = defaults.update(mesh='native', space='T2w')
    surface.toblerone_pve(
        white_surf_right=srf_defaults.update(hemi='right').get('surf_white'),
        white_surf_left=srf_defaults.update(hemi='left').get('surf_white'),
        pial_surf_right=srf_defaults.update(hemi='right').get('surf_pial'),
        pial_surf_left=srf_defaults.update(hemi='left').get('surf_pial'),
        ref=defaults.get('T2w'),
        struct=defaults.get('T2w'),
        struct_to_ref_xfm='I',
        workdir=tmp,
    )

    fsl.fslroi(
        input=f'{tmp}/pve.nii.gz',
        output=defaults.get('T2w_probseg'),
        tmin=0,
        tsize=1,
    )

# template (scan-age) -> struct

reg.DEFAULTS = defaults.update(src_space=f'template-{age}', ref_space='struct')

reg.template_to_struct(
    age=age,
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_T2w=defaults.get('T2w'),
    struct_gmprob=defaults.get('T2w_probseg'),
    atlas=atlas,
)

# struct -> age-matched template -> standard template (composite)

standard_age = 40
reg.DEFAULTS = defaults.update(src_space='struct', ref_space='standard')

reg.struct_to_template_composite(
    struct=defaults.get('T2w'),
    struct2template_warp=defaults.update(src_space=f'template-{age}', ref_space='struct').get('inv_warp'),
    age=age,
    standard_age=standard_age,
    atlas=atlas,
)

# func (undistorted) -> struct -> age-matched template -> standard template (composite)

reg.DEFAULTS = defaults.update(src_space='func-mcdc', ref_space='standard')

reg.func_to_template_composite(
    func=defaults.get('func_mcdc_mean'),
    func2struct_affine=defaults.update(src_space='func-mcdc', ref_space='struct').get('affine'),
    struct2template_warp=defaults.update(src_space='struct', ref_space='standard').get('warp'),
    standard_age=standard_age,
    atlas=atlas,
)
