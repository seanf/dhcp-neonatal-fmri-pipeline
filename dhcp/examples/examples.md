# Examples

This directory contains example scripts for running the pipeline:


| Script | Description |
| --- | --- |
| `dhcp-default.py` | Demonstrates the default dHCP pipeline steps using the lower-level python API |
| `dhcp-default-mocked.py` | As for `dhcp-default.py` but with some steps mocked |
| `dhcp-no-fieldmap.py` | Demonstrates the  dHCP pipeline without a fieldmap using the lower-level python API |
| `dhcp-no-fieldmap-mocked.py` | As for `dhcp-default-fieldmap.py` but with some steps mocked |
