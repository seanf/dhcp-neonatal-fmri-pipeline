#!/usr/bin/env python
import logging
import os
from fsl.utils.filetree import FileTree

from dhcp.func import mcdc, ica, importdata, fieldmap as fmap, registration as reg, denoise as fix, qc
from dhcp.util import util
from dhcp.util.enums import PhaseEncodeDirection, SegType
from dhcp.util.io import Path

import numpy as np

############
# PARAMETERS
############

# Set the parameters for the operation of the pipeline.
# These parameters will need to ba adjusted according to your specific requirements, especially the paths.

INPUT_PATH = './'
LOCAL_BIN = './bin'

# environment variables

os.environ['FSL_FIX_MATLAB_MODE'] = '1'
os.environ['MATLAB_BIN'] = '/usr/bin/matlab'
os.environ['DHCP_FIX_PATH'] = f'{LOCAL_BIN}/fix'

os.environ['DHCP_ANTS_PATH'] = f'{LOCAL_BIN}/ANTs/bin/bin'
os.environ['DHCP_C3D_PATH'] = f'{LOCAL_BIN}/c3d/bin'

os.environ['DHCP_EDDY_PATH'] = f'{LOCAL_BIN}/bin'

# subject info
subid = 'CC00108XX09'
sesid = '36800'
scan_ga = 42.57
birth_ga = 40.0

# input data directories
sourcedata = f'{INPUT_PATH}/sourcedata'
derivatives = f'{INPUT_PATH}/derivatives'

# output workdir
workdir = Path(f'{INPUT_PATH}/derivatives/dhcp-fmri-pipeline')

# imaging parameters
AP = PhaseEncodeDirection.AP
PA = PhaseEncodeDirection.PA

sbref_echospacing = 0.0007216
sbref_pedir = PA

func_echospacing = 0.0007216
func_pedir = PA

# pipeline parameters

do_bbr = True       # use BBR for fmap and sbref registration to structural
do_dc = False        # do NOT do distortion correction of func and sbref (requires fieldmap)
do_s2v = True       # do slice-to-volume motion correction
do_mbs = False       # do NOT do motion-by-susceptibility distortion correction (requires fieldmap)

temporal_fwhm = 150.0   # high-pass filter cut-off (secs)
ica_dim = 600           # cap on single-subject ICA dimensionality



############
# LOGGING & DEFAULT OUTPUT FILENAMES
############

# setup logging
logging.basicConfig(
    format='[%(asctime)s - %(name)s.%(funcName)s ] %(levelname)s : %(message)s',
    level=logging.INFO
)

# setup defaults
defaults = FileTree.read(util.get_resource('dhcp-defaults.tree'), partial_fill=False).update(
    subid=subid,
    sesid=sesid,
    workdir=workdir,
)

############
# IMPORT
############

subdir = f'sub-{subid}/ses-{sesid}'

importdata.DEFAULTS = defaults

importdata.import_info(
    subid=subid,
    sesid=sesid,
    scan_pma=scan_ga,
    birth_ga=birth_ga,
)

importdata.import_func(
    func=f'{sourcedata}/{subdir}/func/sub-{subid}_ses-{sesid}_task-rest_bold.nii.gz',
    func_slorder=util.get_resource('default_func.slorder'),
    func_pedir=func_pedir,
    func_echospacing=func_echospacing,
    sbref=f'{sourcedata}/{subdir}/func/sub-{subid}_ses-{sesid}_task-rest_sbref.nii.gz',
    sbref_pedir=sbref_pedir,
    sbref_echospacing=sbref_echospacing,
)

importdata.import_struct(
    T2w=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-restore_T2w.nii.gz',
    T1w=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-restore_space-T2w_T1w.nii.gz',
    brainmask=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-bet_space-T2w_brainmask.nii.gz',
    dseg=f'{derivatives}/dhcp_anat_pipeline/{subdir}/anat/sub-{subid}_ses-{sesid}_desc-drawem9_space-T2w_dseg.nii.gz',
)


############
# REG A
############

# func -> sbref (distorted)

reg.DEFAULTS = defaults.update(src_space='func', ref_space='sbref')

reg.func_to_sbref(
    func=defaults.get('func0'),
    func_brainmask=defaults.get('func_brainmask'),
    sbref=defaults.get('sbref'),
    sbref_brainmask=defaults.get('sbref_brainmask')
)

# sbref -> struct (with BBR)

reg.DEFAULTS = defaults.update(src_space='sbref', ref_space='struct')

reg.sbref_to_struct(
    sbref=defaults.get('sbref'),
    sbref_brainmask=defaults.get('sbref_brainmask'),
    sbref_pedir=sbref_pedir,
    sbref_echospacing=sbref_echospacing,
    struct=defaults.get('T2w'),
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_boundarymask=defaults.get('T2w_wmmask'),
    do_bbr=do_bbr,
    do_dc=do_dc,
)

# func -> sbref -> struct (composite)

reg.DEFAULTS = defaults.update(src_space='func', ref_space='struct')

reg.func_to_struct_composite(
    func=defaults.get('func0'),
    struct=defaults.get('T2w'),
    func2sbref_affine=defaults.update(src_space='func', ref_space='sbref').get('affine'),
    sbref2struct_affine=defaults.update(src_space='sbref', ref_space='struct').get('affine'),
)

############
# motion correction
############

from unittest import mock
from dhcp.tests.mocks import mock_eddy_mcdc

with mock.patch('dhcp.func.mcdc.eddy_mcdc', side_effect=mock_eddy_mcdc):

    mcdc.DEFAULTS = defaults

    mcdc.mcdc(
        func=defaults.get('func'),
        func_brainmask=defaults.get('func_brainmask'),
        func_echospacing=func_echospacing,
        func_pedir=func_pedir,
        func_slorder=util.get_resource('default_func.slorder'),
        do_dc=do_dc,
        do_s2v=do_s2v,
        do_mbs=do_mbs,
    )


############
# REG B
############

# func (mc) -> sbref

reg.DEFAULTS = defaults.update(src_space='func-mc', ref_space='sbref')

reg.func_to_sbref(
    func=defaults.get('func_mcdc_mean'),
    func_brainmask=defaults.get('func_mcdc_brainmask'),
    sbref=defaults.get('sbref'),
    sbref_brainmask=defaults.get('sbref_brainmask'),
)

# func (mc) -> sbref -> struct [composite]

reg.DEFAULTS = defaults.update(src_space='func-mc', ref_space='struct')

reg.func_to_struct_composite(
    func=defaults.get('func_mcdc_mean'),
    struct=defaults.get('T2w'),
    func2sbref_affine=defaults.update(src_space='func-mc', ref_space='sbref').get('affine'),
    sbref2struct_affine=defaults.update(src_space='sbref', ref_space='struct').get('affine'),
)


############
# ICA
############

ica.DEFAULTS = defaults

ica.ica(
    func=defaults.get('func_mcdc'),
    func_brainmask=defaults.get('func_mcdc_brainmask'),
    temporal_fwhm=temporal_fwhm,
    icadim=ica_dim,
)

util.update_sidecar(
    defaults.get('func_filt'),
    temporal_fwhm=temporal_fwhm,
)

############
# FIX
############

fix.DEFAULTS = defaults

fix.fix_extract(
    func_filt=defaults.get('func_filt'),
    func_ref=defaults.get('func_mcdc_mean'),
    struct=defaults.get('T2w'),
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_dseg=defaults.get('T2w_dseg'),
    dseg_type=SegType.drawem,
    func2struct_mat=defaults.update(src_space='func-mc', ref_space='struct').get('affine'),
    mot_param=defaults.get('motparams'),
    icadir=defaults.get('icadir'),
    temporal_fwhm=temporal_fwhm
)

from unittest import mock
from dhcp.tests.mocks import mock_classify

with mock.patch('dhcp.func.denoise._classify', side_effect=mock_classify):
    fix.fix_classify(
        rdata=util.get_setting('dhcp_trained_fix', None),
        threshold=util.get_setting('dhcp_trained_fix_threshold', None),
    )

fix.fix_apply(
    temporal_fwhm=temporal_fwhm,
)


############
# STANDARD
############

scan_ga = util.json2dict(defaults.get('subject_info'))['scan_ga']
age = int(np.round(scan_ga))

atlas_tree = Path(util.get_setting('dhcp_volumetric_atlas_tree'))
atlas = FileTree.read(atlas_tree).update(path=atlas_tree.dirname)

# template (scan-age) -> struct

reg.DEFAULTS = defaults.update(src_space=f'template-{age}', ref_space='struct')

reg.template_to_struct(
    age=age,
    struct_brainmask=defaults.get('T2w_brainmask'),
    struct_T1w=defaults.get('T1w') if defaults.on_disk('T1w') else None,
    struct_T2w=defaults.get('T2w'),
    atlas=atlas,
)

# struct -> age-matched template -> standard template (composite)

standard_age = 40
reg.DEFAULTS = defaults.update(src_space='struct', ref_space='standard')

reg.struct_to_template_composite(
    struct=defaults.get('T2w'),
    struct2template_warp=defaults.update(src_space=f'template-{age}', ref_space='struct').get('inv_warp'),
    age=age,
    standard_age=standard_age,
    atlas=atlas,
)

# func (undistorted) -> struct -> age-matched template -> standard template (composite)

reg.DEFAULTS = defaults.update(src_space='func-mcdc', ref_space='standard')

reg.func_to_template_composite(
    func=defaults.get('func_mcdc_mean'),
    func2struct_affine=defaults.update(src_space='func-mcdc', ref_space='struct').get('affine'),
    struct2template_warp=defaults.update(src_space='struct', ref_space='standard').get('warp'),
    standard_age=standard_age,
    atlas=atlas,
)
