#!/usr/bin/env python
import os
import shutil
import logging
from dhcp.util import assertions as assrt
from dhcp.util import util
# from dhcp.util.io import ImageDict
import fsl.utils.filetree as filetree
import pprint
from dhcp.util.io import Path
from typing import Optional

class WorkDir(filetree.FileTree):

    # @property
    # def path(self):
    #     return Path(self.variables['path'])
    #
    # @property
    # def subid(self):
    #     return self.variables['subid'] if 'subid' in self.variables else None
    #
    # @property
    # def sesid(self):
    #     return self.variables['sesid'] if 'sesid' in self.variables else None

    def get(self, *args, **kwargs) -> Path:
        return Path(super(WorkDir, self).get(*args, **kwargs))

    # @classmethod
    # def from_id(cls,
    #             path: Path = Path.getcwd(),
    #             subid: Optional[str] = None,
    #             sesid: Optional[str] = None,
    #             ft: Optional[filetree.FileTree] = None):
    #     # get default pipeline filetree
    #     if ft is None:
    #         filetree.tree_directories += [util.get_resource_path()]
    #         ft = filetree.FileTree.read('dhcp-defaults')
    #
    #     # update filetree
    #     args = dict(path=path, subid=subid, sesid=sesid)
    #     ft = ft.update(**{k: v for k, v in args.items() if v is not None})
    #
    #     return cls(
    #         templates=ft.templates,
    #         variables=ft.variables,
    #         sub_trees=ft.sub_trees,
    #         parent=ft.parent
    #     )

    @classmethod
    def from_filename(cls, fname: Path, **kwargs):
        """
        Load workdir from treefile (*.tree)
        :param fname:
        :return:
        """

        filetree.tree_directories += [fname.dirname]
        ft = filetree.FileTree.read(fname.filename)
        ft = ft.update(**kwargs)

        return cls(
                templates=ft.templates,
                variables=ft.variables,
                sub_trees=ft.sub_trees,
                parent=ft.parent
            )

    @classmethod
    def from_dict(cls, d, **kwargs):
        """
        Create WorkDir from dictionary of templates
        :param d:
        :return:
        """

        return cls(
            templates=d,
            variables=kwargs,
        )