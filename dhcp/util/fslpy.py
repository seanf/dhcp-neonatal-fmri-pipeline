#!/usr/bin/env python
"""Python wrappers for FSL tools."""
# import dhcp.util.util as util
# import dhcp.util.util as util
import dhcp.util.assertions as asrt
import dhcp.util.image as img
import dhcp.util.shellops as shops
import os
import os.path as op
import glob


def fnirt(src, ref, aff=None, imprefm=None, impinm=None, applyrefmask=None,
          applyinmask=None, subsamp=None, miter=None, infwhm=None,
          reffwhm=None, lmbda=None, estint=None, warpres=None, ssqlambda=None,
          regmod=None, intmod=None, intorder=None, biasres=None,
          biaslambda=None, refderiv=None, cout=None, intout=None, refout=None,
          iout=None, interp=None, inwarp=None, minmet=None, verbose=False,
          intin=None, jout=None):
    """Do nonlinear image registration."""
    cmd = 'fnirt --in={0} --ref={1}'.format(src, ref)

    if aff is not None:
        cmd += " --aff={0}".format(aff)
    if imprefm is not None:
        cmd += " --imprefm={0}".format(imprefm)
    if impinm is not None:
        cmd += " --impinm={0}".format(impinm)
    if applyrefmask is not None:
        cmd += " --applyrefmask={0}".format(applyrefmask)
    if applyinmask is not None:
        cmd += " --applyinmask={0}".format(applyinmask)
    if subsamp is not None:
        cmd += " --subsamp={0}".format(subsamp)
    if miter is not None:
        cmd += " --miter={0}".format(miter)
    if infwhm is not None:
        cmd += " --infwhm={0}".format(infwhm)
    if reffwhm is not None:
        cmd += " --reffwhm={0}".format(reffwhm)
    if lmbda is not None:
        cmd += " --lambda={0}".format(lmbda)
    if estint is not None:
        cmd += " --estint={0}".format(estint)
    if warpres is not None:
        cmd += " --warpres={0}".format(warpres)
    if ssqlambda is not None:
        cmd += " --ssqlambda={0}".format(ssqlambda)
    if regmod is not None:
        cmd += " --regmod={0}".format(regmod)
    if intmod is not None:
        cmd += " --intmod={0}".format(intmod)
    if intorder is not None:
        cmd += " --intorder={0}".format(intorder)
    if biasres is not None:
        cmd += " --biasres={0}".format(biasres)
    if biaslambda is not None:
        cmd += " --biaslambda={0}".format(biaslambda)
    if refderiv is not None:
        cmd += " --refderiv={0}".format(refderiv)
    if cout is not None:
        cmd += " --cout={0}".format(cout)
    if intout is not None:
        cmd += " --intout={0}".format(intout)
    if refout is not None:
        cmd += " --refout={0}".format(refout)
    if iout is not None:
        cmd += " --iout={0}".format(iout)
    if interp is not None:
        cmd += " --interp={0}".format(interp)
    if inwarp is not None:
        cmd += " --inwarp={0}".format(inwarp)
    if minmet is not None:
        cmd += " --minmet={0}".format(minmet)
    if intin is not None:
        cmd += " --intin={0}".format(intin)
    if jout is not None:
        cmd += " --jout={0}".format(jout)
    if verbose:
        cmd += " --verbose"

    return shops.run(cmd)


def eddy(imain, mask, index, acqp, bvecs, bvals, out, very_verbose=False,
         niter=None, fwhm=None, s2v_niter=None, mporder=None, nvoxhp=None,
         slspec=None, b0_only=False, topup=None, field=None,
         field_mat=None, debug=None, s2v_fwhm=None, interp=None,
         dont_mask_output=False, s2v_interp=None, ref_scan_no=None,
         data_is_shelled=False, estimate_move_by_susceptibility=False,
         mbs_niter=None, mbs_lambda=None, mbs_ksp=None, s2v_lambda=None,
         cnr_maps=False, residuals=False):
    """Correct eddy current-induced distortions and subject movements."""
    asrt.assert_file_exists(imain, mask, index, acqp, bvecs, bvals)
    asrt.assert_is_nifti(imain, mask)

    assert not (topup and field), "topup and field arguments are incompatible"

    out = img.splitext(out)[0]

    opts = " --imain={0} --mask={1} --index={2} --bvals={3} " \
           "--bvecs={4} --acqp={5} --out={6}".format(imain, mask, index, bvals,
                                                     bvecs, acqp, out)

    path = os.getenv('DHCP_EDDY_PATH', None)
    if path is None:
        path = os.getenv('FSLDIR', None)

    assert path is not None, 'Either DHCP_EDDY_PATH or FSLDIR environment variables must be set.'

    cmd = op.join(path, 'eddy')
    # cmd = 'eddy_cuda'
    cmd = cmd + opts

    if very_verbose:
        cmd += " --very_verbose"
    if estimate_move_by_susceptibility:
        cmd += " --estimate_move_by_susceptibility"
    if data_is_shelled:
        cmd += " --data_is_shelled"
    if mbs_niter is not None:
        cmd += " --mbs_niter={0}".format(mbs_niter)
    if mbs_lambda is not None:
        cmd += " --mbs_lambda={0}".format(mbs_lambda)
    if mbs_ksp is not None:
        cmd += " --mbs_ksp={0}".format(mbs_ksp)
    if niter is not None:
        cmd += " --niter={0}".format(niter)
    if fwhm is not None:
        cmd += " --fwhm={0}".format(fwhm)
    if s2v_fwhm is not None:
        cmd += " --s2v_fwhm={0}".format(s2v_fwhm)
    if s2v_niter is not None:
        cmd += " --s2v_niter={0}".format(s2v_niter)
    if s2v_interp is not None:
        cmd += " --s2v_interp={0}".format(s2v_interp)
    if interp is not None:
        cmd += " --interp={0}".format(interp)
    if mporder is not None:
        cmd += " --mporder={0}".format(mporder)
    if nvoxhp is not None:
        cmd += " --nvoxhp={0}".format(nvoxhp)
    if slspec is not None:
        cmd += " --slspec={0}".format(slspec)
    if topup is not None:
        cmd += " --topup={0}".format(topup)
    if field is not None:
        field = img.splitext(field)[0]
        cmd += " --field={0}".format(field)
    if b0_only:
        cmd += " --b0_only"
    if field_mat is not None:
        cmd += " --field_mat={0}".format(field_mat)
    if debug is not None:
        cmd += " --debug={0}".format(debug)
    if dont_mask_output:
        cmd += " --dont_mask_output"
    if ref_scan_no is not None:
        cmd += " --ref_scan_no={0}".format(ref_scan_no)
    if s2v_lambda is not None:
        cmd += " --s2v_lambda={0}".format(s2v_lambda)
    if cnr_maps:
        cmd += " --cnr_maps"
    if residuals:
        cmd += " --residuals"

    shops.run(cmd)


def bet(input, output, mask=False, robust=False, fracintensity=None,
        seg=True):
    """Delete non-brain tissue from an image of the whole head."""
    asrt.assert_is_nifti(input)
    asrt.assert_file_exists(input)

    cmd = "bet {0} {1}".format(input, output)

    if mask:
        cmd += " -m"
    if robust:
        cmd += " -R"
    if fracintensity is not None:
        cmd += " -f {0}".format(fracintensity)
    if not seg:
        cmd += " -n"

    shops.run(cmd)


def topup(imain, datain, config=None, fout=None, iout=None, out=None,
          verbose=False, subsamp=None, logout=None):
    """Estimate and correct susceptibility induced distortions."""
    asrt.assert_file_exists(imain, datain)
    asrt.assert_is_nifti(imain)

    cmd = "topup --imain={0} --datain={1}".format(imain, datain)

    if config is not None:
        cmd += " --config={0}".format(config)
    if fout is not None:
        cmd += " --fout={0}".format(fout)
    if iout is not None:
        cmd += " --iout={0}".format(iout)
    if out is not None:
        cmd += " --out={0}".format(out)
    if subsamp is not None:
        cmd += " --subsamp={0}".format(subsamp)
    if logout is not None:
        cmd += " --logout={0}".format(logout)
    if verbose:
        cmd += " -v"

    shops.run(cmd)


def fslreorient2std(input, output):
    """reorient to match the approx. orientation of the standard (MNI152)."""

    asrt.assert_is_nifti(input, output)
    asrt.assert_file_exists(input)

    cmd = 'fslreorient2std {0} {1}'.format(input, output)
    shops.run(cmd)


def fslroi(input, output, xmin=None, xsize=None, ymin=None, ysize=None,
           zmin=None, zsize=None, tmin=None, tsize=None):
    """Extract region of interest (ROI) from an image."""
    assert ((tmin is not None and tsize is not None) or
            (xmin is not None and xsize is not None and
            ymin is not None and ysize is not None and
            zmin is not None and zsize is not None)), \
        "either time min/size or x/y/z min/size must be provided"

    cmd = "fslroi {0} {1}".format(input, output)

    if xmin is not None:
        cmd += " {0} {1} {2} {3} {4} {5}".format(xmin, xsize, ymin, ysize,
                                                 zmin, zsize)
    if tmin is not None:
        cmd += " {0} {1}".format(tmin, tsize)

    shops.run(cmd)


def sigloss(input, output, te=None, slicedir=None, mask=None):
    """Estimate signal loss from a field map (in rad/s)."""
    cmd = "sigloss -i {0} -s {1}".format(input, output)

    if te is not None:
        cmd += " --te={0}".format(te)
    if slicedir is not None:
        cmd += " --slicedir={0}".format(slicedir)
    if mask is not None:
        cmd += " --mask={0}".format(mask)

    shops.run(cmd)


def catmats(matdir, out):
    """Concatenate FSL trasformations files into a single file."""
    mats = glob.glob(op.join(matdir, "MAT_????"))
    with open(out, 'w') as outfile:
        for fname in mats:
            with open(fname) as infile:
                outfile.write(infile.read())


def applywarp(src: object, ref: object, out: object, warp: object = None, premat: object = None, prematdir: object = None,
              postmat: object = None, postmatdir: object = None, interp: object = "spline",
              paddingsize: object = None, abs: object = False, rel: object = False) -> object:
    """Tool for applying FSL warps."""
    assert (warp or premat or postmat or prematdir or postmatdir), \
        "either a warp or mat (premat, postmat or prematdir) must be supplied"
    assert not (premat and prematdir), \
        "cannot use premat and prematdir arguments together"
    assert not (postmat and postmatdir), \
        "cannot use postmat and postmatdir arguments together"

    cmd = "--in={0} --ref={1} --out={2} --interp={3}".format(src, ref, out,
                                                             interp)
    cmd = "applywarp " + cmd

    if prematdir:
        premat = op.join(prematdir, 'allmats.txt')
        catmats(prematdir, premat)
    if postmatdir:
        postmat = op.join(postmatdir, 'allmats.txt')
        catmats(postmatdir, postmat)
    if warp:
        cmd += " --warp={0}".format(warp)
    if premat:
        cmd += " --premat={0}".format(premat)
    if postmat:
        cmd += " --postmat={0}".format(postmat)
    if paddingsize:
        cmd += " --paddingsize={0}".format(paddingsize)
    if abs:
        cmd += " --abs"
    if rel:
        cmd += " --rel"
    shops.run(cmd)


def invxfm(inmat, omat):
    """Tool for inverting FSL transformation matrices."""
    asrt.assert_file_exists(inmat)

    cmd = "convert_xfm -omat {0} -inverse {1}".format(omat, inmat)
    shops.run(cmd)

    return omat


def applyxfm(src, ref, mat, out, interp='spline'):
    """Tool for applying FSL transformation matrices."""
    # asrt.assertFileExists(src, ref)
    # asrt.assertIsNifti(src, ref)

    cmd = "flirt -init {0} -in {1} -ref {2} -applyxfm -out {3} -interp {4}"

    shops.run(cmd.format(mat, src, ref, out, interp))

    return out

def apply_isoxfm(src, res, out, interp='spline'):
    """Tool for resampling images to a isometric resolution."""
    asrt.assert_file_exists(src)
    asrt.assert_is_nifti(src)

    fsldir = os.getenv('FSLDIR', None)
    assert fsldir is not None, 'FSLDIR environment variables must be set.'

    ident = op.join(fsldir, 'etc/flirtsch/ident.mat')

    cmd = f"flirt -init {ident} -in {src} -ref {src} -applyisoxfm {res} -out {out} -interp {interp}"
    return shops.run(cmd)


def concatxfm(inmat1, inmat2, outmat):
    """Tool to concatenate two FSL transformation matrices."""
    asrt.assert_file_exists(inmat1, inmat2)

    cmd = "convert_xfm -omat {0} -concat {1} {2}"
    shops.run(cmd.format(outmat, inmat2, inmat1))


def invwarp(inwarp, ref, outwarp):
    """Tool for inverting FSL warps."""
    asrt.assert_file_exists(inwarp, ref)
    asrt.assert_is_nifti(inwarp, ref, outwarp)

    cmd = __fslcmd("invwarp", warp=inwarp, out=outwarp, ref=ref, style="--")
    shops.run(cmd)


def convertwarp(out, ref, warp1=None, warp2=None, premat=None, midmat=None,
                postmat=None, shiftmap=None, shiftdir=None, absout=False,
                abs=False, rel=False, relout=False):
    """Tool for converting FSL warps."""

    assert (warp1 or warp2 or premat or midmat or postmat), \
        "either a warp (warp1 or warp2) or mat (premat, midmat, or " + \
        "postmat) must be supplied"

    cmd = "convertwarp --ref={0} --out={1}".format(ref, out)
    if warp1:
        cmd = cmd + " --warp1={0}".format(warp1)
    if warp2:
        cmd = cmd + " --warp2={0}".format(warp2)
    if premat:
        cmd = cmd + " --premat={0}".format(premat)
    if midmat:
        cmd = cmd + " --midmat={0}".format(midmat)
    if postmat:
        cmd = cmd + " --postmat={0}".format(postmat)
    if shiftmap:
        cmd = cmd + " --shiftmap={0}".format(shiftmap)
    if shiftdir:
        cmd = cmd + " --shiftdir={0}".format(shiftdir)
    if absout:
        cmd = cmd + " --absout"
    if relout:
        cmd = cmd + " --relout"
    if abs:
        cmd = cmd + " --abs"
    if rel:
        cmd = cmd + " --rel"

    shops.run(cmd)


# def flirt(src, ref, *args, **kwargs):
#     """FLIRT (FMRIB's Linear Image Registration Tool)."""
#     asrt.assertIsNifti(src, ref)
#     asrt.assertFileExists(src, ref)

#     cmd = "flirt -in {0} -ref {1}".format(src, ref)

#     __fslrun(cmd, *args, style="-", **kwargs)


def fugue(unmaskshift=False, despike=False, unmaskfmap=False, **kwargs):
    """FMRIB's Utility for Geometric Unwarping of EPIs."""

    cmd = "fugue"

    if unmaskshift:
        cmd += " --unmaskshift"
    if despike:
        cmd += " --despike"
    if unmaskfmap:
        cmd += " --unmaskfmap"

    __fslrun(cmd, style="--", **kwargs)

    # shops.run("fugue --loadfmap={0} --mask={1} --saveshift={2} --unmaskshift " \
    #     "--dwell={3} --unwarpdir={4}".format(fmap_ph2func,fmap_ph2func_mask,
    #     fmap_ph2func_shift,echospacing,pedir[1]))


def __fslcmd(cmd, style="-", *args, **kwargs):

    for value in args:
        cmd += " {0}".format(value)

    for key in kwargs:
        if style == "-":
            cmd += " -{0} {1}".format(key, kwargs[key])
        elif style == "--":
            cmd += " --{0}={1}".format(key, kwargs[key])

    return cmd


def __fslrun(cmd, *args, **kwargs):
    cmd = __fslcmd(cmd, *args, **kwargs)
    shops.run(cmd)

def flirt(src, ref, out=None, omat=None, dof=None, cost=None, wmseg=None,
          init=None, schedule=None, echospacing=None, pedir=None,
          fieldmap=None, fieldmapmask=None, bbrslope=None, bbrtype=None,
          interp=None, refweight=None, applyisoxfm=None, usesqform=False,
          nosearch=False, verbose=0, searchrx=None, searchry=None, searchrz=None):
    """FLIRT (FMRIB's Linear Image Registration Tool)."""
    asrt.assert_is_nifti(src, ref)
    asrt.assert_file_exists(src, ref)

    cmd = "flirt -in {0} -ref {1}".format(src, ref)

    if out is not None:
        asrt.assert_is_nifti(out)
        cmd += " -out {0}".format(out)
    if omat is not None:
        cmd += " -omat {0}".format(omat)
    if dof is not None:
        cmd += " -dof {0}".format(dof)
    if cost is not None:
        cmd += " -cost {0}".format(cost)
    if wmseg is not None:
        asrt.assert_is_nifti(wmseg)
        cmd += " -wmseg {0}".format(wmseg)
    if init is not None:
        cmd += " -init {0}".format(init)
    if schedule is not None:
        cmd += " -schedule {0}".format(schedule)
    if echospacing is not None:
        cmd += " -echospacing {0}".format(echospacing)
    if pedir is not None:
        cmd += " -pedir {0}".format(pedir)
    if fieldmap is not None:
        cmd += " -fieldmap {0}".format(fieldmap)
    if fieldmapmask is not None:
        cmd += " -fieldmapmask {0}".format(fieldmapmask)
    if bbrslope is not None:
        cmd += " -bbrslope {0}".format(bbrslope)
    if bbrtype is not None:
        cmd += " -bbrtype {0}".format(bbrtype)
    if interp is not None:
        cmd += " -interp {0}".format(interp)
    if refweight is not None:
        asrt.assert_is_nifti(refweight)
        cmd += " -refweight {0}".format(refweight)
    if applyisoxfm is not None:
        cmd += " -applyisoxfm {0}".format(applyisoxfm)
    if verbose is not None:
        cmd += " -verbose {0}".format(verbose)
    if searchrx is not None:
        cmd += " -searchrx {0} {1}".format(searchrx[0], searchrx[1])
    if searchry is not None:
        cmd += " -searchry {0} {1}".format(searchry[0], searchry[1])
    if searchrz is not None:
        cmd += " -searchrz {0} {1}".format(searchrz[0], searchrz[1])
    if usesqform:
        cmd += " -usesqform"
    if nosearch:
        cmd += " -nosearch"

    out = shops.run(cmd)

    return omat


def melodic(input, outdir, dim=None, tr=None, mmthresh=None, report=False,
            prefix=None, nomask=False, updatemask=False, nobet=False,
            mask=None, Oall=False):
    """Multivariate Exploratory Linear Optimised ICA."""
    asrt.assert_is_nifti(input)

    cmd = "melodic -i {0} -v --outdir={1}".format(input, outdir)

    if mmthresh is not None:
        cmd += " --mmthresh={0}".format(mmthresh)
    if dim is not None:
        cmd += " -d -{0}".format(dim)
    if report:
        cmd += " --report"
    if Oall:
        cmd += " --Oall"
    if tr is not None:
        cmd += " --tr={0}".format(tr)
    if nomask:
        cmd += " --nomask"
    if updatemask:
        cmd += " --update_mask"
    if nobet:
        cmd += " --nobet"
    if prefix is not None:
        cmd = prefix + " " + cmd
    if mask is not None:
        cmd += " --mask={0}".format(mask)
    shops.run(cmd)


def fsl_regfilt(infile, outfile, mix, ics):
    """Data de-noising by regression.

    Data de-noising by regressing out part of a design matrix
    using simple OLS regression on 4D images
    """
    asrt.assert_is_nifti(infile, outfile)

    icstr = '"'
    for i in range(0, len(ics)-1):
        icstr = icstr + '{0},'.format(ics[i]+1)
    icstr = icstr + '{0}"'.format(ics[-1]+1)

    cmd = "fsl_regfilt -i {0} -o {1} -d {2} -f {3}".format(infile, outfile,
                                                           mix, icstr)
    shops.run(cmd)


def mcflirt(infile, outfile, reffile=None, spline_final=True, plots=True,
            mats=True, refvol=None):
    """Rigid-body motion correction using mcflirt."""
    outfile = img.splitext(outfile)[0]
    cmd = "mcflirt -in {0} -out {1} -rmsrel -rmsabs".format(infile, outfile)

    if reffile is not None:
        cmd += " -reffile {0}".format(reffile)
    if refvol is not None:
        cmd += " -refvol {0}".format(refvol)
    if spline_final:
        cmd += " -spline_final"
    if plots:
        cmd += " -plots"
    if mats:
        cmd += " -mats"

    shops.run(cmd)


def slicer(input, input2=None, label=None, lut=None, intensity=None, edgethreshold=None,
           x=None, y=None, z=None):

    cmd = "slicer {0}".format(input)

    if input2 is not None:
        cmd += " {0}".format(input2)
    if label is not None:
        cmd += " -L {0}".format(label)
    if lut is not None:
        cmd += " -l {0}".format(lut)
    if intensity is not None:
        cmd += " -i {0} {1}".format(intensity[0], intensity[1])
    if edgethreshold is not None:
        cmd += " -e {0}".format(edgethreshold)
    if x is not None:
        cmd += " -x {0} {1}".format(x[0], x[1])
    if y is not None:
        cmd += " -y {0} {1}".format(y[0], y[1])
    if z is not None:
        cmd += " -z {0} {1}".format(z[0], z[1])

    shops.run(cmd)




def cluster(infile, thresh, oindex=None, no_table=False):
    """
    Form clusters, report information about clusters and/or perform
    cluster-based inference.
    """
    cmd = "cluster --in={0} --thresh={1}".format(infile, thresh)

    if oindex is not None:
        cmd += " -o {0}".format(oindex)

    if no_table:
        cmd += " --no_table"

    shops.run(cmd)


class fslmaths(object):
    """Perform mathematical manipulation of images."""

    def __init__(self, input):
        """Constructor."""
        self.inputImage = input
        self.outputImage = None
        self.operations = []

    def abs(self):
        """Absolute value."""
        self.operations.append("-abs")
        return self

    def bin(self):
        """Use (current image>0) to binarise."""
        self.operations.append("-bin")
        return self

    def binv(self):
        """Binarise and invert (binarisation and logical inversion)."""
        self.operations.append("-binv")
        return self

    def recip(self):
        """Reciprocal (1/current image)."""
        self.operations.append("-recip")
        return self

    def Tmean(self):
        """Mean across time."""
        self.operations.append("-Tmean")
        return self

    def Tstd(self):
        """Standard deviation across time."""
        self.operations.append("-Tstd")
        return self

    def Tmin(self):
        """Min across time."""
        self.operations.append("-Tmin")
        return self

    def Tmax(self):
        """Max across time."""
        self.operations.append("-Tmax")
        return self

    def fillh(self):
        """fill holes in a binary mask (holes are internal - i.e. do not touch
        the edge of the FOV)."""
        self.operations.append("-fillh")
        return self

    def ero(self, repeat=1):
        """Erode by zeroing non-zero voxels when zero voxels in kernel."""
        for i in range(repeat):
            self.operations.append("-ero")
        return self

    def dilM(self, repeat=1):
        """Mean Dilation of non-zero voxels."""
        for i in range(repeat):
            self.operations.append("-dilM")
        return self

    def dilF(self, repeat=1):
        """Maximum filtering of all voxels."""
        for i in range(repeat):
            self.operations.append("-dilF")
        return self

    def add(self, input):
        """Add input to current image."""
        self.operations.append("-add {0}".format(input))
        return self

    def sub(self, input):
        """Subtract input from current image."""
        self.operations.append("-sub {0}".format(input))
        return self

    def mul(self, input):
        """Multiply current image by input."""
        self.operations.append("-mul {0}".format(input))
        return self

    def sqr(self):
        """Square current image."""
        self.operations.append("-sqr")
        return self

    def sqrt(self):
        """Square root of current image."""
        self.operations.append("-sqrt")
        return self

    def div(self, input):
        """Divide current image by input."""
        self.operations.append("-div {0}".format(input))
        return self

    def mas(self, image):
        """Use image (>0) to mask current image."""
        self.operations.append("-mas {0}".format(image))
        return self

    def rem(self, input):
        """Divide current image by following input and take remainder."""
        self.operations.append("-rem {0}".format(input))
        return self

    def thr(self, input):
        """use input number to threshold current image (zero < input)."""
        self.operations.append("-thr {0}".format(input))
        return self

    def uthr(self, input):
        """use input number to upper-threshold current image (zero
        anything above the number)."""
        self.operations.append("-uthr {0}".format(input))
        return self

    def inm(self, input):
        """Intensity normalisation (per 3D volume mean)"""
        self.operations.append("-inm {0}".format(input))
        return self

    def bptf(self, hp_sigma, lp_sigma):
        """Bandpass temporal filtering; nonlinear highpass and Gaussian linear
        lowpass (with sigmas in volumes, not seconds); set either sigma<0 to
        skip that filter."""
        self.operations.append("-bptf {0} {1}".format(hp_sigma, lp_sigma))
        return self

    def toString(self):
        """Generate fslmaths command string."""
        cmd = "fslmaths {0} ".format(self.inputImage)
        for oper in self.operations:
            cmd = cmd + oper + " "
        cmd = cmd + str(self.outputImage)
        return cmd

    def run(self, output=None):
        """Save output of operations to image."""
        if output:
            self.outputImage = output
        else:
            self.outputImage = self.inputImage

        shops.run(self.toString())

        return self.outputImage


# def fslselectvols(infile, outfile, vols):
#     volstr="{0}".format(vols[0])
#     for i in range(1, len(vols)):
#         volstr = volstr + ", {0}".format(vols[i])
#     cmd = "fslselectvols -i {0} -o {1} --vols={2}".format(infile, outfile,
#                                                           volstr)
#     shops.run(cmd)
