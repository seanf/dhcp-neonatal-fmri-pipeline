#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Setup structural masks.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import nibabel as nb
from dhcp.util.enums import SegType
import numpy as np
from dhcp.util.io import Path
from typing import List, Dict, Union


def get_dseg_labels(seg_type: SegType) -> Dict:
    """

    :param seg_type:
    :return:
    """
    if seg_type is SegType.drawem:
        labels = {
            'csf': [1, 5],
            'gm': [2],
            'wm': [3],
            'sc': [7, 9],
            'bs': [8],
            'cb': [6]
        }
    elif seg_type is SegType.freesurfer_aseg:
        labels = {
            'gm': [3, 42],
            'wm': [2, 41, 77, 78, 79, 192, 250, 251, 252, 253, 254, 255],
            'sc': [9, 10, 11, 12, 13, 17, 18, 19, 20, 26, 27, 28, 31, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60,
                   63],
            'bs': [16],
            'cb': [7, 8, 46, 47]
        }
    elif seg_type is SegType.fsl_fast:
        labels = {
            'csf': [1],
            'gm': [2],
            'wm': [3],
            'sc': [2],
            'bs': [3],
            'cb': [3]
        }
    else:
        raise RuntimeError('Unknown segtype.')

    return labels


def convert_to_fsl_fast(dseg: Union[Path, str],
                        type: SegType,
                        outname: Union[Path, str]):
    """

    :param dseg:
    :param type:
    :param outname:
    :return:
    """
    dseg = nb.load(str(dseg))
    d = dseg.get_data()
    d0 = np.zeros(d.shape)

    label_map = get_dseg_labels(type)

    for name, idx in get_dseg_labels(SegType.fsl_fast).items():
        if name not in label_map:
            continue
        for i in label_map[name]:
            d0[d == i] = idx

    nii = nb.Nifti1Image(d0, dseg.affine, dseg.header)
    nii.to_filename(str(outname))


def create_mask(dseg: Union[Path, str],
                dseg_type: SegType,
                labels: List[str],
                outname: Union[Path, str]):
    """

    :param dseg:
    :param dseg_type:
    :param labels:
    :param outname:
    :return:
    """
    dseg = nb.load(str(dseg))
    d = dseg.get_data()
    label_map = get_dseg_labels(dseg_type)

    d0 = np.zeros(d.shape)

    for l in labels:
        for i in label_map[l]:
            d0[d == i] = 1

    nii = nb.Nifti1Image(d0, dseg.affine, dseg.header)
    nii.to_filename(str(outname))
