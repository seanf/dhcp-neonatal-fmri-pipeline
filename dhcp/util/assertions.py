#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Assertions for DHCP fMRI pipeline.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os.path as op
import nibabel as nb
from dhcp.util import io


def assert_isinstance(type_, *args):
    """Raise an exception if the args are not an instance of type_."""
    for d in args:
        assert isinstance(d, type_), 'invalid type {}, should be {}'.format(type(d), type_)


def assert_isint(*args):
    """Raise an exception if the args are not an instance of int."""
    for d in args:
        assert isinstance(d, int), 'invalid type {}, should be int'.format(type(d))

def assert_ispath(*args):
    """Raise an exception if the args are not an instance of Path."""
    for d in args:
        assert isinstance(d, io.Path), 'invalid type {}, should be dhcp.util.io.Path'.format(type(d))


def assert_isfloat(*args):
    """Raise an exception if the args are not an instance of float."""
    for d in args:
        assert isinstance(d, float), 'invalid type {}, should be float'.format(type(d))


def assert_file_exists(*args):
    """Raise an exception if the specified file/folder/s do not exist."""
    for f in args:
        f = io.Path(f)
        if not f.exists():
            raise IOError(f'File/folder does not exist: {f}')


def assert_file_not_exists(*args):
    """Raise an exception if the specified file/folder/s exist."""
    for f in args:
        f = io.Path(f)
        if f.exists():
            raise IOError(f'File/folder should not exist: {f}')


def assert_is_nifti_3D(*args):
    """Raise an exception if the specified file/s are not 3D nifti."""
    for f in args:
        assert_is_nifti(f)
        if isinstance(f, str):
            f = nb.load(f)
        if len(f.shape) != 3:
            raise ValueError(f'incorrect shape for 3D nifti: {f.shape}:{f.get_filename()}')


def assert_is_nifti_4D(*args):
    """Raise an exception if the specified file/s are not 4D nifti."""
    for f in args:
        assert_is_nifti(f)
        if isinstance(f, str):
            f = nb.load(str(f))
        if len(f.shape) != 4:
            raise ValueError(f'incorrect shape for 4D nifti: {f.shape}:{f.get_filename()}')


def assert_is_nifti(*args):
    """Raise an exception if the specified file/s are not nifti."""
    for f in args:
        f = io.Path(f) if isinstance(f, str) else f
        if not isinstance(f, (nb.nifti1.Nifti1Image, nb.nifti2.Nifti2Image)) and f.ext not in ['.nii', '.nii.gz']:
            raise TypeError(f'file must be a nifti (.nii or .nii.gz): {f}')


def assert_nifti_shape(shape, *args):
    """Raise an exception if the specified nifti/s are not specified shape."""
    for fname in args:
        assert_file_exists(fname)
        assert_is_nifti(fname)
        d = nb.load(fname)
        assert d.shape == shape, "incorrect shape ({2}) for nifti: {0}:{1}".format(d.shape, fname, shape)


def assert_is_surf_gifti(*args):
    """Raise an exception if the specified file/s are not surface gifti."""
    for fname in args:
        fname = io.Path(fname) if isinstance(fname, str) else fname
        if fname.ext not in ('.surf', '.gii'):
            raise TypeError(f'file must be a surface gifti (.surf.gii): {fname}')


def assert_is_func_gifti(*args):
    """Raise an exception if the specified file/s are not functional gifti."""
    for fname in args:
        fname = io.Path(fname) if isinstance(fname, str) else fname
        if fname.ext not in ('.func', '.gii'):
            raise TypeError(f'file must be a functional gifti (.func.gii): {fname}')


def assert_is_shape_gifti(*args):
    """Raise an exception if the specified file/s are not shape gifti."""
    for fname in args:
        fname = io.Path(fname) if isinstance(fname, str) else fname
        if fname.ext not in ('.shape', '.gii'):
            raise TypeError(f'file must be a shape gifti (.shape.gii): {fname}')


def assert_is_melodic_dir(path):
    """Raise an exception if the specified path is not a melodic directory.

    :arg path:     Path to melodic directory
    :type path:    string
    """
    path = io.Path(path)

    assert path.exists(), "melodic dir does not exist: {0}".format(path)
    assert path.ext == '.ica', "melodic directory must end in *.ica: {0}".format(path)
    assert op.exists(op.join(path, 'melodic_IC.nii.gz')), (
        "melodic directory must contain a file called melodic_IC.nii.gz"
    )
    assert op.exists(op.join(path, 'melodic_mix')), (
        "melodic directory must contain a file called melodic_mix"
    )
    assert op.exists(op.join(path, 'melodic_FTmix')), (
        "melodic directory must contain a file called melodic_FTmix"
    )
