#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Fetch dHCP volumetric atlas, trained FIX, group maps, and group QC.
"""
import argparse
import os.path as op
import sys

from dhcp.util import util
from dhcp.util.io import Path


def main(path: None,
         fetch_trained_fix=True,
         fetch_volumetric_atlas=True,
         fetch_group_maps=True,
         fetch_group_qc=True
         ):

    if fetch_trained_fix:
        util.fetch_dhcp_trained_fix(path=path)

    if fetch_volumetric_atlas:
        util.fetch_dhcp_volumetric_atlas(path=path)

    if fetch_group_maps:
        util.fetch_dhcp_group_maps(path=path)

    if fetch_group_qc:
        util.fetch_dhcp_group_qc(path=path)


def type_path(x):
    return Path(op.expanduser(x))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        '--path',
        metavar='<path>',
        help='Location for resources (default=<DHCPDIR>/dhcp/resources)',
        type=type_path,
    )

    parser.add_argument('--skip_volumetric_atlas', action='store_false', dest='fetch_volumetric_atlas')
    parser.add_argument('--skip_trained_fix', action='store_false', dest='fetch_trained_fix')
    parser.add_argument('--skip_group_maps', action='store_false', dest='fetch_group_maps')
    parser.add_argument('--skip_group_qc', action='store_false', dest='fetch_group_qc')

    args = vars(parser.parse_args())
    main(**args)
