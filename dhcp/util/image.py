#!/usr/bin/env python
# DHCP_FMRI_PIPELINE - Image filename handling for DHCP pipeline.
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os.path as op
from collections import namedtuple

DOUBLE_EXT = ['.nii.gz', '.tar.gz']


def split(fname):
    """Split the filename into path, basename, and extension.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       path, basename, and extension
    :rtype:         namedtuple of strings
    """

    file = namedtuple('File', ['path', 'basename', 'ext'])

    if any([fname.endswith(x) for x in DOUBLE_EXT]):
        root, ext0 = op.splitext(fname)
        root, ext1 = op.splitext(root)
        return file(op.dirname(root), op.basename(root), ext1+ext0)
    else:
        root, ext0 = op.splitext(fname)
        return file(op.dirname(root), op.basename(root), ext0)


def splitext(fname):
    """Split the filename into path+basename, and extension.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       path/basename, and extension
    :rtype:         tuple of strings
    """
    return op.join(dirname(fname), basename(fname)), ext(fname)


def swapext(fname, ext):
    """Swap the filename extension for ext.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Original filename
    :type ext:      New extension
    :returns:       file with new extension
    :rtype:         string
    """
    return splitext(fname)[0] + ext


def dirname(fname):
    """Return the path to the supplied filename.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       path
    :rtype:         string
    """
    return split(fname)[0]


def basename(fname):
    """Return the basename of the supplied filename.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       basename
    :rtype:         string
    """
    return split(fname)[1]


def ext(fname):
    """Return the extension of the supplied filename.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       extension
    :rtype:         string
    """
    return split(fname)[2]


def basenameext(fname):
    """Return the basename+extension of the supplied filename.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       extension
    :rtype:         string
    """
    return split(fname)[1]+split(fname)[2]
