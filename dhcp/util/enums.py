#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Enums for dHCP pipeline.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from enum import Enum, unique

@unique
class FieldmapType(Enum):
    """Source of fieldmap."""
    spin_echo_epi_derived = 1
    dual_echo_time_derived = 2

@unique
class SegType(Enum):
    """Enum for supported segmentation type."""
    fsl_fast = 1
    freesurfer_aseg = 2
    drawem = 3
    toblerone = 4

@unique
class BBRType(Enum):
    """Enum for supported BBR type."""
    signed = 1
    abs = 2


@unique
class DataType(Enum):
    """Enum for supported image data type."""
    anat = 1
    func = 2
    dwi = 3
    fmap = 4


@unique
class PhaseUnits(Enum):
    """Enum for supported phase units."""
    rads = 1
    hz = 2


@unique
class PhaseEncodeDirection(Enum):
    """Enum for supported phase encode directions."""
    PA = 1
    AP = 2
    LR = 3
    RL = 4
    IS = 5
    SI = 6

@unique
class Structure(Enum):
    """Cortex structure enumeration."""
    CortexLeft = 1
    CortexRight = 2

@unique
class MotionMetric(Enum):
    """Enum for Motion Metric."""
    dvars = 1
    refrms = 2
