#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Image IO for the dHCP pipeline.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
from __future__ import annotations
import json
import os
import os.path as op
from collections import namedtuple
# from typing import Union


import nibabel as nb

# from dhcp.util.image import img

PATH_DOUBLE_EXT = [
    '.nii.gz',
    '.tar.gz',
]


class Path(str):

    def __new__(cls, *args, **kw):
        """

        :rtype: object
        """
        return str.__new__(cls, *args, **kw)

    @property
    def dirname(self) -> Path:
        return Path(self.pathsplit().dirname)

    @property
    def basename(self) -> Path:
        return Path(self.pathsplit().basename)

    @property
    def ext(self) -> str:
        return self.pathsplit().ext

    @property
    def filename(self) -> str:
        return self.basename + self.ext

    def pathsplit(self):
        file = namedtuple('File', ['dirname', 'basename', 'ext'])
        if any([self.endswith(x) for x in PATH_DOUBLE_EXT]):
            root, ext0 = op.splitext(self)
            root, ext1 = op.splitext(root)
            return file(op.dirname(root), op.basename(root), ext1 + ext0)
        else:
            root, ext0 = op.splitext(self)
            return file(op.dirname(root), op.basename(root), ext0)

    def exists(self) -> bool:
        return op.exists(self)

    def makedirs(self):
        os.makedirs(self.dirname)

    def join(self, *args) -> Path:
        return Path(op.join(self, *args))

    def with_ext(self, ext) -> Path:
        return Path(op.join(self.dirname, self.basename + ext))

    def splitext(self):
        return Path(op.join(self.dirname, self.basename)), self.ext

    def abspath(self) -> Path:
        return Path(op.abspath(self))

    def relpath(self, ref) -> Path:
        return Path(op.relpath(self, ref))

    def remove(self):
        os.remove(self)

    @classmethod
    def getcwd(cls):
        return cls(os.getcwd())


# class ImageDict(dict):
#     # TODO: update to use Path class instead of dhcp.util.image
#
#     # def __init__(self):
#     #     super().__init__()
#     #     self.__nib = None
#
#     @property
#     def nib(self):
#         return self.__nib
#
#     @property
#     def filename(self):
#         return None if self.nib is None else Path(self.nib.get_filename())
#
#     @nib.setter
#     def nib(self, d):
#         self.__nib = d
#
#     def to_filename(self, fname):
#         self.nib.to_filename(fname)
#         self.write_sidecar()
#
#     # @property
#     # def dirname(self):
#     #     return None if self.nib is None else img.dirname(self.filename)
#     #
#     # @property
#     # def basename(self):
#     #     return None if self.nib is None else img.basename(self.filename)
#
#     # @property
#     # def ext(self):
#     #     return self.splitext[1]
#     #
#     # @property
#     # def splitext(self):
#     #     return None if self.nib is None else img.splitext(self.filename)
#
#     def write_sidecar(self):
#         sidecar = self.splitext[0] + '.json'
#         with open(sidecar, 'w') as outf:
#             json.dump(self, outf, indent=4)
#
#     def __repr__(self):
#         return repr(self.nib) + '\n' + super(ImageDict, self).__repr__()
#
#     @classmethod
#     def from_filename(cls, fname, **kwargs):
#         # assert file exists
#         fname = Path(fname)
#         d = cls()
#         d.nib = nb.load(fname)
#         sidecar = fname.splitext()[0] + '.json'
#         if op.exists(sidecar):
#             with open(sidecar, 'r') as inf:
#                 d.update(json.load(inf))
#         d.update(**kwargs)
#         return d
#
#     @classmethod
#     def from_nib(cls, nib, **kwargs):
#         d = cls()
#         d.nib = nib
#         d.update(**kwargs)
#         return d
