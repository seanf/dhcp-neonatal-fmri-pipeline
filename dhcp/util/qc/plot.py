#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Quality control plots for the dHCP neonatal fMRI pipeline
"""
import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import nibabel as nb
import numpy as np
from tempfile import TemporaryFile
import base64
import seaborn as sns
import nilearn.plotting as plotting
import ptitprince as pt


def colorbar(ax, cmap, vmin=0, vmax=1, color='k', figsize=(1, 5), orientation='vertical', label=''):
    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=figsize)

    sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=vmin, vmax=vmax))
    sm._A = ax
    ax.set_visible(False)
    cb = plt.colorbar(sm, orientation=orientation, fraction=1)
    cb.set_label(label, color=color)
    cb.ax.yaxis.set_tick_params(color=color)
    cb.ax.xaxis.set_tick_params(color=color)
    cb.outline.set_edgecolor(color)
    plt.setp(plt.getp(cb.ax.axes, 'yticklabels'), color=color)
    plt.setp(plt.getp(cb.ax.axes, 'xticklabels'), color=color)


def raincloudplot(d0, ax=None, xticklabels=None, xtickrot=None, xlabel=None, ylabel=None,
                  title=None, style='whitegrid', figsize=(12, 12), marker=None, z=False,
                  markercolor='r', markersize=100, orient='v', markerstyle='x', markerlegend=None,
                  ylim=None, **kwargs):
    if z:
        mu = np.mean(d0)
        sigma = np.std(d0, ddof=0)

        d0 = (d0 - mu) / sigma

        marker = None if marker is None else (marker - mu) / sigma

    with sns.axes_style(style=style):

        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=figsize)

        pt.RainCloud(
            data=d0, ax=ax, palette="pastel", orient=orient, **kwargs
        )
        # sns.despine(left=True, bottom=True, ax=ax)

        if marker is not None:

            sc = []

            for idx, c in enumerate(d0.columns):
                y = marker[c].values
                x = np.ones(np.array(y).shape) * idx

                if orient is 'h':
                    tmp = x
                    x = y
                    y = tmp

                sc += [ax.scatter(x, y, s=markersize, marker=markerstyle, c=markercolor, linewidths=1, zorder=100)]

            if markerlegend is not None:
                ax.legend(sc, markerlegend)

        if xticklabels is not None:
            ax.set_xticklabels(xticklabels)
        if xtickrot is not None:
            for l in ax.get_xticklabels():
                l.set_rotation(xtickrot)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if title is not None:
            ax.set_title(title)
        if ylim is not None:
            ax.set_ylim(ylim)


def distplot(d0,
             ax=None,
             xlabel=None,
             ylabel=None,
             title=None,
             style='darkgrid',
             figsize=(12, 12),
             **kwargs):
    with sns.axes_style(style=style):

        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=figsize)
        sns.distplot(d0, ax=ax, kde=False, norm_hist=False, **kwargs)

        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if title is not None:
            ax.set_title(title)


def barplot(d0,
            ax=None,
            xticklabels=None,
            xtickrot=None,
            ylabel=None,
            title=None,
            xlabel=None,
            style='darkgrid',
            figsize=(12, 12),
            **kwargs):
    with sns.axes_style(style=style):

        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=figsize)
        sns.barplot(data=d0, ax=ax, **kwargs)

        if xticklabels is not None:
            ax.set_xticklabels(xticklabels)
        if xtickrot is not None:
            for l in ax.get_xticklabels():
                l.set_rotation(xtickrot)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if title is not None:
            ax.set_title(title)


def violinplot(d0, ax=None, xticklabels=None, xtickrot=None, xlabel=None, ylabel=None,
               title=None, style='darkgrid', figsize=(12, 12), marker=None, z=False,
               markercolor='#FFFF00', markersize=250, **kwargs):
    if z:
        mu = np.mean(d0)
        sigma = np.std(d0, ddof=0)

        d0 = (d0 - mu) / sigma

        marker = None if marker is None else (marker - mu) / sigma

    with sns.axes_style(style=style):

        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=figsize)

        sns.violinplot(
            data=d0, ax=ax, width=.5, palette="pastel",
            linewidth=1, inner="point", **kwargs
        )
        sns.despine(left=True, bottom=True, ax=ax)

        if marker is not None:

            for idx, c in enumerate(d0.columns):
                y = marker[c].values
                x = np.ones(np.array(y).shape) * idx
                ax.scatter(x, y, s=markersize, marker='*', c=markercolor, linewidths=1)

        if xticklabels is not None:
            ax.set_xticklabels(xticklabels)
        if xtickrot is not None:
            for l in ax.get_xticklabels():
                l.set_rotation(xtickrot)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if title is not None:
            ax.set_title(title)


def motion_parameters(mp: np.ndarray,
                      figsize=(22, 8),
                      ax=None,
                      title='Motion Parameters',
                      xlabel='TRs'):
    '''Plot motion parameters timeseries.'''
    mp = np.array(mp)

    rot = mp[:, :3]
    tr = mp[:, 3:]

    nTR = mp.shape[0]

    if ax is None:
        _, (ax0, ax1) = plt.subplots(2, 1, figsize=figsize)

    # fig, (ax0, ax1) = plt.subplots(2, 1, figsize=figsize)

    # plot rotations
    ln0 = ax0.plot(rot)
    ax0.legend(['X', 'Y', 'Z'], loc=1)
    ax0.grid()
    ax0.set_ylabel('Rotation (radians)')
    ax0.set_xlim(0, nTR)
    ax0.set_xticklabels([])

    # plot translations
    ln1 = ax1.plot(tr)
    ax1.legend(['X', 'Y', 'Z'], loc=1)
    ax1.grid()
    ax1.set_ylabel('Translation (mm)')
    ax1.set_xlim(0, nTR)

    if xlabel is not None:
        ax1.set_xlabel(xlabel)
    if title is not None:
        ax0.set_title(title)


def framewise_displacement(fd: np.ndarray,
                           figsize=(22, 4),
                           ax=None,
                           title='Framewise Displacement',
                           ylabel='mm',
                           xlabel='TRs'):
    '''Plot framewise displacement timeseries.'''
    fd = np.array(fd)

    nTR = fd.shape[0]

    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=figsize)

    # plot fd
    ln = ax.plot(fd)
    ax.grid()
    ax.set_xlim(0, nTR)

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if title is not None:
        ax.set_title(title)


def dvars(dvars: np.ndarray,
          figsize=(22, 4),
          ax=None,
          title='DVARs',
          ylabel='DVARs',
          xlabel='TRs',
          legend=None):
    '''Plot DVARS timeseries.'''
    dvars = np.array(dvars)
    nTR = dvars.shape[0]

    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=figsize)

    # plot dvars
    ln = ax.plot(dvars)
    ax.grid()
    ax.set_xlim(0, nTR)

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if title is not None:
        ax.set_title(title)
    if legend is not None:
        ax.legend(legend)


def netmat(netmat: np.ndarray,
           figsize=(12, 10),
           ax=None,
           cmap='bwr',
           title='netmat',
           ylabel=None,
           xlabel=None,
           colorbar=True,
           colorbarlabel=None,
           cbar_shrink=0.75,
           clim=(-3, 3)):
    '''Plot netmat.'''

    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=figsize)

    mp = ax.matshow(netmat)
    mp.set_clim(clim[0], clim[1])
    mp.set_cmap(cmap)

    # add colorbar
    if colorbar:
        cbar = plt.colorbar(mp, ax=ax, shrink=cbar_shrink)
        if colorbarlabel is not None:
            cbar.set_label(colorbarlabel)

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if title is not None:
        ax.set_title(title)


def spatialcor(sc: np.ndarray,
               figsize=(12, 10),
               ax=None,
               title='Correlation with spatial template',
               ylabel='Correlation',
               xlabel=None,
               xticklabels=None,
               xtickrot=None,
               ymin=0,
               ymax=1):
    '''Plot spatial correlation as barplot.'''

    sc = np.array(sc)

    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=figsize)

    ax.set_axisbelow(True)
    ax.grid()

    bar = ax.bar(np.arange(np.max(sc.shape)), sc)
    ax.set_ylim(ymin, ymax)
    ax.set_xticks(np.arange(sc.shape[0]))

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if title is not None:
        ax.set_title(title)

    if xticklabels is not None:
        ax.set_xticklabels(xticklabels)

    if xtickrot is not None:
        for l in ax.get_xticklabels():
            l.set_rotation(xtickrot)


def voxplot(img: nb.Nifti1Image,
            dseg=None,
            dseg_labels=None,
            brain_mask=None,
            ax=None,
            figsize=(12, 6),
            title=None,
            xlabel='TRs',
            ylabel='Voxels',
            colorbar=True,
            colorbarlabel=None,
            cmap='bwr',
            clim=None,
            cbar_shrink=0.75,
            zscore=False,
            grid_color='w',
            grid_style=':',
            grid_width=2,
            img_mean=None,
            img_std=None,
            interp='nearest',
            ):
    if ax is None:
        _, ax = plt.subplots(figsize=figsize)

    img = img.get_data()
    img = np.reshape(img, (-1, img.shape[-1]))

    if dseg is not None and dseg_labels is not None:
        idx = []
        count = [0]
        labels = []

        dseg = dseg.get_data().ravel()
        for k, m in dseg_labels.items():
            labels += [k]
            idx0 = list(np.where(np.isin(dseg, m))[0])
            idx += idx0
            count += [int(len(idx0))]
        img = img[idx, :]
    elif brain_mask is not None:
        brain_mask = brain_mask.get_data().ravel().astype(bool)
        img = img[brain_mask, :]

    # regress the temporal mean and linear trend as per Power
    r0 = np.ones((2, img.shape[1]))
    r0[1, :] = np.linspace(0, 1, img.shape[1])

    beta = np.dot(np.linalg.pinv(r0.T), img.T)
    pred = np.dot(r0.T, beta)
    img = img - pred.T

    if zscore:
        img_mean = np.mean(img) if img_mean is None else img_mean
        img_std = np.std(img) if img_std is None else img_std
        img = (img - img_mean) / img_std

    aximg = ax.imshow(img, aspect='auto', interpolation=interp)
    aximg.set_cmap(cmap)

    if clim is not None:
        aximg.set_clim(clim)

    if dseg is not None and dseg_labels is not None:
        count = np.cumsum(np.array(count))[:-1]
        ax.yaxis.set_major_locator(ticker.FixedLocator(count))
        ax.set_yticklabels(dseg_labels.keys())
    else:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    ax.xaxis.grid(False)
    ax.grid(linewidth=grid_width, axis='y', color=grid_color, linestyle=grid_style)

    # add colorbar
    if colorbar:
        cbar = plt.colorbar(aximg, ax=ax, shrink=cbar_shrink)
        if colorbarlabel is not None:
            cbar.set_label(colorbarlabel)

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if title is not None:
        ax.set_title(title)


def plot_overlay(base, overlay=None, levels=[0.5, 1.5, 2.5, 3.5], linewidth=1, contrast=0, colors=['yellow', 'red', 'aqua', 'lime'], **kwargs):

    display = plotting.plot_anat(base, dim=contrast * -1, **kwargs)
    if overlay is not None:
        # display.add_edges(overlay)
        # display.add_contours(img, levels=[.5], colors=’r’)
        display.add_contours(overlay, linewidths=linewidth, levels=levels, colors=colors)


def plot_overlay_contour(base, overlay=None, levels=[0.5, 1.5, 2.5, 3.5], linewidth=1, contrast=0, colors=['yellow', 'red', 'aqua', 'lime'], **kwargs):

    display = plotting.plot_anat(base, dim=contrast * -1, **kwargs)
    if overlay is not None:
        display.add_contours(overlay, linewidths=linewidth, levels=levels, colors=colors)


def plot_overlay_edges(base, overlay=None, contrast=0, color=None, **kwargs):

    display = plotting.plot_anat(base, dim=contrast * -1, **kwargs)
    if overlay is not None:
        display.add_edges(overlay)


def to_file(fcn, fname, *args, **kwargs):
    dpi = kwargs.pop('dpi', 100)
    fcn(*args, **kwargs)
    fig = plt.gcf()
    # fig.tight_layout()
    plt.savefig(fname, dpi=dpi, bbox_inches='tight')
    # fig.savefig(fname, dpi=dpi)
    plt.close()


def to_base64(fcn, *args, **kwargs):
    ext = kwargs.pop('ext', '.svg')
    with TemporaryFile(suffix=ext) as tmp:
        to_file(fcn, tmp, *args, **kwargs)
        tmp.seek(0)
        s = base64.b64encode(tmp.read()).decode("utf-8")
    return 'data:image/{};base64,'.format(ext) + s
