#!/usr/bin/env python
#
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Quality control utilities for the dHCP neonatal fMRI pipeline
"""
import json
import os.path as op
import subprocess
import sys
from collections import OrderedDict
from tempfile import TemporaryDirectory
from typing import Tuple

import nibabel as nb
import numpy as np


def dict2json(dict, jsonfile, indent=4):
    """Write dictionary to json file."""
    for k, v in dict.items():

        if isinstance(v, np.ndarray):
            dict[k] = v.tolist()
        elif isinstance(v, nb.Nifti1Image):
            dict[k] = v.get_filename()

    with open(jsonfile, 'w') as outfile:
        json.dump(dict, outfile, indent=indent)


def json2dict(jsonfile):
    """Read dictionary from json file."""
    with open(jsonfile, 'r') as infile:
        d = json.load(infile, object_pairs_hook=OrderedDict)
    return d


def run(cmd):
    for i, v in enumerate(cmd):
        if isinstance(v, nb.Nifti1Image):
            cmd[i] = v.get_filename()
    print("RUNNING: " + " ".join(cmd))
    sys.stdout.flush()
    jobout = subprocess.check_output(cmd)
    return jobout.decode('utf-8').strip()


def load_img(d):
    return nb.load(op.expanduser(d)) if isinstance(d, str) else d


def bet(func, maskname, tmpdir=None):
    with TemporaryDirectory(dir=tmpdir) as td:
        run(['fslmaths', func.get_filename(), '-Tmean', op.join(td, 'mean')])
        run(['bet', op.join(td, 'mean'), op.join(td, 'brain'), '-n', '-m'])
        nb.load(op.join(td, 'brain_mask.nii.gz')).to_filename(maskname)
        maskname = nb.load(maskname)

    return maskname


def split(fname: str) -> Tuple[str, str, str]:
    """Split the filename into path, basename, and extension.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       path, basename, and extension
    :rtype:         tuple of strings
    """
    if fname.endswith('.nii.gz'):
        return (op.dirname(fname),
                op.basename(op.splitext(op.splitext(fname)[0])[0]),
                '.nii.gz')
    else:
        return (op.dirname(fname),
                op.basename(op.splitext(fname)[0]),
                op.splitext(fname)[1])


def remove_ext(fname: str) -> str:
    """Remove the extension from the supplied filename.

    Can handle zipped nifti (*.nii.gz)

    :arg fname:     Filename to be split
    :type fname:    string
    :returns:       dirname+basename
    :rtype:         string
    """
    return op.join(split(fname)[0], split(fname)[1])
