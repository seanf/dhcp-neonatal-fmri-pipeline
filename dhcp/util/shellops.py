#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Shell operations for DHCP fMRI pipeline.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import logging
import sys
from subprocess import check_output
import os
import os.path as op

logger = logging.getLogger(__name__)


def run(cmd):
    if type(cmd) is list:
        str = " ".join(cmd)
    else:
        str = cmd
        cmd = str.split()

    logger.info(str)
    sys.stdout.flush()
    jobout = check_output(cmd)
    return jobout.decode('utf-8').strip()


def run_fsl(cmd, fsldir=None):
    if fsldir is None:
        fsldir = os.getenv('FSLDIR')
    assert fsldir is not None, 'Cannot locate FSL'

    if type(cmd) is str:
        cmd = cmd.split()

    cmd[0] = op.join(fsldir, 'bin', cmd[0])

    return run(cmd)
