#!/usr/bin/env python
"""
    Image coordinate operations.

    Mark Jenkinson, Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import os
import numpy as np
import nibabel as nib
import scipy.ndimage as ndimage
# import scipy.interpolate as interp
import nibabel.gifti.giftiio as gio

import pdb
# pdb.set_trace()

nib.nifti1.Nifti1Header.quaternion_threshold = \
    -np.finfo(np.float32).eps * 1000000

# GIFTI and NIFTI support functions
#


def read_gifti_verts(giftiname):
    # Read in GIFTI
    gft = gio.read(giftiname)
    a = gft.getArraysFromIntent('NIFTI_INTENT_POINTSET')
    assert len(a) == 1, 'Cannot handle gifti with multiple ' + \
        'NIFTI_INTENT_POINTSET arrays'
    return a.pop().data


def get_best_affine(im, preference='sform'):

    if preference == 'sform' and (im.header['sform_code'] > 0):
        return im.get_sform()

    if preference == 'qform' and (im.header['qform_code'] > 0):
        return im.get_qform()

    # im.affine algorithm is defined as:
    # If sform_code != 0 (‘unknown’) use the sform affine; else
    # If qform_code != 0 (‘unknown’) use the qform affine; else
    # Use the fall-back affine.
    return im.affine

# General coordinate support functions

# xfm(...) works with either coord conventions - it all depends on the inputs
# TODO: I think this has a bug for less than three coords


def xfm(mat, coord):
    v = np.array(coord)
    [nr, nc] = v.shape
    if nr < nc and (nc == 3 or nc == 4):
        return xfm(mat, v.T)
    if nc == 3:
        v = np.hstack((v, np.ones([nr, 1])))
    v = np.dot(mat, v.T).T
    if len(v.shape) > 1:
        v = v[:, :3]
    else:
        v = v[:3]
    return v


def int_coord(coord):
    #icoord=[int(round(x)) for x in coord]
    icoord = np.round(np.array(coord)).astype(int)
    return icoord

# NIFTI coordinate support functions


def niimm2vox(coord, im):
    qf = im.get_qform()
    qfi = np.linalg.inv(qf)
    return xfm(qfi, coord)


def vox2niimm(coord, im):
    qf = im.get_qform()
    return xfm(qf, coord)

# FSL coordinate support functions


def vox2fslmm_mat(im):
    [dx, dy, dz] = im.header.get_zooms()[:3]
    # vox2mm_mat = np.matrix(np.zeros([4, 4]))
    vox2mm_mat = np.zeros([4, 4])
    vox2mm_mat[0, 0] = dx
    vox2mm_mat[1, 1] = dy
    vox2mm_mat[2, 2] = dz
    vox2mm_mat[3, 3] = 1
    # Put in x-axis swap for neurological volumes here
    if np.linalg.det(get_best_affine(im)) > 0:
        vox2mm_mat[0, 0] = -dx
        nx = im.get_data().shape[0]
        vox2mm_mat[0, 3] = (nx - 1) * dx
    return vox2mm_mat


def vox2fslmm(coord, im):
    vox2mm_mat = vox2fslmm_mat(im)
    return xfm(vox2mm_mat, coord)


def fslmm2vox(coord, im):
    vox2mm_mat = vox2fslmm_mat(im)
    return xfm(np.linalg.inv(vox2mm_mat), coord)


def flirt2vox_mat(mat, inpim, refim):
    v2mm_in = vox2fslmm_mat(inpim)
    v2mm_ref = vox2fslmm_mat(refim)
    vmat = np.linalg.inv(v2mm_ref) * np.matrix(mat) * v2mm_in
    return vmat


def rel_abs_convention(warpdata):
    conv = 'abs'
    [nx, ny, nz] = warpdata.shape[:3]
    [dx, dy, dz] = warpdata.get_header().get_zooms()[:3]
    warpdata = warpdata.get_data()
    # for relative warps the values should range around zero
    #  but for abs warps the values should lie in 0 to +FOV in mm
    # use 0.4 times FOV in mm as a rough cutoff (0.5 unreliable?)
    if warpdata.mean() < 0.4 * min((nx * dx, ny * dy, nz * dz)):
        conv = 'rel'
    return conv


def warp_coords(warpim, inpim, coords):
    # input coords [coords] as voxel coords in reference (structural) space
    #  and warp [warpim] from input (functional) space [inpim] to reference space
    # output coords are voxel coords
    warpd = warpim.get_data()
    co = np.array(coords)
    # interpolate to get coords in input space (abs convention)
    newx = ndimage.map_coordinates(warpd[:, :, :, 0], co.T)
    newy = ndimage.map_coordinates(warpd[:, :, :, 1], co.T)
    newz = ndimage.map_coordinates(warpd[:, :, :, 2], co.T)
    # need to do something about relative convention here - TODO!!
    if rel_abs_convention(warpim) == 'rel':
        mmco = np.array(vox2fslmm(co, warpim))
        newx += mmco[:, 0]
        newy += mmco[:, 1]
        newz += mmco[:, 2]
    newcoords = np.vstack((newx, newy, newz)).T
    newcoords = fslmm2vox(newcoords, inpim)
    return newcoords


# FreeSurfer coordinate support functions

# From: https://surfer.nmr.mgh.harvard.edu/fswiki/CoordinateSystems
# 3. I have a point on the surface ("Vertex RAS" in tksurfer) and want to compute the Scanner RAS in orig.mgz that corresponds to this point:
#
# ScannerRAS = Norig*inv(Torig)*[tkrR tkrA tkrS 1]'
#
# Norig: mri_info --vox2ras orig.mgz Torig: mri_info --vox2ras-tkr orig.mgz
# Test: click on a point in tksurfer. Use "Vertex RAS" to compute
# ScannerRAS. Save Point. In tkmedit, Goto Saved Point, compare ScannerRAS
# to "Volume Scanner Coordinates".


####################

# Main code

# def p_main1():
#
#     struct=nib.load('highres.nii.gz')
#     epi=nib.load('example_func.nii.gz')
#     warp=nib.load('example_func2highres_warp.nii.gz')
#
#     edata=epi.get_data()
#     maxe=np.max(edata)
#
#     wc=np.array([[72,65,87],[114,61,87],[74,136,74],[105,135,74]])
#     epicoord=int_coord(warp_coords(warp,epi,wc))
#     for n in range(epicoord.shape[0]):
#         edata[epicoord[n,0],epicoord[n,1],epicoord[n,2]]=2*maxe
#     nib.save(epi,'grot.nii.gz')
#
#     return 0
#
#
# def p_main2():
#
#     struct=nib.load('T2.nii')
#     wc=read_gifti_verts('L.white.native.surf')
#     wc=wc[:,:3]
#     pc=read_gifti_verts('L.pial.native.surf')
#     pc=pc[:,:3]
#
#     sdata=struct.get_data()
#     maxv=np.max(sdata)
#     # transform WM coord
#     icoord=int_coord(warp_coord(niimm2vox(wc,struct))
#     for n in range(wc.shape[0]):
#         sdata[icoord[n,0],icoord[n,1],icoord[n,2]]=1.5*maxv    # do something with the coord
#     # transform GM (pial) coord
#     icoord=int_coord(niimm2vox(pc,struct))
#     for n in range(wc.shape[0]):
#         sdata[icoord[n,0],icoord[n,1],icoord[n,2]]=2*maxv    # do something with the coord
#
#     nib.save(struct,'grot.nii.gz')
#
#     return 0

####################

# Call the main function
# p_main2()
