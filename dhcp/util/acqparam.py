#!/usr/bin/env python
"""DHCP_FUNC - Setup acq params for EDDY/TOPUP and FLIRT."""
# pylint: disable=R0913,W0511,R0914,C0103
import argparse
import json
import os.path as op
import nibabel as nb
import numpy as np


def main(epi, echospacing, pedir, outname, epifactor=None, inplaneaccel=1,
         outflirt=None):

    # parse args
    # TODO: do some error checking on args

    pedir = pedir.split(",")

    eddyp = np.zeros((len(pedir), 4))
    ax = [''] * len(pedir)

    echospacing = float(echospacing)
    inplaneaccel = float(inplaneaccel)

    epi = nb.load(epi)

    # loop through PEdirs and determine axis orientation, epi factor,
    # and timing for eddy/topup

    for i in range(len(pedir)):

        ax[i], axidx, axdir = _get_axis(epi, pedir[i])

        eddyp[i, axidx] = axdir

        if epifactor is None:
            epif = epi.shape[axidx]
        else:
            epif = int(epifactor)

        eddyp[i, 3] = (epif - 1) * echospacing / inplaneaccel

    # write params for FLIRT
    if outflirt is not None:
        param = {'pedir': ax, 'echospacing': echospacing}
        _dict2json(param, outflirt)

    # write params for EDDY/TOPUP
    np.savetxt(outname, eddyp, fmt='%i %i %i %f')


def _get_axis(epi, pedir):

    if not isinstance(epi, nb.nifti1.Nifti1Image):
        epi = nb.load(epi)
    ornt = _get_orientation(epi)

    if ornt[0] == pedir:
        ax = 'x'
        axidx = 0
        axdir = 1
    elif _flip(ornt[0]) == pedir:
        ax = 'x-'
        axidx = 0
        axdir = -1
    elif ornt[1] == pedir:
        ax = 'y'
        axidx = 1
        axdir = 1
    elif _flip(ornt[1]) == pedir:
        ax = 'y-'
        axidx = 1
        axdir = -1
    elif ornt[2] == pedir:
        ax = 'z'
        axidx = 2
        axdir = 1
    elif _flip(ornt[2]) == pedir:
        ax = 'z-'
        axidx = 2
        axdir = -1

    return ax, axidx, axdir


def _flip(d):
    return d[::-1]


def _get_orientation(epi):

    labels = (('RL', 'LR'), ('AP', 'PA'), ('SI', 'IS'))

    code = nb.orientations.aff2axcodes(epi.affine, labels)
    return code


def _dict2json(dict, jsonfile, indent=4):
    """Write dictionary to json file."""
    with open(jsonfile, 'w') as outfile:
        json.dump(dict, outfile, indent=indent)


if __name__ == "__main__":

    DESC = "Setup acq params for EDDY/TOPUP and FLIRT"

    PARSER = argparse.ArgumentParser(description=DESC,
                                     argument_default=argparse.SUPPRESS)

    REQD = PARSER.add_argument_group('Required')
    REQD.add_argument('--epi', help='Name of EPI image', required=True)
    REQD.add_argument('--echospacing', help='EPI echo spacing in seconds',
                      required=True)
    REQD.add_argument('--pedir',
                      help='EPI phase encode direction (AP,PA,LR,RL,IS,SI)',
                      required=True)

    OPT = PARSER.add_argument_group('Optional')
    OPT.add_argument('--epifactor', default=None,
                     help='EPI factor (calulated from EPI by default)')
    OPT.add_argument('--inplaneaccel', default=1,
                     help='In plane acceleration factor (default=1)')
    OPT.add_argument('--outname', default=None,
                     help='Basename for output files')

    ARGS = PARSER.parse_args()

    main(**vars(ARGS))


def phase_encode_dict():
    pedict = {
        'x': (1, 'x'),
        'y': (2, 'y'),
        'z': (3, 'z'),
        '-x': (-1, 'x-'),
        '-y': (-2, 'y-'),
        '-z': (-3, 'z-'),
        'x-': (-1, 'x-'),
        'y-': (-2, 'y-'),
        'z-': (-3, 'z-')
    }
    return pedict