#!/usr/bin/env python
"""
    DHCP_FMRI_PIPELINE - Colleciton of utilities functions for DHCP pipeline.

    Sean Fitzgibbon
    FMRIB Analysis Group

    Copyright 2017 University of Oxford

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import json
import logging
import os
import os.path as op
import tarfile
import time
import urllib.request as urllib
from shutil import copy as shcopy
from typing import Union

import fsl.utils.filetree as filetree
import nibabel as nb
from tqdm import tqdm
from tempfile import TemporaryDirectory

import dhcp.util.assertions as asrt
import dhcp.util.shellops as shops
from dhcp.util import assertions as assrt
from dhcp.util.io import Path

logger = logging.getLogger(__name__)

SETTINGS_FILENAME='settings.json'

def timeit(f):
    def timed(*args, **kw):
        t_start = time.time()
        logger.info('BEGIN {}'.format(f.__name__))
        result = f(*args, **kw)
        t_end = time.time()
        logger.info("END {}: Elapsed time {:0.2f}s".format(f.__name__, t_end - t_start))
        return result

    return timed


def get_dhcp_dir() -> Path:
    rpath = op.realpath(op.join(op.dirname(op.abspath(__file__)), ".."))
    return Path(rpath)


def get_resource_path() -> Path:
    rpath = op.realpath(
        op.join(op.dirname(op.abspath(__file__)), "../resources"))
    return Path(rpath)


def get_resource(name) -> Path:
    r = Path(op.join(get_resource_path(), name))
    asrt.assert_file_exists(r)
    return r


def read_settings_file():
    settings_fname = get_resource_path().join(SETTINGS_FILENAME)
    if not op.exists(settings_fname):
        dict2json({}, settings_fname)
    return json2dict(settings_fname)


def write_settings_file(settings_dict):
    settings_fname = get_resource_path().join(SETTINGS_FILENAME)
    dict2json(settings_dict, settings_fname)


def clear_settings():
    settings_fname = get_resource_path().join(SETTINGS_FILENAME)
    dict2json({}, settings_fname)

def get_setting(name, default=None):
    settings = read_settings_file()
    return settings[name] if name in settings else default

def has_setting(name):
    settings = read_settings_file()
    return name in settings


def add_setting(name, value):

    settings_fname = get_resource_path().join(SETTINGS_FILENAME)

    if not op.exists(settings_fname):
        dict2json({}, settings_fname)

    settings = read_settings_file()
    settings[name] = value
    write_settings_file(settings)


def is_std_orient(nfti: Union[Path, str]) -> str:
    """Test if image has same oriented as std (MNI)."""
    d = nb.load(str(nfti))
    return nb.orientations.aff2axcodes(d.affine)[1:] == ('L', 'A', 'S')[1:]


def copy_zip_orient(infile: Union[Path, str],
                    outfile: Union[Path, str]) -> Path:
    """Copy, gzip, and reorient to std."""
    if infile != outfile:
        copy_zip(infile, outfile)  # copy and gzip
    shops.run("fslreorient2std {0} {0}".format(outfile))  # reorient

    return outfile


def copy_zip(infile: Union[Path, str],
             outfile: Union[Path, str]) -> Path:
    """Copy and if necessary gzip nifti."""
    infile = Path(infile)
    outfile = Path(outfile)
    if infile != outfile:
        if infile.endswith('.gz'):
            shcopy(infile, outfile)  # copy
        else:
            if outfile.endswith('.gz'):
                outfile = Path(outfile[:-3])
            shcopy(infile, outfile)  # copy
            shops.run("gzip -f {0}".format(str(outfile)))  # gzip
        # copy sidecar if it exists
        # if infile.with_ext('.json').exists():
        #     shcopy(infile.with_ext('.json'), outfile.with_ext('.json'))

    return outfile


def rel_sym_link(target: Union[Path, str],
                 linkname: Union[Path, str]) -> Path:
    """Create a relative symlink."""

    target = Path(target)
    linkname = Path(linkname)

    asrt.assert_file_exists(target)

    linkname = linkname.abspath()
    if linkname.exists():
        linkname.remove()

    target = target.relpath(linkname.dirname)
    cmd = "ln -s {target} {linkname}".format(target=target, linkname=linkname)
    shops.run(cmd)

    return linkname


def imgopen(inimg, outimg=None):
    if outimg is None:
        outimg = inimg
    cmd = "fslmaths {0} -dilM -ero {1}".format(inimg, outimg)
    shops.run(cmd)


def imgclose(inimg, outimg=None):
    if outimg is None:
        outimg = inimg
    cmd = "fslmaths {0} -ero -dilM {1}".format(inimg, outimg)
    shops.run(cmd)


def imgdilate(inimg, outimg=None):
    if outimg is None:
        outimg = inimg
    cmd = "fslmaths {0} -dilM {1}".format(inimg, outimg)
    shops.run(cmd)


def dict2json(dict, jsonfile, indent=4):
    """Write dictionary to json file."""
    with open(jsonfile, 'w') as outfile:
        json.dump(dict, outfile, indent=indent)


def json2dict(jsonfile):
    """Read dictionary from json file."""
    with open(jsonfile, 'r') as infile:
        d = json.load(infile)
    return d


def volreorder(input, output, index):
    """Reorder volumes in a 4D nifti."""
    d = nb.load(input)
    d0 = nb.Nifti1Image(d.get_data()[:, :, :, index], d.affine, d.header)
    nb.save(d0, output)


def load_default_filetree(override_defaults={}):
    # get default pipeline filetree
    filetree.tree_directories += [get_resource_path()]
    ft = filetree.FileTree.read('dhcp-defaults')

    # override default filetree
    for k, v in override_defaults.items():
        if k not in ft.templates:
            raise ValueError(f'unknown default key {k}')
        ft.templates[k] = v

    return ft


def validate_path_input(fname, ndim=3, test_exist=True, test_nifti=True):
    fname = Path(fname)

    if test_exist:
        assrt.assert_file_exists(fname)

    if test_nifti:
        if test_exist & (ndim == 3):
            assrt.assert_is_nifti_3D(fname)
        elif test_exist & (ndim == 4):
            assrt.assert_is_nifti_4D(fname)
        else:
            assrt.assert_is_nifti(fname)

    return fname


def get_fsl_version():
    with open(op.join(os.environ['FSLDIR'], 'etc/fslversion'), 'r') as f:
        ver = f.read().split(':')[0]
    return ver


def update_sidecar(fname, **kwargs):
    """

    Args:
        fname:
        **kwargs:

    Returns:

    """
    sidecar = Path(fname).splitext()[0] + '.json'

    d = load_sidecar(fname)
    d.update(**kwargs)

    with open(sidecar, 'w') as out_f:
        json.dump(d, out_f, indent=4)


def load_sidecar(fname):
    """

    Args:
        fname:

    Returns:

    """
    sidecar = Path(fname).splitext()[0] + '.json'
    d = {}

    if Path(sidecar).exists():
        with open(sidecar, 'r') as in_f:
            d.update(json.load(in_f))
    return d


def fetch_dhcp_volumetric_atlas(path=None, extended=True):
    """

    Args:
        path:

    Returns:

    """
    path = get_resource_path() if path is None else op.realpath(op.expanduser(path))

    if extended:
        atlas_url = 'https://users.fmrib.ox.ac.uk/~seanf/dhcp-augmented-volumetric-atlas-extended.tar.gz'
        path = f'{path}/dhcp_volumetric_atlas_extended'
    else:
        atlas_url = 'https://users.fmrib.ox.ac.uk/~seanf/dhcp-augmented-volumetric-atlas.tar.gz'
        path = f'{path}/dhcp_volumetric_atlas'

    if not op.exists(path):
        os.makedirs(path)

    print('Download atlas:')

    with TemporaryDirectory(dir=path) as tmp:

        with DownloadBar(unit='B', unit_scale=True, miniters=1,
                         desc=atlas_url.split('/')[-1]) as t:  # all optional kwargs
            file_tmp = urllib.urlretrieve(atlas_url, filename=op.join(tmp, 'atlas.tar.gz'), reporthook=t.update_to)[0]

        print('Unpack atlas:')
        with tarfile.open(name=file_tmp) as tar:
            for m in tqdm(iterable=tar.getmembers(), total=len(tar.getmembers())):
                tar.extract(member=m, path=path)

    add_setting(
        'dhcp_volumetric_atlas_tree',
        op.join(path, 'atlas', 'atlas.tree')
    )


def fetch_dhcp_trained_fix(path=None):
    """

    Args:
        path:

    Returns:

    """
    rdata_url = 'https://users.fmrib.ox.ac.uk/~seanf/train35.RData'

    path = get_resource_path() if path is None else op.realpath(op.expanduser(path))

    if not op.exists(path):
        os.makedirs(path)

    path = op.join(path, op.basename(rdata_url))

    print('Download trained FIX:')

    with DownloadBar(unit='B', unit_scale=True, miniters=1,
                     desc=rdata_url.split('/')[-1]) as t:  # all optional kwargs
        urllib.urlretrieve(rdata_url, filename=path, reporthook=t.update_to)

    add_setting('dhcp_trained_fix', path)
    add_setting('dhcp_trained_fix_threshold', 10)


def fetch_dhcp_group_qc(path=None):
    rdata_url = 'https://users.fmrib.ox.ac.uk/~seanf/grp_qc_512_anon.json'

    path = get_resource_path() if path is None else op.realpath(op.expanduser(path))

    if not op.exists(path):
        os.makedirs(path)

    path = op.join(path, op.basename(rdata_url))

    print('Download group QC:')

    with DownloadBar(unit='B', unit_scale=True, miniters=1,
                     desc=rdata_url.split('/')[-1]) as t:  # all optional kwargs
        urllib.urlretrieve(rdata_url, filename=path, reporthook=t.update_to)

    add_setting('dhcp_group_qc', path)


def fetch_dhcp_group_maps(path=None):
    rdata_url = 'https://users.fmrib.ox.ac.uk/~seanf/group_maps.nii.gz'

    path = get_resource_path() if path is None else op.realpath(op.expanduser(path))

    if not op.exists(path):
        os.makedirs(path)

    path = op.join(path, op.basename(rdata_url))

    print('Download group maps:')

    with DownloadBar(unit='B', unit_scale=True, miniters=1,
                     desc=rdata_url.split('/')[-1]) as t:  # all optional kwargs
        urllib.urlretrieve(rdata_url, filename=path, reporthook=t.update_to)

    add_setting('dhcp_group_maps', path)



# FROM TQDM DOCS https://github.com/tqdm/tqdm#table-of-contents
class DownloadBar(tqdm):
    """Provides `update_to(n)` which uses `tqdm.update(delta_n)`."""

    def update_to(self, b=1, bsize=1, tsize=None):
        """
        b  : int, optional
            Number of blocks transferred so far [default: 1].
        bsize  : int, optional
            Size of each block (in tqdm units) [default: 1].
        tsize  : int, optional
            Total size (in tqdm units). If [default: None] remains unchanged.
        """
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)  # will also set self.n = b * bsize
