#!/usr/bin/env python
# Sean Fitzgibbon
# FMRIB Analysis Group
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
dHCP neonatal fMRI pipeline - main CLI script
"""
import argparse
from dhcp.util.io import Path
import os.path as op
import sys
from dhcp.util.enums import PhaseEncodeDirection, SegType, FieldmapType
from dhcp.util import util
from dhcp.func.pipeline import Pipeline

from dhcp.util.enums import PhaseEncodeDirection


def type_path(x):
    return Path(op.expanduser(x))


def type_slice_spec(x):
    if x == 'dhcp':
        x = util.get_resource('default_func.slorder')
    return type_path(x)


def type_pedir(x):
    return PhaseEncodeDirection[x]


def type_pedir_list(xlist):
    return [PhaseEncodeDirection[x] for x in xlist.split(',')]


def type_segtype(x):
     return SegType[x]


def type_fieldmap(x):
    return FieldmapType[x]


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers()

    parser.add_argument(
        'path',
        help='Base path for pipeline outputs',
        type=type_path
    )
    parser.add_argument(
        'subid',
        help='Subject ID',
        type=str
    )
    parser.add_argument(
        'sesid',
        help='Session ID',
        type=str
    )

    #############################################
    # create the parser for the RUNALL command
    #############################################

    parser_run = subparsers.add_parser(
        'RUNALL',
        description='Run full pipeline',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_run.add_argument(
        '--fieldmap_source',
        choices={'spin_echo_epi_derived', 'dual_echo_time_derived'},
        help='Fieldmap type [default=spin_echo_epi_derived]',
        type=type_fieldmap,
        default='spin_echo_epi_derived'
    )

    parser_run.add_argument(
        '--pedir_n',
        metavar='<int>',
        help='Number of volumes per spinecho pedir to use [default=1]',
        type=int,
        default=1
    )

    parser_run.add_argument(
        '--temporal_fwhm',
        metavar='<float>',
        help='Temporal filter FWHM (seconds) [default=150s]',
        type=float,
        default=150.0,
    )

    parser_run.add_argument(
        '--icadim',
        metavar='<int>',
        help='Maximum ICA dimension',
        type=int,
    )

    parser_run.add_argument(
        '--rdata',
        metavar='<path>',
        help='Trained FIX RData filename',
        type=type_path,
    )

    parser_run.add_argument(
        '--fix_threshold',
        metavar='<int>',
        help='FIX classifier threshold',
        type=int,
    )

    parser_run.add_argument(
        '--standard_age',
        metavar='<int>',
        default=40,
        help='Age in weeks of standard template [default=40]',
        type=int,
    )

    parser_run.add_argument(
        '--group_map',
        metavar='<path>',
        help='Name of group maps',
        type=type_path,
        default=None,
    )

    parser_run.add_argument(
        '--standard_res',
        metavar='<int>',
        default=1.5,
        help='Standard output resolution for QC [default=1.5mm]',
        type=int,
    )

    parser_run.add_argument(
        '--group_qc',
        metavar='<path>',
        help='Name of group QC json',
        type=type_path,
        default=None,
    )

    parser_run.set_defaults(method='runall')

    #############################################
    # create the parser for the PRE-MCDC command
    #############################################

    parser_pre_mcdc = subparsers.add_parser(
        'PRE-MCDC',
        description='Run pre-MCDC (motion and distortion correction) stages of the pipeline',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_pre_mcdc.add_argument(
        '--fieldmap_source',
        choices={'spin_echo_epi_derived', 'dual_echo_time_derived'},
        help='Fieldmap type [default=spin_echo_epi_derived]',
        type=type_fieldmap,
        default='spin_echo_epi_derived'
    )

    parser_pre_mcdc.add_argument(
        '--pedir_n',
        metavar='<int>',
        help='Number of volumes per spinecho pedir to use [default=1]',
        type=int,
        default=1
    )

    parser_pre_mcdc.set_defaults(method='pre_mcdc')


    #############################################
    # create the parser for the MCDC command
    #############################################

    parser_mcdc = subparsers.add_parser(
        'MCDC',
        description='Run motion and susceptibility distortion correction',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_mcdc.set_defaults(method='mcdc')

    #############################################
    # create the parser for the POST-MCDC command
    #############################################

    parser_post_mcdc = subparsers.add_parser(
        'POST-MCDC',
        description='Run POST-MCDC (motion and distortion correction) stages of the pipeline',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_post_mcdc.add_argument(
        '--temporal_fwhm',
        metavar='<float>',
        help='Temporal filter FWHM (seconds) [default=150s]',
        type=float,
        default=150.0,
    )

    parser_post_mcdc.add_argument(
        '--icadim',
        metavar='<int>',
        help='Maximum ICA dimension',
        type=int,
    )

    parser_post_mcdc.add_argument(
        '--rdata',
        metavar='<path>',
        help='Trained FIX RData filename',
        type=type_path,
    )
    parser_post_mcdc.add_argument(
        '--fix_threshold',
        metavar='<int>',
        help='FIX classifier threshold',
        type=int,
    )

    parser_post_mcdc.add_argument(
        '--standard_age',
        metavar='<int>',
        default=40,
        help='Age in weeks of standard template [default=40]',
        type=int,
    )

    parser_post_mcdc.add_argument(
        '--group_map',
        metavar='<path>',
        help='Name of group maps',
        type=type_path,
        default=None,
    )

    parser_post_mcdc.add_argument(
        '--standard_res',
        metavar='<int>',
        default=1.5,
        help='Standard output resolution for QC [default=1.5mm]',
        type=int,
    )

    parser_post_mcdc.add_argument(
        '--group_qc',
        metavar='<path>',
        help='Name of group QC json',
        type=type_path,
        default=None,
    )

    parser_post_mcdc.set_defaults(method='post_mcdc')

    #############################################
    # create the parser for the IMPORT command
    #############################################

    parser_import = subparsers.add_parser(
        'IMPORT',
        description='Import files in the pipeline',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100)
    )

    parser_import_reqd = parser_import.add_argument_group('required named arguments')

    parser_import_reqd.add_argument(
        '--scan_pma',
        metavar='<float>',
        help='Gestational age at scan (weeks)',
        type=float,
        required=True,
    )

    parser_import_reqd.add_argument(
        '--birth_ga',
        metavar='<float>',
        help='Gestational age at birth (weeks)',
        type=float,
        required=True,
    )

    parser_import_reqd.add_argument(
        '--T2w',
        metavar='<path>',
        help='T2w image filename',
        type=type_path,
        required=True
    )
    parser_import_reqd.add_argument(
        '--T2w_brainmask',
        metavar='<path>',
        help='T2w binary brain mask filename',
        type=type_path,
        required=True)

    parser_import_reqd.add_argument(
        '--T2w_dseg',
        metavar='<path>',
        help='T2w discrete segmentation filename',
        type=type_path,
        required=True
    )

    parser_import_reqd.add_argument(
        '--T2w_dseg_type',
        help='T2w discrete segmentation type [default=drawem]',
        type=type_segtype,
        metavar="{'drawem', 'freesurfer_aseg', 'fsl_fast'}",
        default='drawem',
    )

    parser_import.add_argument(
        '--T1w',
        metavar='<path>',
        help='T1w image filename (aligned with T2w)',
        type=type_path,
    )

    parser_import_reqd.add_argument(
        '--func',
        metavar='<path>',
        help='Functional 4D image filename',
        type=type_path,
        required=True,
    )

    parser_import_reqd.add_argument(
        '--func_pedir',
        help="Functional phase encoding direction",
        metavar="{'AP', 'PA', 'LR', 'RL'}",
        type=type_pedir,
    )

    parser_import_reqd.add_argument(
        '--func_echospacing',
        metavar='<float>',
        help='Functional effective EPI echo spacing (seconds)',
        type=float
    )

    parser_import_reqd.add_argument(
        '--func_sliceorder',
        metavar='<path>',
        help='Name of slice order specification file (or "dhcp" for default dhcp slice spec)',
        type=type_slice_spec,
    )

    parser_import_reqd.add_argument(
        '--sbref',
        metavar='<path>',
        help='Name of SBref image',
        type=type_path,
        required=True
    )

    parser_import_reqd.add_argument(
        '--sbref_pedir',
        help='SBRef phase encoding direction',
        type=type_pedir,
        metavar="{'AP', 'PA', 'LR', 'RL'}",
    )

    parser_import_reqd.add_argument(
        '--sbref_echospacing',
        metavar='<float>',
        help='SBRef effective EPI echo spacing (seconds)',
        type=float
    )

    parser_import_reqd.add_argument(
        '--spinecho',
        metavar='<path>',
        help='Name of 4D file with spinecho images',
        type=type_path,
        required=True
    )

    parser_import_reqd.add_argument(
        '--spinecho_pedir',
        metavar='<comma-separated list>',
        help='Spinecho phase encoding direction',
        required=True,
        type=type_pedir_list
    )

    parser_import_reqd.add_argument(
        '--spinecho_echospacing',
        metavar='<float>',
        help='Spinecho effective EPI echo spacing (seconds)',
        type=float,
        required=True
    )

    parser_import.add_argument(
        '--spinecho_epifactor',
        metavar='<int>',
        type=int,
        help='Spinecho epi factor (k-space lines in single epi shot) (default=calculated from spinecho)',
        default=None
    )

    parser_import.add_argument(
        '--spinecho_inplaneaccel',
        metavar='<float>',
        default=1,
        help='Spinecho in-plane acceleration factor (default=1)',
        type=float
    )

    parser_import.set_defaults(method='import')

    #############################################
    # create the parser for the FIELDMAP command
    #############################################

    parser_fmap = subparsers.add_parser(
        'FIELDMAP',
        description='Prepare fieldmaps',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_fmap.add_argument(
        '--fieldmap_source',
        choices={'spin_echo_epi_derived', 'dual_echo_time_derived'},
        help='Fieldmap type [default=spin_echo_epi_derived]',
        type=type_fieldmap,
        default='spin_echo_epi_derived'
    )

    parser_fmap.add_argument(
        '--pedir_n',
        metavar='<int>',
        help='Number of volumes per spinecho pedir to use [default=1]',
        type=int,
        default=1
    )

    parser_fmap.set_defaults(method='fmap')

    #############################################
    # create the parser for the ICA command
    #############################################

    parser_ica = subparsers.add_parser(
        'ICA',
        description='Run melodic ICA',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_ica.add_argument(
        '--temporal_fwhm',
        metavar='<float>',
        help='Temporal filter FWHM (seconds) [default=150s]',
        type=float,
        default=150.0,
    )
    parser_ica.add_argument(
        '--icadim',
        metavar='<int>',
        help='Maximum ICA dimension',
        type=int,
    )

    parser_ica.set_defaults(method='ica')

    #############################################
    # create the parser for the DENOISE command
    #############################################

    parser_denoise = subparsers.add_parser(
        'DENOISE',
        description='Run FIX-based denoising',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_denoise.add_argument(
        '--rdata',
        metavar='<path>',
        help='Trained FIX RData filename',
        type=type_path,
    )
    parser_denoise.add_argument(
        '--fix_threshold',
        metavar='<int>',
        help='FIX classifier threshold',
        type=int,
    )

    parser_denoise.set_defaults(method='denoise')


    #############################################
    # create the parser for the STANDARD command
    #############################################

    parser_standard = subparsers.add_parser(
        'STANDARD',
        description='Register to standard/atlas/template space',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_standard.add_argument(
        '--standard_age',
        metavar='<int>',
        default=40,
        help='Age in weeks of standard template [default=40]',
        type=int,
    )

    parser_standard.set_defaults(method='standard')

    #############################################
    # create the parser for the QC command
    #############################################

    parser_qc = subparsers.add_parser(
        'QC',
        description='Perform subject level QC',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_qc.add_argument(
        '--group_map',
        metavar='<path>',
        help='Name of group maps',
        type=type_path,
        default=None,
    )

    parser_qc.add_argument(
        '--standard_age',
        metavar='<int>',
        default=40,
        help='Age in weeks of standard template [default=40]',
        type=int,
    )

    parser_qc.add_argument(
        '--standard_res',
        metavar='<int>',
        default=1.5,
        help='Standard output resolution for QC [default=1.5mm]',
        type=int,
    )

    parser_qc.set_defaults(method='qc')

    #############################################
    # create the parser for the QC-REPORT command
    #############################################

    parser_report = subparsers.add_parser(
        'QC-REPORT',
        description='Generate QC report',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=55, width=100))

    parser_report.add_argument(
        '--group_qc',
        metavar='<path>',
        help='Name of group QC json',
        type=type_path,
        default=None,
    )

    parser_report.set_defaults(method='report')

    #############################################
    # parse args
    #############################################

    if len(sys.argv) == 1:
        parser.parse_args(['-h'])
    else:
        args = vars(parser.parse_args())

    # print(args)

    method = args.pop('method')

    p = Pipeline(
        args.pop('subid'),
        args.pop('sesid'),
        args.pop('path')
    )

    if method == 'import':
        p.import_data(**args)
    elif method == 'fmap':
        p.prepare_fieldmap(**args)
    elif method == 'mcdc':
        p.mcdc(**args)
    elif method == 'ica':
        p.ica(**args)
    elif method == 'standard':
        p.standard(**args)
    elif method == 'qc':
        p.qc(**args)
    elif method == 'report':
        p.report(**args)
    elif method == 'runall':
        p.run_all(**args)
    elif method == 'pre_mcdc':
        p.pre_mcdc(**args)
    elif method == 'post_mcdc':
        p.post_mcdc(**args)
    else:
        raise RuntimeError(f'Unknown method: {method}')
